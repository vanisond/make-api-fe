# Make API - FE

A web application for creating APIs from web pages.

## Pre-requisites

You need to have the [URL Fetcher](https://gitlab.fit.cvut.cz/vanisond/make-api-be-fetcher) service running. 

## Environment variables

Before running the service, you need to create an environment file `.env.local` in the root directory of the project. You can use the `.env.example` file as a template.

(You can also use the `.env` name.)

## Running the service

To run the service, you need to have [Node.js](https://nodejs.org/en/) and [npm](https://www.npmjs.com/) installed. You also need to have [Docker](https://docs.docker.com/get-docker/) in order to run the PostgreSQL database. (Although you can use any other PostgreSQL database, you just need to change the connection URL in the environment file.)

To start a PostgreSQL database, run the following command:

```bash
docker run --name make-api-db -e POSTGRES_PASSWORD=mysecretpassword -p 5455:5432 -d postgres
```

To start a MailDev server, run the following command:

```bash
docker run --name make-api-maildev -p 1080:1080 -p 1025:1025 maildev/maildev
```

### Setup database

To set up the database, you need to run the following command:

```bash
npx dotenv -e .env.local -- npx prisma db push
npx dotenv -e .env.local -- npx prisma generate
```

It will create the database schema and generate the Prisma client.

### Start the server

Then you need to install the dependencies and start the service:

```bash
npm install
npm run start # or `npm run dev` for development
```

Now the service should be running at [http://localhost:3000/](http://localhost:3000/).
