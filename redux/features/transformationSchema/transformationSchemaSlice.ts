import type {PayloadAction} from '@reduxjs/toolkit'
import {createSlice} from '@reduxjs/toolkit'
import {TransformationSchemaNodeType} from "@/lib/transformService/nodeTypes";
import {nullType} from "@/lib/transformService/nodeTypes/nullType";
import {ZodIssue} from "zod";
import {TransformContextErrorType} from "@/lib/transformService/transform";

export interface TransformationSchemaState {
    schema: TransformationSchemaNodeType,
    pageHTML?: string,
    outputJSON?: string,
    schemaValidationErrors: ZodIssue[],
    transformationErrors: TransformContextErrorType[],
}

const initialState: TransformationSchemaState = {
    schema: nullType.defaultValue(),
    pageHTML: undefined,
    schemaValidationErrors: [],
    transformationErrors: [],
}

export const transformationSchemaSlice = createSlice({
    name: 'transformationSchema',
    initialState,
    reducers: {
        setSchema: (state, action: PayloadAction<TransformationSchemaNodeType>) => {
            state.schema = action.payload
        },
        setPageHTML: (state, action: PayloadAction<string>) => {
            state.pageHTML = action.payload
        },
        setOutputJSON: (state, action: PayloadAction<string>) => {
            state.outputJSON = action.payload
        },
        setSchemaValidationErrors: (state, action: PayloadAction<ZodIssue[]>) => {
            state.schemaValidationErrors = action.payload
        },
        setTransformationErrors: (state, action: PayloadAction<TransformContextErrorType[]>) => {
            state.transformationErrors = action.payload
        },
    },
})

export const { setSchema, setPageHTML, setOutputJSON, setSchemaValidationErrors, setTransformationErrors } = transformationSchemaSlice.actions

export default transformationSchemaSlice.reducer

