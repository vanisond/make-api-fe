import { configureStore } from '@reduxjs/toolkit'
import transformationSchemaReducer from "./features/transformationSchema/transformationSchemaSlice";

export const store = configureStore({
    reducer: {
        transformationSchema: transformationSchemaReducer
    },
    devTools: true
})

export type RootState = ReturnType<typeof store.getState>

export type AppDispatch = typeof store.dispatch


