// This is a simple rate limiter that uses a Map to store the timestamps of the requests.
// Uses FIFO/queue to remove expired requests.
const requests = new Map<string, number[]>();

const cleanupInterval = parseInt(process.env.API_RATE_LIMIT_CLEANUP_INTERVAL_MS ?? '0') // e.g. 1 hour (= 3600000 ms); how often to remove expired requests from the queue
const inactivityThreshold = parseInt(process.env.API_RATE_LIMIT_INACTIVITY_THRESHOLD_MS ?? '0'); // e.g. 24 hours (= 86400000 ms); how long to keep the request in the queue after the last request
let lastCleanup = Date.now();

/**
 * Removes expired requests from the queue.
 */
const cleanupOldEntries = () => {
    const now = Date.now();
    if (now - lastCleanup < cleanupInterval) {
        return;
    }

    lastCleanup = now;
    requests.forEach((requestTimes, entityId) => {
        if (requestTimes.length === 0 || now - requestTimes[requestTimes.length - 1] > inactivityThreshold) {
            requests.delete(entityId);
        }
    });
};

/**
 * Returns the cleaned requests for the given entity, uses shallow copy of the array.
 */
const getCleanedRequests = (entityId: string, windowMs: number) => {
    const now = Date.now();
    const requestWindowStart = now - windowMs;
    const entityRequests = requests.get(entityId) ?? [];

    // the expired timestamps will be in the beginning of the array
    const firstValidTimestampIndex = entityRequests.findIndex(requestTime => requestTime >= requestWindowStart);


    console.log({
        entityId,
        windowMs,
        entityRequests,
        validRequests: entityRequests.slice(firstValidTimestampIndex),
        firstValidTimestampIndex,
        now
    });

    return entityRequests.slice(firstValidTimestampIndex);
};

/**
 * Consumes one point (request) for the given entity.
 */
export const consume = (entityId: string, limit: number, windowMs: number) => {
    cleanupOldEntries();

    const now = Date.now();
    const entityRequests = getCleanedRequests(entityId, windowMs); // valid requests in the current window
    const remaining = Math.max(0, limit - entityRequests.length - 1); // remaining requests count for the current window
    const msBeforeNext = Math.max(0, entityRequests[0] ? entityRequests[0] - now + windowMs : windowMs); // number of milliseconds before next action can be done

    if (entityRequests.length >= limit) {
        return {limit, remaining, msBeforeNext, isLimited: true};
    }

    // add request timestamp to the end of the array
    entityRequests.push(Date.now());
    requests.set(entityId, entityRequests);

    return {limit, remaining, msBeforeNext, isLimited: false};
};
