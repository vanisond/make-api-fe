import {ProjectAccessToken} from "@prisma/client";
import {getProjectById} from "@/lib/repository/projects";
import {getUserById} from "@/lib/repository/users";
import {getRateLimit} from "@/lib/repository/rateLimits";
import {consume} from "@/lib/rateLimitService/rateLimiter";

const defaultAccessTokenRateLimit = {
    maxRequests: parseInt(process.env.API_RATE_LIMIT_DEFAULT_ACCESS_TOKEN_MAX_REQUESTS ?? '0'),
    windowMs: parseInt(process.env.API_RATE_LIMIT_DEFAULT_ACCESS_TOKEN_WINDOW_MS ?? '0'),
};

const defaultUserRateLimit = {
    maxRequests: parseInt(process.env.API_RATE_LIMIT_DEFAULT_USER_MAX_REQUESTS ?? '0'),
    windowMs: parseInt(process.env.API_RATE_LIMIT_DEFAULT_USER_WINDOW_MS ?? '0'),
};

export const applyRateLimitPolicy = async (validatedToken: ProjectAccessToken) => {
    // 1. rate limit by user
    const project = await getProjectById(validatedToken.projectId);
    const user = await getUserById(project!.ownerId);
    const userRateLimit = await getRateLimit(user?.apiRequestsPerUserRateLimitId ?? undefined);
    const userMaxRequests = userRateLimit?.maxRequests ? userRateLimit.maxRequests : defaultUserRateLimit.maxRequests; // Note: When user limit is set to 0, default rate limit is used
    const userWindowMs = userRateLimit?.windowMs ? userRateLimit.windowMs : defaultUserRateLimit.windowMs;
    const userRateLimiterResponse = consume(
        `user:${user!.id}`,
        userMaxRequests,
        userWindowMs,
    );

    // 2. rate limit by access token
    const accessTokenRateLimit = await getRateLimit(validatedToken?.apiRequestsPerTokenRateLimitId ?? undefined)
    const accessTokenMaxRequests = accessTokenRateLimit?.maxRequests ? accessTokenRateLimit.maxRequests : defaultAccessTokenRateLimit.maxRequests;
    const accessTokenWindowMs = accessTokenRateLimit?.windowMs ? accessTokenRateLimit.windowMs : defaultAccessTokenRateLimit.windowMs;

    const accessTokenRateLimiterResponse = consume(
        `access-token:${validatedToken.id}`,
        accessTokenMaxRequests,
        accessTokenWindowMs,
    );

    // Combine both rate limiters
    const rateLimiterResponse = {
        isLimited: userRateLimiterResponse.isLimited || accessTokenRateLimiterResponse.isLimited,
        limit: Math.min(
            userRateLimiterResponse.limit,
            accessTokenRateLimiterResponse.limit
        ),
        remaining: Math.min(
            userRateLimiterResponse.remaining,
            accessTokenRateLimiterResponse.remaining
        ),
        secondsBeforeNext: Math.min(
            Math.round(userRateLimiterResponse.msBeforeNext / 1000),
            Math.round(accessTokenRateLimiterResponse.msBeforeNext / 1000)
        ),
    }

    const rateLimitHeaders = {
        'RateLimit': `limit=${rateLimiterResponse.limit}, remaining=${rateLimiterResponse.remaining}, reset=${rateLimiterResponse.secondsBeforeNext}`,
    }

    return {
        rateLimiterResponse,
        rateLimitHeaders,
    }
}