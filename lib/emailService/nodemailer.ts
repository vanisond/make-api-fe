import nodemailer from "nodemailer";
import {getUserById} from "@/lib/repository/users";

export const transporter = nodemailer.createTransport({
    host: process.env.EMAIL_SERVER_HOST,
    port: Number(process.env.EMAIL_SERVER_PORT),
    auth: {
        user: process.env.EMAIL_SERVER_USER,
        pass: process.env.EMAIL_SERVER_PASSWORD,
    },
});

/**
 * Send mail to user
 *
 * @example
 *  const info = await sendMailTo(userId, {
 *   subject: "Hello ✔", // Subject line
 *   text: "Hello world?", // plain text body
 *   html: "<b>Hello world?</b>", // html body
 *  });
 */
export const sendMailToUser = async (userId: string, options: nodemailer.SendMailOptions) => {
    const user = await getUserById(userId);
    if (!user) {
        throw new Error(`Can't send mail. User with id ${userId} not found.`);
    }

    return transporter.sendMail({
        from: process.env.EMAIL_FROM,
        to: user.email,
        ...options,
    });
}
