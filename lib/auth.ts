import prisma from "@/lib/prisma";
import CredentialsProvider from "next-auth/providers/credentials";
import type {GetServerSidePropsContext, NextApiRequest, NextApiResponse} from "next"
import type {NextAuthOptions} from "next-auth"
import {getServerSession} from "next-auth"
import {compare} from "bcrypt";
import {User} from "@prisma/client";

// You'll need to import and pass this
// to `NextAuth` in `app/api/auth/[...nextauth]/route.ts`
export const config = {
    // Adapter is needed for OAuth and Email providers
    // @see https://authjs.dev/reference/adapter/prisma
    // adapter: PrismaAdapter(prisma),
    providers: [
        CredentialsProvider({
            // The name to display on the sign in form (e.g. "Sign in with...")
            name: "Credentials",
            // `credentials` is used to generate a form on the sign in page.
            // You can specify which fields should be submitted, by adding keys to the `credentials` object.
            // e.g. domain, username, password, 2FA token, etc.
            // You can pass any HTML attribute to the <input> tag through the object.
            credentials: {
                email: {label: "E-mail", type: "email", placeholder: "jsmith"},
                password: {label: "Password", type: "password"}
            },
            authorize: async (credentials, req) => {
                const {email, password} = credentials ?? {}
                if (!email || !password) {
                    throw new Error("Missing username or password");
                }
                const user = await prisma.user.findUnique({
                    where: {
                        email,
                    },
                });
                // if user doesn't exist or password doesn't match
                if (!user || !(await compare(password, user.password))) {
                    throw new Error("Invalid username or password");
                }
                return user
            },
        }),
        // Example email provider
        // EmailProvider({
        //     server: {
        //         host: process.env.EMAIL_SERVER_HOST,
        //         port: Number(process.env.EMAIL_SERVER_PORT),
        //     },
        //     from: process.env.EMAIL_FROM, // The email address from which the verification email will be sent
        // }),
    ],


    // @see https://next-auth.js.org/configuration/callbacks#session-callback
    callbacks: {
        jwt: ({token, user}) => {
            // User is only available on the first call during email sign
            if (user) {
                token.role = (user as User)?.role
            }
            return token;
        },
        session: async ({ session, token}) => {
            return {...session, user: {...session.user, role: token.role, id: token.sub}};
        },
    },
    session: {
        strategy: 'jwt',
    },

    // Custom pages for auth
    // @see https://next-auth.js.org/configuration/pages
    pages: {
        signIn: '/login',
        // signOut: '/auth/signout',
        // error: '/auth/error', // Error code passed in query string as ?error=
        // verifyRequest: '/auth/verify-request', // (used for check email message)
        // newUser: '/auth/new-user' // New users will be directed here on first sign in (leave the property out if not of interest)
    },

    debug: process.env.NODE_ENV === "development",
} satisfies NextAuthOptions


// Use it in server contexts
// @see https://next-auth.js.org/configuration/initialization#advanced-initialization
export function auth(...args: [GetServerSidePropsContext["req"], GetServerSidePropsContext["res"]] | [NextApiRequest, NextApiResponse] | []) {
    return getServerSession(...args, config)
}


