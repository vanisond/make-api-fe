import {type ClassValue, clsx} from "clsx"
import {twMerge} from "tailwind-merge"

export function cn(...inputs: ClassValue[]) {
  return twMerge(clsx(inputs))
}

/**
 * Get the robots.txt URL for a given URL
 */
export const getRobotsTxtUrl = (originalUrl: string) => {
  try {
    const url = new URL(originalUrl);
    url.pathname = '/robots.txt';
    return url.toString();
  } catch (error) {
    return undefined;
  }
}

export const shortenLongStrings = (obj: any, maxLength: number): any => {
    if (typeof obj === 'string') {
        // Shorten the string if it's longer than maxLength
        return obj.length > maxLength ? obj.substring(0, maxLength) + ' (...)' : obj;
    } else if (Array.isArray(obj)) {
        return obj.map(element => shortenLongStrings(element, maxLength));
    } else if (typeof obj === 'object' && obj !== null) {
        const shortenedObj: { [key: string]: any } = {};
        for (const key in obj) {
            if (Object.prototype.hasOwnProperty.call(obj, key)) {
                shortenedObj[key] = shortenLongStrings(obj[key], maxLength);
            }
        }
        return shortenedObj;
    } else {
        return obj;
    }
};
