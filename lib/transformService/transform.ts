import * as cheerio from 'cheerio'
import {NodeSchemaType, nodeTypeSchemas, TransformationSchemaNodeType} from "./nodeTypes";


export type TransformationNodeFunctionType<TType extends keyof typeof nodeTypeSchemas = keyof typeof nodeTypeSchemas> = (
    self: TransformationNodeFunctionType,
    contextNode: cheerio.Cheerio<cheerio.Element>,
    schema: NodeSchemaType<TType>,
    path: (string | number)[],
    context: ReturnType<typeof createContext>
) => any;

const transformNode: TransformationNodeFunctionType = (
    self,
    contextNode,
    schema,
    path,
    context
): any => {
    try {
        const typeTransformer = nodeTypeSchemas[schema.type];

        if (!typeTransformer) {
            // This should never happen
            context.addError({
                code: "unknown_node_type",
                severity: "error",
                message: `Unknown node type "${schema.type}"`,
                path: [...path],
            });
            return null;
        }

        // Type assertion is needed here, because TypeScript cannot infer the type of the schema correctly
        // However, the type of the schema is always correct, because it is validated by the schema itself
        return typeTransformer.transformNode(self, contextNode, schema as any, path, context);

    } catch (error) {
        // Cheerio throws errors that are instances of Error
        if (error instanceof Error) {
            context.addError({
                code: "parse_error",
                message: error.message,
                severity: "error",
                path: [...path],
            });
            return null;
        }

        // This should never happen
        context.addError({
            code: "unknown_error",
            message: `Unknown error`,
            severity: "error",
            path: [...path],
        });
        return null;
    }
};

export type TransformContextErrorType = {
    code: string;
    severity: "error" | "warning",
    message: string,
    path: (string | number)[],
};

export const createContext = ($: cheerio.CheerioAPI) => {
    const errors: TransformContextErrorType[] = [];
    return {
        $,
        errors,
        addError: (error: TransformContextErrorType) => {
            errors.push(error);
        }
    }
}

export const transformHTMLtoJSON = (html: string, schema: TransformationSchemaNodeType) => {
    try {
        const $ = cheerio.load(html);
        const root = $('html');
        const context = createContext($);
        const data = transformNode(transformNode, root, schema, [], context);
        return {
            success: true,
            data,
            context,
        };
    } catch (error) {
        // This should never happen, if the passed `html` is of type string, cheerio should always be able to parse it
        return {
            success: false,
            data: null,
            context: null,
            error: "Failed to transform HTML to JSON"
        }
    }
}
