import * as cheerio from "cheerio";
import {nodeSchema} from "../nodeTypes";
import {objectType} from "../nodeTypes/objectType";
import {stringType} from "../nodeTypes/stringType";
import {createContext} from "../transform";

describe('objectType', () => {
    it('should identify ObjectNodeType correctly', () => {
        const nodeType = { type: "object" };
        expect(objectType.isOfType(nodeType)).toBeTruthy();
    });

    describe('typeSchema', () => {
        it('should validate object schema correctly', () => {
            const validSchema = {
                type: "object",
                selector: ".test",
                properties: [
                    {property: "test", type: "string", selector: ".prop"},
                ]
            };
            expect(() => objectType.typeSchema(nodeSchema).parse(validSchema)).not.toThrow();
        });

        it('should validate object schema with nested objects correctly', () => {
            const validSchema = {
                type: "object",
                selector: ".test",
                properties: [
                    {property: "test", type: "string", selector: ".prop1"},
                    {
                        property: "test2", type: "object", selector: ".prop2", properties: [
                            {property: "test3", type: "string", selector: ".prop3"},
                        ]
                    },
                ]
            };
            expect(() => objectType.typeSchema(nodeSchema).parse(validSchema)).not.toThrow();
        });

        it('should throw error for invalid object schema', () => {
            const invalidSchema = {
                type: "object",
                selector: ".test",
                // missing properties
            };
            expect(() => objectType.typeSchema(nodeSchema).parse(invalidSchema)).toThrow();
        });
    });

    it('should transform object node correctly', () => {
        const html = "<div><span id='p1'>aaa</span><span id='p2'>bbb</span></div>";
        const schema = {
            type: "object",
            selector: "div",
            properties: [
                {property: "prop1", type: "string", selector: "#p1"},
                {property: "prop2", type: "string", selector: "#p2"},
            ]
        };

        const MOCK_VALUE = "innerValue";
        const transformNodeMock = jest.fn().mockImplementation(() => MOCK_VALUE)
        const $ = cheerio.load(html);
        const context = createContext($);

        const rootNode = context.$('html');
        const path = [];

        const result = objectType.transformNode(transformNodeMock, rootNode, schema, path, context);

        expect(transformNodeMock).toHaveBeenCalledTimes(2);
        expect(transformNodeMock).toHaveBeenCalledWith(
            expect.anything(),
            expect.anything(),
            schema.properties[0],
            ["properties", 0],
            context
        );
        expect(transformNodeMock).toHaveBeenCalledWith(
            expect.anything(),
            expect.anything(),
            schema.properties[1],
            ["properties", 1],
            context
        );
        expect(result).toEqual({
            prop1: MOCK_VALUE,
            prop2: MOCK_VALUE,
        });
    });

    describe('defaultValue', () => {
        it('should return previous value when previous value is of type object', () => {
            const previousValue = {type: 'object', selector: 'a', properties: []};
            expect(objectType.defaultValue(previousValue)).toEqual(previousValue);
        });

        it('should return default object when previous value is null', () => {
            const previousValue = null;
            const expectedValue = {
                type: "object",
                properties: [{property: "", ...stringType.defaultValue()}],
                selector: "",
            };
            expect(objectType.defaultValue(previousValue)).toEqual(expectedValue);
        });

        it('should copy selector from previous value', () => {
            const previousValue = {type: "number", selector: 'a'};
            const expectedValue = {
                type: "object",
                properties: [{property: "", ...stringType.defaultValue()}],
                selector: "a",
            };
            expect(objectType.defaultValue(previousValue)).toEqual(expectedValue);
        });
    });

});
