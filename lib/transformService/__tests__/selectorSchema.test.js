import {selectorSchema, hasSelector} from "../nodeTypes/selectorSchema";

describe('selectorSchema', () => {
    it('should identify type node with a selector property', () => {
        const nodeType = { type: "any", selector: ".test" };
        expect(hasSelector(nodeType)).toBeTruthy();
    });

    it('should validate selector with selector schema correctly', () => {
        const selector = ".test";
        expect(() => selectorSchema.parse(selector)).not.toThrow();
    });
});
