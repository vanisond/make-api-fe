import * as cheerio from "cheerio";
import {nodeSchema} from "../nodeTypes";
import {stringType} from "../nodeTypes/stringType";
import {createContext} from "../transform";

describe('stringType', () => {
    it('should identify StringNodeType correctly', () => {
        const nodeType = {type: "string"};
        expect(stringType.isOfType(nodeType)).toBeTruthy();
    });

    describe('typeSchema', () => {
        it('should validate string schema correctly', () => {
            const validSchema = {
                type: "string",
                selector: ".test",
            };
            expect(() => stringType.typeSchema(nodeSchema).parse(validSchema)).not.toThrow();
        });

        it('should throw error for invalid string schema', () => {
            const validSchema = {
                type: "string",
                // missing selector
            };
            expect(() => stringType.typeSchema(nodeSchema).parse(validSchema)).toThrow();
        });
    });

    it('should transform string node correctly', () => {
        const html = "<div id='test'>aaa</div>";
        const schema = {
            type: "string",
            selector: "div#test",
        };

        const transformNodeMock = jest.fn();
        const $ = cheerio.load(html);
        const context = createContext($);

        const rootNode = context.$('html');
        const path = [];

        const result = stringType.transformNode(transformNodeMock, rootNode, schema, path, context);

        expect(transformNodeMock).toHaveBeenCalledTimes(0);
        expect(result).toEqual("aaa");
    });

    describe('defaultValue', () => {
        it('should return previous value when previous value is of type string', () => {
            const previousValue = {type: 'string', selector: 'a'};
            expect(stringType.defaultValue(previousValue)).toEqual(previousValue);
        });

        it('should return default object when previous value is null', () => {
            const previousValue = null;
            const expectedValue = {
                type: "string",
                selector: "",
            };
            expect(stringType.defaultValue(previousValue)).toEqual(expectedValue);
        });

        it('should copy selector from previous value', () => {
            const previousValue = {type: "number", selector: 'a'};
            const expectedValue = {
                type: "string",
                selector: "a",
            };
            expect(stringType.defaultValue(previousValue)).toEqual(expectedValue);
        });
    });
});