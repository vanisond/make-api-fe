import * as cheerio from "cheerio";
import {nodeSchema} from "../nodeTypes";
import {arrayType} from "../nodeTypes/arrayType";
import {createContext} from "../transform";

describe('arrayType', () => {
    it('should identify ArrayNodeType correctly', () => {
        const nodeType = { type: "array" };
        expect(arrayType.isOfType(nodeType)).toBeTruthy();
    });

    describe('typeSchema', () => {
        it('should validate array schema correctly', () => {
            const validSchema = {
                type: "array",
                selector: ".test",
                items: {type: "null"},
            };
            expect(() => arrayType.typeSchema(nodeSchema).parse(validSchema)).not.toThrow();
        });

        it('should validate array schema with nested items correctly', () => {
            const validSchema = {
                type: "array",
                selector: ".test",
                items: {
                    type: "array",
                    selector: ".item",
                    items: {type: "null"},
                }
            };
            expect(() => arrayType.typeSchema(nodeSchema).parse(validSchema)).not.toThrow();
        });

        it('should throw error for invalid array schema', () => {
            const invalidSchema = {
                type: "array",
                selector: ".test",
                // missing items
            };
            expect(() => arrayType.typeSchema(nodeSchema).parse(invalidSchema)).toThrow();
        });
    });

    it('should transform array node correctly', () => {
        const html = "<ul><li>>aaa</li><li>bbb</li><li>ccc</li></ul>";
        const schema = {
            type: "array",
            selector: "ul > li",
            items: {type: "string", selector: ""},
        };

        const MOCK_VALUE = "innerValue";
        const transformNodeMock = jest.fn().mockImplementation(() => MOCK_VALUE)
        const $ = cheerio.load(html);
        const context = createContext($);

        const rootNode = context.$('html');
        const path = [];

        const result = arrayType.transformNode(transformNodeMock, rootNode, schema, path, context);

        expect(transformNodeMock).toHaveBeenCalledTimes(3);
        expect(transformNodeMock).toHaveBeenCalledWith(
            expect.anything(),
            expect.anything(),
            schema.items,
            ["items"],
            context
        );
        expect(result).toEqual([MOCK_VALUE, MOCK_VALUE, MOCK_VALUE]);
    });

});