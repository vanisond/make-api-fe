import {createContext, transformHTMLtoJSON} from '../transform';
import * as cheerio from 'cheerio'

describe('createContext tests', () => {
    let $;
    let context;

    beforeEach(() => {
        $ = cheerio.load('<div></div>');
        context = createContext($);
    });

    it('should create a context with an empty errors array', () => {
        expect(context.errors).toEqual([]);
    });

    it('should add an error to the context correctly', () => {
        const error = {
            code: 'test_error',
            severity: 'warning',
            message: 'Test error message',
            path: ['path', 'to', 'error']
        };
        context.addError(error);
        expect(context.errors).toContain(error);
    });

    it('should correctly set the error object content', () => {
        const error = {
            code: 'unique_code',
            severity: 'info',
            message: 'Unique message',
            path: ['unique', 'path']
        };
        context.addError(error);
        const addedError = context.errors[0];
        expect(addedError).toEqual(error);
    });
});


describe('transformHTMLtoJSON', () => {

    it('returns an error if the schema is invalid', () => {
        const html = '<div>Test</div>';
        /** @type {any} */
        const schema = { type: 'invalid' };
        const result = transformHTMLtoJSON(html, schema);
        expect(result.success).toBeTruthy(); // should be always true, only for unexpected errors should be false
        expect(result.data).toEqual(null);
        expect(result.context?.errors.length).toBe(1);
    });

    it('transforms simple HTML correctly', () => {
        const html = '<div>Test</div>';
        const schema = { type: 'number', selector: 'div' };
        const result = transformHTMLtoJSON(html, schema);
        expect(result.success).toBeTruthy();
        expect(result.data).toEqual(null);
    });

    it('should transform complex HTML correctly', () => {
        const html = `
            <ul id="test">
                <span class="test">1.45</span>
                <span class="test">text</span>
            </ul>
        `;
        const schema = {
            type: 'object',
            selector: '#test',
            properties: [
                { property: 'n', type: 'number', selector: '.test:nth-of-type(1)' },
                { property: 's', type: 'string', selector: '.test:nth-of-type(2)' },
                { property: 'b', type: 'boolean', selector: '.test' },
                { property: 'a', type: 'array', selector: '.test', items: { type: 'string', selector: "" } },
            ]
        };
        const result = transformHTMLtoJSON(html, schema);
        expect(result.success).toBeTruthy();
        expect(result.data).toEqual({
            n: 1.45,
            s: 'text',
            b: true,
            a: ['1.45', 'text']
        });
    });

});