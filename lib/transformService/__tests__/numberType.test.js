import * as cheerio from "cheerio";
import {nodeSchema} from "../nodeTypes";
import {numberType} from "../nodeTypes/numberType";
import {createContext} from "../transform";

describe('numberType', () => {
    it('should identify NumberNodeType correctly', () => {
        const nodeType = { type: "number" };
        expect(numberType.isOfType(nodeType)).toBeTruthy();
    });

    describe('typeSchema', () => {
        it('should validate array schema correctly', () => {
            const validSchema = {
                type: "number",
                selector: ".test",
            };
            expect(() => numberType.typeSchema(nodeSchema).parse(validSchema)).not.toThrow();
        });

        it('should throw error for invalid number schema', () => {
            const validSchema = {
                type: "number",
                // missing selector
            };
            expect(() => numberType.typeSchema(nodeSchema).parse(validSchema)).toThrow();
        });
    });

    it('should transform number node correctly', () => {
        const html = "<div id='test'>1.45</div>";
        const schema = {
            type: "number",
            selector: "div#test",
        };

        const transformNodeMock = jest.fn();
        const $ = cheerio.load(html);
        const context = createContext($);

        const rootNode = context.$('html');
        const path = [];

        const result = numberType.transformNode(transformNodeMock, rootNode, schema, path, context);

        expect(transformNodeMock).toHaveBeenCalledTimes(0);
        expect(result).toEqual(1.45);
    });

    describe('defaultValue', () => {
        it('should return previous value when previous value is of type number', () => {
            const previousValue = {type: 'number', selector: 'a'};
            expect(numberType.defaultValue(previousValue)).toEqual(previousValue);
        });

        it('should return default object when previous value is null', () => {
            const previousValue = null;
            const expectedValue = {
                type: "number",
                selector: "",
            };
            expect(numberType.defaultValue(previousValue)).toEqual(expectedValue);
        });

        it('should copy selector from previous value', () => {
            const previousValue = {type: "string", selector: 'a'};
            const expectedValue = {
                type: "number",
                selector: "a",
            };
            expect(numberType.defaultValue(previousValue)).toEqual(expectedValue);
        });
    });


});