import * as cheerio from "cheerio";
import {nodeSchema} from "../nodeTypes";
import {booleanType} from "../nodeTypes/booleanType";
import {createContext} from "../transform";

describe('booleanType', () => {
    it('should identify BooleanNodeType correctly', () => {
        const nodeType = {type: "boolean"};
        expect(booleanType.isOfType(nodeType)).toBeTruthy();
    });

    describe('typeSchema', () => {
        it('should validate boolean schema correctly', () => {
            const validSchema = {
                type: "boolean",
                selector: ".test",
            };
            expect(() => booleanType.typeSchema(nodeSchema).parse(validSchema)).not.toThrow();
        });

        it('should throw error for invalid boolean schema', () => {
            const validSchema = {
                type: "boolean",
                // missing selector
            };
            expect(() => booleanType.typeSchema(nodeSchema).parse(validSchema)).toThrow();
        });
    });

    it('should transform boolean node correctly', () => {
        const html = "<div id='test'>aaa</div>";
        const schema = {
            type: "boolean",
            selector: "div#test",
        };

        const transformNodeMock = jest.fn();
        const $ = cheerio.load(html);
        const context = createContext($);

        const rootNode = context.$('html');
        const path = [];

        const result = booleanType.transformNode(transformNodeMock, rootNode, schema, path, context);

        expect(transformNodeMock).toHaveBeenCalledTimes(0);
        expect(result).toEqual(true);
    });

    describe('defaultValue', () => {
        it('should return previous value when previous value is of type boolean', () => {
            const previousValue = {type: 'boolean', selector: 'a'};
            expect(booleanType.defaultValue(previousValue)).toEqual(previousValue);
        });

        it('should return default object when previous value is null', () => {
            const previousValue = null;
            const expectedValue = {
                type: "boolean",
                selector: "",
            };
            expect(booleanType.defaultValue(previousValue)).toEqual(expectedValue);
        });

        it('should copy selector from previous value', () => {
            const previousValue = {type: "number", selector: 'a'};
            const expectedValue = {
                type: "boolean",
                selector: "a",
            };
            expect(booleanType.defaultValue(previousValue)).toEqual(expectedValue);
        });
    });

});