import * as cheerio from "cheerio";
import {nodeSchema} from "../nodeTypes";
import {nullType} from "../nodeTypes/nullType";
import {createContext} from "../transform";

describe('nullType', () => {
    it('should identify NullNodeType correctly', () => {
        const nodeType = { type: "null" };
        expect(nullType.isOfType(nodeType)).toBeTruthy();
    });

    it('should validate null schema correctly', () => {
        const validSchema = {
            type: "null",
        };
        expect(() => nullType.typeSchema(nodeSchema).parse(validSchema)).not.toThrow();
    });


    it('should transform null node correctly', () => {
        const html = "<div id='test'>aaa</div>";
        const schema = {
            type: "null",
        };

        const transformNodeMock = jest.fn();
        const $ = cheerio.load(html);
        const context = createContext($);

        const rootNode = context.$('html');
        const path = [];

        const result = nullType.transformNode(transformNodeMock, rootNode, schema, path, context);

        expect(transformNodeMock).toHaveBeenCalledTimes(0);
        expect(result).toEqual(null);
    });

    describe('defaultValue', () => {
        it('should return default object', () => {
            const previousValue = null;
            const expectedValue = {
                type: "null",
            };
            expect(nullType.defaultValue(previousValue)).toEqual(expectedValue);
        });
    });
});