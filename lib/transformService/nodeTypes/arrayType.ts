import {hasSelector, selectorSchema} from "@/lib/transformService/nodeTypes/selectorSchema";
import {z} from "zod";
import {NodeTypeDefinition} from "@/lib/transformService/nodeTypes/types";
import {NodeSchemaType} from "@/lib/transformService/nodeTypes/index";
import {stringType} from "@/lib/transformService/nodeTypes/stringType";

type ArrayNodeType = {
    type: "array",
    selector: string,
    items: NodeSchemaType,
};

export const arrayType: NodeTypeDefinition<ArrayNodeType> = {
    isOfType: (nodeType: any): nodeType is ArrayNodeType => nodeType?.type === "array",
    typeSchema: (nodeSchema) => z.object({
        selector: selectorSchema,
        type: z.literal("array"),
        items: nodeSchema,
    }),
    transformNode: (self, contextNode, schema, path, context) => {
        const selector = schema.selector;
        const nodes = selector === "" ? contextNode : contextNode.find(schema.selector);
        const pathId = path.join(".");

        if (nodes.length === 0) {
            context.addError({
                code: "selector_not_found",
                severity: "warning",
                message: `Selector "${selector}" matched no nodes at path .${pathId}`,
                path: [...path, "selector"],
            });
        }

        const arr: any[] = [];
        nodes.each((index, item) => {
            arr.push(self(self, context.$(item), schema.items, [...path, "items"], context));
        })
        return arr;
    },
    defaultValue: (previousValue) => {
        if (arrayType.isOfType(previousValue)) {
            return previousValue;
        }

        return {
            type: "array",
            items: stringType.defaultValue(),
            selector: hasSelector(previousValue) ? previousValue.selector : "",
        }
    }
} as const;
