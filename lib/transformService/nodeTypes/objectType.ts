import {hasSelector, selectorSchema} from "@/lib/transformService/nodeTypes/selectorSchema";
import {z} from "zod";
import {NodeTypeDefinition} from "@/lib/transformService/nodeTypes/types";
import {NodeSchemaType} from "@/lib/transformService/nodeTypes/index";
import {stringType} from "@/lib/transformService/nodeTypes/stringType";

type ObjectNodeType = {
    type: "object",
    selector: string,
    properties: (NodeSchemaType & { property: string })[],
};

export const objectType: NodeTypeDefinition<ObjectNodeType> = {
    isOfType: (nodeType: any): nodeType is ObjectNodeType => nodeType?.type === "object",
    typeSchema: (nodeSchema) => z.object({
        selector: selectorSchema,
        type: z.literal("object"),
        properties: z.array(nodeSchema.and(z.object({
            property: z.string().refine(
                value => value !== "",
                {
                    message: "Property name cannot be empty"
                }
            )
        })))
    }).refine(data => {
        // Check that properties are unique
        const propertyNames = data.properties.map(p => p.property);
        return propertyNames.length === new Set(propertyNames).size;
    }, {
        message: "Properties must be unique",
        path: ['properties'],
    }),
    transformNode: (self, contextNode, schema, path, context) => {
        const selector = schema.selector;
        const nodes = selector === "" ? contextNode : contextNode.find(schema.selector);
        const firstNode = nodes.first();
        const pathId = path.join(".");

        if (nodes.length === 0) {
            context.addError({
                code: "selector_not_found",
                severity: "warning",
                message: `Selector "${selector}" matched no nodes at path .${pathId}.selector`,
                path: [...path, "selector"],
            });
        }

        if (nodes.length > 1) {
            context.addError({
                code: "multiple_nodes_found",
                severity: "warning",
                message: `Selector "${selector}" matched multiple nodes at path .${pathId}.selector`,
                path: [...path, "selector"],
            });
        }

        const value: { [key: string]: any } = {}
        schema.properties?.forEach((childSchema, index) => {
            if (childSchema.property || childSchema.property !== '') {
                value[childSchema.property] = self(
                    self,
                    firstNode,
                    childSchema,
                    [...path, "properties", index],
                    context
                );
            }
        })
        return value;
    },
    defaultValue: (previousValue) => {
        if (objectType.isOfType(previousValue)) {
            return previousValue;
        }

        return {
            type: "object",
            properties: [
                {
                    property: "",
                    ...stringType.defaultValue(),
                }
            ],
            selector: hasSelector(previousValue) ? previousValue.selector : "",
        }
    }
} as const;