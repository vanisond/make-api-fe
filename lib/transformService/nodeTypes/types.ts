import {TransformationNodeFunctionType} from "@/lib/transformService/transform";
import {z} from "zod";
import {NodeSchemaType, nodeTypeSchemas} from "@/lib/transformService/nodeTypes/index";

export type NodeTypeDefinition<NodeType extends { type: keyof typeof nodeTypeSchemas }> = {
    isOfType: (nodeType: any) => nodeType is NodeType,
    typeSchema: (nodeSchema: z.ZodType<NodeSchemaType>) => z.ZodType<NodeType>,
    transformNode: TransformationNodeFunctionType<NodeType["type"]>,
    defaultValue: (previousValue?: ExtractNodeType<typeof nodeTypeSchemas[keyof typeof nodeTypeSchemas]>) => NodeType,
}

export type ExtractNodeType<T> = T extends NodeTypeDefinition<infer NodeType> ? NodeType : never;
