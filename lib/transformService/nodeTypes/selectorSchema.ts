import {z} from "zod";
import * as CSSwhat from "css-what";

export const selectorSchema = z.string().refine(value => {
    try {
        CSSwhat.parse(value);
        return true;
    } catch (e) {
        return false;
    }
}, {
    message: "Invalid selector",
});

export const hasSelector = (nodeType: any): nodeType is { selector: string } => {
    return nodeType?.selector !== undefined;
}
