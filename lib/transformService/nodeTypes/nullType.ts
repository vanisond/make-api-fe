import {z} from "zod";
import {NodeTypeDefinition} from "@/lib/transformService/nodeTypes/types";

type NullNodeType = {
    type: "null",
};

export const nullType: NodeTypeDefinition<NullNodeType> = {
    isOfType: (nodeType: any): nodeType is NullNodeType => nodeType?.type === "null",
    typeSchema: (nodeSchema) => z.object({
        type: z.literal("null"),
    }),
    transformNode: () => {
        return null;
    },
    defaultValue: () => {
        return {
            type: "null",
        }
    }
} as const;
