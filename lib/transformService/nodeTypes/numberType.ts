import {hasSelector, selectorSchema} from "@/lib/transformService/nodeTypes/selectorSchema";
import {z} from "zod";
import {NodeTypeDefinition} from "@/lib/transformService/nodeTypes/types";

type NumberNodeType = {
    type: "number",
    selector: string,
};

export const numberType: NodeTypeDefinition<NumberNodeType> = {
    isOfType: (nodeType: any): nodeType is NumberNodeType => nodeType?.type === "number",
    typeSchema: (nodeSchema) => z.object({
        selector: selectorSchema,
        type: z.literal("number"),
    }),
    transformNode: (self, contextNode, schema, path, context) => {
        const selector = schema.selector;
        const nodes = selector === "" ? contextNode : contextNode.find(schema.selector);
        const firstNode = nodes.first();
        const pathId = path.join(".");

        if (nodes.length === 0) {
            context.addError({
                code: "selector_not_found",
                severity: "warning",
                message: `Selector "${selector}" matched no nodes at path .${pathId}.selector`,
                path: [...path, "selector"],
            });
        }

        if (nodes.length > 1) {
            context.addError({
                code: "multiple_nodes_found",
                severity: "warning",
                message: `Selector "${selector}" matched multiple nodes at path .${pathId}.selector`,
                path: [...path, "selector"],
            });
        }

        if (firstNode.children().length > 0) {
            const currentTag = firstNode.prop("tagName");
            const childTags = firstNode.children().map((index, child) => context.$(child).prop("tagName")).get().join(", ");
            // Node is not a leaf node, but contains children
            context.addError({
                code: "node_contains_children",
                severity: "warning",
                message: `Selector "${selector}" matched a non-leaf node ${currentTag} with ${firstNode.children().length} children node(s) (${childTags}) at path .${pathId}.selector`,
                path: [...path, "selector"],
            });
        }

        if (firstNode.length === 0) {
            return null;
        }

        const value = parseFloat(firstNode.text().trim());
        if (isNaN(value)) {
            context.addError({
                code: "node_contains_non_numeric_value",
                severity: "warning",
                message: `Selector "${selector}" matched a node with non-numeric value at path .${pathId}.selector`,
                path: [...path, "selector"],
            });
            return null;
        }

        return value;
    },
    defaultValue: (previousValue) => {
        if (numberType.isOfType(previousValue)) {
            return previousValue;
        }

        return {
            type: "number",
            selector: hasSelector(previousValue) ? previousValue.selector : "",
        }
    }
} as const;