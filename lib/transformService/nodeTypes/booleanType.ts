import {NodeTypeDefinition} from "@/lib/transformService/nodeTypes/types";
import {z} from "zod";
import {hasSelector, selectorSchema} from "@/lib/transformService/nodeTypes/selectorSchema";

type BooleanNodeType = {
    type: "boolean",
    selector: string,
};

export const booleanType: NodeTypeDefinition<BooleanNodeType> = {
    isOfType: (nodeType: any): nodeType is BooleanNodeType => nodeType?.type === "boolean",
    typeSchema: (nodeSchema) => z.object({
        selector: selectorSchema,
        type: z.literal("boolean"),
    }),
    transformNode: (self, contextNode, schema, path, context) => {
        const selector = schema.selector;
        const nodes = selector === "" ? contextNode : contextNode.find(schema.selector);
        return nodes.length > 0;
    },
    defaultValue: (previousValue) => {
        if (booleanType.isOfType(previousValue)) {
            return previousValue;
        }

        return {
            type: "boolean",
            selector: hasSelector(previousValue) ? previousValue.selector : "",
        }
    }
} as const;