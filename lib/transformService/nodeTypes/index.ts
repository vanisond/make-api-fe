// Option 3: Use a Record of all possible types
import {z} from "zod";
import {ExtractNodeType} from "@/lib/transformService/nodeTypes/types";
import {stringType} from "@/lib/transformService/nodeTypes/stringType";
import {numberType} from "@/lib/transformService/nodeTypes/numberType";
import {objectType} from "@/lib/transformService/nodeTypes/objectType";
import {arrayType} from "@/lib/transformService/nodeTypes/arrayType";
import {booleanType} from "@/lib/transformService/nodeTypes/booleanType";
import {nullType} from "@/lib/transformService/nodeTypes/nullType";

/**
 * You can register new node types here
 */
export const nodeTypeSchemas = {
    string: stringType,
    number: numberType,
    boolean: booleanType,
    object: objectType,
    array: arrayType,
    null: nullType,
} as const;

export type NodeSchemaType<TType extends keyof typeof nodeTypeSchemas = keyof typeof nodeTypeSchemas> = ExtractNodeType<typeof nodeTypeSchemas[TType]>

export type NodeType = NodeSchemaType["type"];


// Recursive schema must be defined lazily
export const nodeSchema: z.ZodType<NodeSchemaType> = z.lazy((...arg) => {
    // 1. One option is to manually define the union of all possible types
    // const s = stringType.typeSchema(nodeSchema);
    // const n = numberType.typeSchema(nodeSchema);
    // const b = booleanType.typeSchema(nodeSchema);
    // const o = objectType.typeSchema(nodeSchema);
    // const a = arrayType.typeSchema(nodeSchema);
    // const f = nullType.typeSchema(nodeSchema);
    // return z.union([s, n, b, o, a, f]);

    // 2. Another option is to spread the array of all possible types, but this needs a type assertion `as any`
    const typesArray = Object.values(nodeTypeSchemas).map(nodeType => nodeType.typeSchema(nodeSchema))
    return z.union(typesArray as any);
});

export type TransformationSchemaNodeType = z.infer<typeof nodeSchema>;
