import {Prisma} from "@prisma/client";
import prisma from "@/lib/prisma";
import {TransformationSchemaNodeType} from "@/lib/transformService/nodeTypes";

export const getTransformationSchemas = async (documentId?: string) => {
    return prisma.transformationSchema.findMany({
        where: {
            projectDocumentId: documentId,
        },
    });

}

export const getTransformationSchemaById = async (id: string) => {
    return prisma.transformationSchema.findUnique({
        where: {
            id
        },
    });
}

export const createRootTransformationSchema = async (documentId: string) => {
    const defaultRootTransformationSchema: TransformationSchemaNodeType = {
        type: "object",
        selector: "body",
        properties: []
    }

    return prisma.transformationSchema.create({
        data: {
            projectDocumentId: documentId,
            name: undefined,
            schema: defaultRootTransformationSchema
        }
    });
}

export const getRootTransformationSchema = async (documentId: string) => {
    let rootSchema = await prisma.transformationSchema.findFirst({
        where: {
            projectDocumentId: documentId,
            name: undefined,
        },
    });

    if (!rootSchema) {
        rootSchema = await createRootTransformationSchema(documentId);
    }

    return rootSchema;
}

export const updateTransformationSchema = async (id: string, data: Prisma.TransformationSchemaUpdateArgs['data']) => {
    return prisma.transformationSchema.update({
        where: {
            id
        },
        data: {
            ...data,
            updatedAt: new Date(),
        }
    });
}


export const createTransformationSchema = async (documentId: string, transformationSchema: Prisma.JsonNullValueInput) => {
    return prisma.transformationSchema.create({
        data: {
            projectDocumentId: documentId,
            name: "Untitled",
            schema: transformationSchema
        }
    });
}
