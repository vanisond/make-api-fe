import {Prisma, RateLimitType} from "@prisma/client";
import prisma from "@/lib/prisma";
import {RateLimitSchemaType} from "@/app/(forms)/schema";
import {getProjectAccessTokenById} from "@/lib/repository/projectAccessTokens";
import {getUserById, updateUser} from "@/lib/repository/users";

export const createRateLimit = async (data: RateLimitSchemaType & { type: RateLimitType }) => {
    return prisma.rateLimit.create({data});
}

export const getRateLimit = async (id?: string) => {
    if (!id) {
        return null;
    }

    return prisma.rateLimit.findUnique({
        where: {
            id,
        },
    });
}

export const updateRateLimit = async (id: string, data: Prisma.RateLimitUpdateArgs['data']) => {
    return prisma.rateLimit.update({
        where: {
            id,
        },
        data,
    });
}

export const updateUserRateLimit = async (userId: string, data: RateLimitSchemaType) => {
    const user = await getUserById(userId);

    if (!user) {
        throw new Error('User not found');
    }

    if (user.apiRequestsPerUserRateLimitId) {
        return await updateRateLimit(user.apiRequestsPerUserRateLimitId, {
            ...data,
            type: RateLimitType.USER,
        });
    } else {
        return prisma.$transaction(async (prisma) => {
            const rateLimit = await createRateLimit({
                ...data,
                type: RateLimitType.USER,
            });
            return updateUser(userId, {
                apiRequestsPerUserRateLimitId: rateLimit.id,
            })
        });
    }
}
export const updateAccessTokenRateLimit = async (accessTokenId: string, data: RateLimitSchemaType) => {
    const accessToken = await getProjectAccessTokenById(accessTokenId);
    if (!accessToken) {
        throw new Error('User not found');
    }

    if (accessToken.apiRequestsPerTokenRateLimitId) {
        return await updateRateLimit(accessToken.apiRequestsPerTokenRateLimitId, {
            ...data,
            type: RateLimitType.ACCESS_TOKEN,
        });
    } else {
        return prisma.$transaction(async (prisma) => {
            const rateLimit = await createRateLimit({
                ...data,
                type: RateLimitType.ACCESS_TOKEN,
            });
            return prisma.projectAccessToken.update({
                where: {
                    id: accessTokenId,
                },
                data: {
                    apiRequestsPerTokenRateLimitId: rateLimit.id,
                },
            });
        });
    }
}
