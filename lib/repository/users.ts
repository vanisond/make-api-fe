import prisma from "@/lib/prisma";
import {Prisma} from "@prisma/client";

export const getAllUsers = async () => {
    return prisma.user.findMany({
        orderBy: {
            id: "desc",
        }
    });
}

export const getUserById = async (id: string) => {
    return prisma.user.findUnique({
        where: {
            id,
        },
    });
}

export const updateUser = async (id: string, data: Prisma.UserUpdateArgs['data']) => {
    return prisma.user.update({
        where: {
            id,
        },
        data,
    });
}

export const deleteUser = async (id: string) => {
    return prisma.user.delete({
        where: {
            id,
        },
    });
}
