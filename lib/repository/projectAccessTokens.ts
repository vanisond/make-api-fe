import prisma from "@/lib/prisma";
import {compare, hash} from "bcrypt";
import {ProjectAccessTokensSchemaType} from "@/app/(forms)/schema";

export const createProjectAccessToken = async (data: ProjectAccessTokensSchemaType) => {
    if (!data.token) {
        throw new Error("Token is required");
    }
    const token = await hash(data.token, 10);
    const allowedOrigins = data.allowedOrigins?.split("\n").filter((line) => line.trim() !== "");
    return prisma.projectAccessToken.create({
        data: {
            ...data,
            allowedOrigins,
            token,
        }
    });
}

export const getProjectAccessTokens = async (projectId: string) => {
    return prisma.projectAccessToken.findMany({
        where: {
            projectId
        }
    });
}

export const getProjectAccessTokenById = async (id: string) => {
    return prisma.projectAccessToken.findUnique({
        where: {
            id
        },
        include: {
            project: true
        }
    });
}

export const updateProjectAccessToken = async (id: string, data: {
    description?: string,
    updatedAt?: Date,
    lastUsedAt?: Date,
    expiresAt?: Date,
    allowedOrigins?: string,
}) => {
    const allowedOrigins = data.allowedOrigins?.split("\n").filter((line) => line.trim() !== "");
    return prisma.projectAccessToken.update({
        where: {
            id,
        },
        data: {
            ...data,
            allowedOrigins,
        },
    });

}

export const deleteProjectAccessToken = async (id: string) => {
    return prisma.projectAccessToken.delete({
        where: {
            id,
        },
    });
}

export const verifyProjectAccessToken = async (projectId: string, accessToken?: string) => {
    if (!accessToken) {
        return null;
    }

    const relatedAccessTokens = await getProjectAccessTokens(projectId);
    let validToken = null;
    for (const item of relatedAccessTokens) {
        if (await compare(accessToken, item.token)) {
            validToken = item;
            break; // Exit the loop once a match is found
        }
    }

    if (!validToken) {
        return null;
    }

    // Delete the token if it's expired
    if (validToken.expiresAt && validToken.expiresAt < new Date()) {
        await deleteProjectAccessToken(validToken?.id);
        return null;
    }

    const token = await updateProjectAccessToken(validToken.id, {lastUsedAt: new Date()});
    return token;
}
