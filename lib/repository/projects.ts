import prisma from "@/lib/prisma";

export const getUserProjects = async (userId?: string) => {
    return prisma.project.findMany({
        where: {
            ownerId: userId,
        },
    });

}

export const getProjectById = async (id: string) => {
    return prisma.project.findUnique({
        where: {
            id,
        },
    });
}

export const createProject = async (ownerId: string, data: { name: string }) => {
    return prisma.project.create({
        data: {
            ...data,
            ownerId,
        },
    });
}

export const updateProject = async (id: string, data: { name: string }) => {
    return prisma.project.update({
        where: {
            id,
        },
        data,
    });
}

export const deleteProject = async (id: string) => {
    return prisma.project.delete({
        where: {
            id,
        },
    });

}

