import prisma from "@/lib/prisma";
import {ProjectDocumentSchemaType} from "@/app/(forms)/schema";
import {Prisma} from "@prisma/client";

export const getProjectDocuments = async (projectId?: string) => {
    return prisma.projectDocument.findMany({
        where: {
            projectId,
        },
    });

}

export const getProjectDocumentById = async (documentId: string) => {
    return prisma.projectDocument.findUnique({
        where: {
            id: documentId,
        },
    });
}

export const createProjectDocument = async (data: ProjectDocumentSchemaType) => {
    return prisma.projectDocument.create({data});
}

export const getProjectDocumentsCount = async (projectIds: string[]) => {
    const counts = await prisma.projectDocument.groupBy({
        by: ['projectId'],
        _count: {
            id: true,
        },
        where: {
            projectId: {
                in: projectIds,
            },
        },
    });
    return counts.reduce((acc, cur) => {
        acc[cur.projectId] = cur._count.id;
        return acc;
    }, {} as Record<string, number>);
}

export const getDisabledProjectDocumentsCount = async (projectIds: string[]) => {
    const counts = await prisma.projectDocument.groupBy({
        by: ['projectId'],
        _count: {
            id: true
        },
        where: {
            projectId: {
                in: projectIds,
            },
            transformDisabled: {
                equals: true,
            }
        },
    });
    return counts.reduce((acc, cur) => {
        acc[cur.projectId] = cur._count.id;
        return acc;
    }, {} as Record<string, number>);
}

export const updateProjectDocument = async (id: string, data: Prisma.ProjectDocumentUpdateArgs['data']) => {
    return prisma.projectDocument.update({
        where: {
            id,
        },
        data: {
            ...data,
            updatedAt: new Date(),
        },
    });
}

export const deleteProjectDocument = async (id: string) => {
    return prisma.projectDocument.delete({
        where: {
            id,
        },
    });
}

export const disableProjectDocumentTransform = async (id: string, transformDisabled = true) => {
    return prisma.projectDocument.update({
        where: {
            id,
        },
        data: {
            transformDisabled,
            // Don't update updatedAt
        },
    });
}

export const getTransformFailure = async (documentId: string) => {
    return prisma.transformFailure.findUnique({
        where: {
            documentId,
        },
    });
}

export const upsertTransformFailure = async (documentId: string, data: Awaited<ReturnType<typeof getTransformFailure>>) => {
    return prisma.transformFailure.upsert({
        where: {
            documentId,
        },
        update: {
            documentId,
            ...data,
        },
        create: {
            documentId,
            ...data,
        },
    });
}

export const deleteTransformFailure = async (documentId: string) => {
    try {
        return await prisma.transformFailure.delete({
            where: {
                documentId,
            },
        });
    } catch (error) {
        if (error instanceof Prisma.PrismaClientKnownRequestError && error.code === 'P2025') {
            // Ignore if record does not exist
            return;
        }
        throw error;
    }
}
