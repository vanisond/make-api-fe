import {addMilliseconds, isAfter} from "date-fns";
import {
    deleteTransformFailure,
    disableProjectDocumentTransform,
    getTransformFailure,
    upsertTransformFailure
} from "@/lib/repository/documents";
import {sendMailToUser} from "@/lib/emailService/nodemailer";
import {getProjectById} from "@/lib/repository/projects";

const failureTimeouts = {
    disableDocumentMs: parseInt(process.env.TRANSFORM_FAILURE_DISABLE_DOCUMENT_TIMEOUT_MS ?? '0'),
    sendMailReport: parseInt(process.env.TRANSFORM_FAILURE_SEND_MAIL_REPORT_TIMEOUT_MS ?? '0'),
}

type ReporterOptions = {
    projectId: string,
}

export const createFailureReporter = (documentId: string, {projectId}: ReporterOptions) => {
    const reportTransformFailure = async (reasonCode: string) => {
        const now = new Date();
        let failureRecord = await getTransformFailure(documentId);
        if (!failureRecord) {
            failureRecord = {
                documentId,
                firstOccurrence: now,
                lastOccurrence: null,
                transformDisabledAt: null,
                notificationSent: null,
            };
        }

        // Update last failure record
        if (!failureRecord.transformDisabledAt) {
            failureRecord.lastOccurrence = now;
        }

        // Disable document if timeout has passed
        if (!failureRecord.transformDisabledAt && isAfter(now, addMilliseconds(failureRecord.firstOccurrence, failureTimeouts.disableDocumentMs))) {
            await disableProjectDocumentTransform(documentId, true);
            failureRecord.transformDisabledAt = now;
        }

        // Send email if timeout has passed
        if (!failureRecord.notificationSent && isAfter(now, addMilliseconds(failureRecord.firstOccurrence, failureTimeouts.sendMailReport))) {
            const project = await getProjectById(projectId);
            const userId = project?.ownerId;
            if (!userId) {
                throw new Error(`Project ${projectId} has no owner`);
            }
            try {
                await sendMailToUser(userId, {
                    subject: "Transform failure",
                    text: `Transform failure for document ${documentId} with reason code ${reasonCode}.`,
                });
                failureRecord.notificationSent = now;
            } catch (error) {
                if (error instanceof Error) {
                    console.error(`Failed to send email to user ${userId}: ${error.message}`);
                }
            }
        }

        // Update failure record
        await upsertTransformFailure(documentId, failureRecord);
    }

    const resetTransformFailure = async () => {
        await disableProjectDocumentTransform(documentId, false);
        return deleteTransformFailure(documentId);
    }

    return {
        reportTransformFailure,
        resetTransformFailure,
    }
}
