import {DocumentDownloadStrategy} from "@prisma/client";
import axios, {AxiosError} from "axios";

export const fetchHtml = async (url: string, strategy: DocumentDownloadStrategy) => {
    try {
        const res = strategy === DocumentDownloadStrategy.HEADLESS
            ? await axios.get(`${process.env.PAGE_FETCHER_SERVICE_BASE_URL}/html?url=${url}`, {responseType: "document"})
            : await axios.get(`${process.env.PAGE_FETCHER_SERVICE_BASE_URL}/raw?url=${url}`, {responseType: "document"}); // type "document" ensures that the response is parsed as string, and not as JSON

        return {
            success: [200, 304].includes(res.status),
            status: res.status,
            data: res.data,
            headers: res.headers,
        };
    } catch (error) {
        // For Axios errors, e.g. when the status code is 4xx or 5xx
        if (error instanceof AxiosError) {
            return {
                success: false,
                error: error,
                status: error.response?.status,
                data: null,
                headers: error.response?.headers,
            };
        }

        // Unknown error
        return {
            success: false,
            error: error,
            status: null,
            data: null,
            headers: null,
        };
    }
}
