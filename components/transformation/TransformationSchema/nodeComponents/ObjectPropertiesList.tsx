import React from "react";
import {TransformationNode} from "@/components/transformation/TransformationSchema/TransformationNode";
import {MdAdd, MdDelete, MdLabel} from "react-icons/md";
import {objectType} from "@/lib/transformService/nodeTypes/objectType";
import {ExtractNodeType} from "@/lib/transformService/nodeTypes/types";
import {TransformationSchemaNodeType} from "@/lib/transformService/nodeTypes";
import {PathProvider, usePath} from "@/components/transformation/TransformationSchema/PathContext";
import {useTransformationContext} from "@/components/transformation/TransformationSchema/TransformationContext";
import {Input} from "@/components/ui/input";
import {Button} from "@/components/ui/button";
import {cx} from "class-variance-authority";


type ObjectPropertiesListProps = {
    properties: ExtractNodeType<typeof objectType>["properties"]
    onPropertiesUpdate: (properties: ExtractNodeType<typeof objectType>["properties"]) => void
}

const ObjectPropertyInput: React.FC<{
    property: string,
    onChange: (event: React.ChangeEvent<HTMLInputElement>) => void
}> = ({property, onChange}) => {
    const {path} = usePath();
    const {schemaValidationErrors, transformationErrors} = useTransformationContext([...path, "property"]);
    const hasError = schemaValidationErrors.length > 0 || transformationErrors.length > 0;

    return (
        <div className="relative flex items-center gap-2">
            <MdLabel className="absolute left-2 w-4 h-4 text-gray-400"/>
            <Input
                placeholder="Property name..."
                type="text"
                onChange={onChange}
                required
                value={property}
                className={cx("pl-7 h-9", {"border-red-500": hasError})}
            />
        </div>
    );

}

export const ObjectPropertiesList: React.FC<ObjectPropertiesListProps> = ({properties, onPropertiesUpdate}) => {
    const {path} = usePath();

    const handlePropertyChange = (index: number) => (event: React.ChangeEvent<HTMLInputElement>) => {
        let newProperties = [...properties];
        newProperties[index] = {...newProperties[index], property: event.target.value};
        onPropertiesUpdate(newProperties);
    }

    const handleUpdateNode = (index: number) => (node: TransformationSchemaNodeType) => {
        const newProperties = [...properties];
        newProperties[index] = {
            property: newProperties[index].property,
            ...node,
        };
        onPropertiesUpdate(newProperties);
    }

    const handleRemoveProperty = (index: number) => () => {
        onPropertiesUpdate(properties.filter((_, i) => i !== index))
    }

    const handleAddProperty = () => {
        onPropertiesUpdate([...properties, {property: "", type: "string", selector: ""}])
    }

    return (
        <ol className="relative border-l border-gray-300 ml-1 pt-2 mb-2">
            {properties.length === 0 && (
                <li className="mb-2 ml-4">
                    <div
                        className={"absolute w-2 h-2 bg-gray-100 rounded-full mt-2.5 -left-1 border border-gray-300"}></div>
                    <div className="py-1 italic text-gray-400 text-sm">
                        <span>This object is empty.</span>
                    </div>
                </li>)
            }

            {properties.map(({property, ...node}, index) => (
                <PathProvider key={path.join(".") + index} path={[...path, "properties", index]}>
                    <li className="mb-2 ml-4">
                        <div
                            className="absolute w-2 h-2 bg-gray-100 rounded-full mt-3.5 -left-1 border border-gray-300"/>

                        <div className="flex items-center gap-2">
                            <div className="flex-1">
                                <ObjectPropertyInput
                                    property={property}
                                    onChange={handlePropertyChange(index)}
                                />
                            </div>
                            <button onClick={handleRemoveProperty(index)}>
                                <MdDelete
                                    className="w-8 h-8 text-gray-400 hover:text-red-700 p-1.5 -ml-1.5"
                                />
                                <span className="sr-only">
                                    Remove property
                                </span>
                            </button>

                        </div>
                        <TransformationNode node={node} onSchemaUpdate={handleUpdateNode(index)}/>
                        {index !== properties.length - 1 && (
                            <div className="h-2"></div>
                        )}

                    </li>
                </PathProvider>
            ))}

            <li key={"add-property"} className="mb-2 ml-4">
                <Button
                    variant="outline"
                    size="sm"
                    onClick={handleAddProperty}
                >
                    <MdAdd className="w-4 h-4 mr-1"/>
                    Add Property
                </Button>
            </li>
        </ol>
    );
}