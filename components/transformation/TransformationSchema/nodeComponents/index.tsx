import {NodeType} from "@/lib/transformService/nodeTypes";
import React from "react";
import {TransformationNodeProps} from "@/components/transformation/TransformationSchema/TransformationNode";
import {LiteralNode} from "@/components/transformation/TransformationSchema/nodeComponents/LiteralNode";
import {NullNode} from "@/components/transformation/TransformationSchema/nodeComponents/NullNode";
import {ArrayNode} from "@/components/transformation/TransformationSchema/nodeComponents/ArrayNode";
import {ObjectNode} from "@/components/transformation/TransformationSchema/nodeComponents/ObjectNode";

const nodeComponents: { [nodeType in NodeType]: React.ComponentType<TransformationNodeProps> } = {
    string: LiteralNode,
    number: LiteralNode,
    boolean: LiteralNode,
    object: ObjectNode,
    array: ArrayNode,
    null: NullNode,
}

export default nodeComponents;
