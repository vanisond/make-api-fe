import React from "react";
import {TransformationNodeProps} from "@/components/transformation/TransformationSchema/TransformationNode";
import {ExtractNodeType} from "@/lib/transformService/nodeTypes/types";
import {hasSelector} from "@/lib/transformService/nodeTypes/selectorSchema";
import {objectType} from "@/lib/transformService/nodeTypes/objectType";
import {TypeInput} from "@/components/transformation/TransformationSchema/nodeComponents/TypeInput";
import {SelectorInput} from "@/components/transformation/TransformationSchema/nodeComponents/SelectorInput";
import {
    ObjectPropertiesList
} from "@/components/transformation/TransformationSchema/nodeComponents/ObjectPropertiesList";

export const ObjectNode: React.FC<TransformationNodeProps> = ({node, onSchemaUpdate}) => {
    if (!objectType.isOfType(node)) {
        return null;
    }

    const handlePropertiesUpdate = (properties: ExtractNodeType<typeof objectType>["properties"]) => {
        onSchemaUpdate({...node, selector: hasSelector(node) ? node.selector : "", type: "object", properties});
    }

    return (<div>
        <div className="flex gap-2 items-center py-2">
            <div className="shrink-1">
                <TypeInput node={node} onSchemaUpdate={onSchemaUpdate}/>
            </div>
            <SelectorInput onSchemaUpdate={onSchemaUpdate} node={node}/>
        </div>

        {objectType.isOfType(node) && (
            <div className="px-2 bg-gray-100 rounded-md border border-gray-200">
                <div className={"text-xs text-gray-500 py-1 px-2.5 -ml-2 -mr-2 bg-gray-200"}>Object properties</div>
                <ObjectPropertiesList onPropertiesUpdate={handlePropertiesUpdate} properties={node.properties ?? [{
                    property: "prop1",
                    type: "string",
                    selector: ""
                }]}/>
            </div>
        )}
    </div>)
}