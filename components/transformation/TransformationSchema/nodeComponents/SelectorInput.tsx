import React, {useEffect, useMemo, useRef} from 'react';
import {MdNearMe, MdSelectAll} from "react-icons/md";
import {TransformationNodeProps} from "@/components/transformation/TransformationSchema/TransformationNode";
import {hasSelector} from "@/lib/transformService/nodeTypes/selectorSchema";
import {useTransformationContext} from "@/components/transformation/TransformationSchema/TransformationContext";
import {usePath} from "@/components/transformation/TransformationSchema/PathContext";
import {Input} from "@/components/ui/input";
import {cx} from "class-variance-authority";
import DOMPurify from "dompurify";
import {Dialog, DialogContent, DialogHeader, DialogTitle, DialogTrigger} from "@/components/ui/dialog";
import {useAppSelector} from "@/redux/hooks";
import * as cheerio from "cheerio";

const IframeViewer: React.FC<{
    onSelectedElement: (selector: string) => void
}> = ({onSelectedElement}) => {
    const iframeRef = useRef<HTMLIFrameElement>(null);

    const {path} = usePath();
    const schema = useAppSelector(state => state.transformationSchema.schema);

    const selectors = useMemo(() => {
        if (path.length === 0) return [];
        return path.reduce((acc, key, i) => {
            const x = acc.currentNode;

            const {value} = Object.getOwnPropertyDescriptor(acc.currentNode, key)!;
            return {
                selector: value.selector ? [...acc.selector, value.selector] : acc.selector,
                currentNode: value
            };


        }, {selector: hasSelector(schema) ? [schema.selector] : [], currentNode: schema}).selector;
    }, [path, schema]);

    const pageHTML = useAppSelector(state => state.transformationSchema.pageHTML);
    const $ = cheerio.load(pageHTML ?? "");
    const htmFragment = $(":root " + selectors.slice(0,-1).join(" ")).html()

    const highlightSrc = `
        <style>
            * {
                border: 1px dashed rgba(0, 0, 0, 0.1);
                padding: 3px;
                margin: 1px;
                border-radius: 2px;
            }
            img {
                max-height: 35% !important;
                max-width: 35% !important;
            }
            .highlighted-element {
                position: relative;
                outline: 2px solid red;
                z-index: 9999;
                cursor: pointer;
            }
            .highlighted-element::after {
                content: '';
                position: absolute;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                background-color: rgba(255, 0, 0, 0.2);
                pointer-events: none;
            }
        </style>
        <script type="module">
          import * as CssSelectorGenerator from 'https://cdn.jsdelivr.net/npm/css-selector-generator@3.6.6/+esm'
          document.addEventListener('DOMContentLoaded', (event) => {
            document.body.addEventListener('mouseover', (e) => {
              e.target.classList.add('highlighted-element');
            });
            document.body.addEventListener('mouseout', (e) => {
              e.target.classList.remove('highlighted-element');
            });
            document.addEventListener('click', function (e) {
              e.preventDefault();
              e.target.classList.remove('highlighted-element');
              window.parent.postMessage({
                type: 'element-selected',
                selectedElement : CssSelectorGenerator.getCssSelector(e.target)
              }, '*');
              e.target.classList.add('highlighted-element');
            }, true); // Use capture to ensure this runs before other click events
          });
        </script>
    `;

    useEffect(() => {
        const handleMessage = (event: MessageEvent<any>) => {
            // Handle the message from the iframe
            if (event.data?.type === 'element-selected') {
                onSelectedElement(event.data?.selectedElement ?? "");
            }
        };
        window.addEventListener('message', handleMessage);
        return () => {
            window.removeEventListener('message', handleMessage);
        };
    }, [onSelectedElement]);


    return (
        <>
            <iframe
                ref={iframeRef}
                sandbox="allow-scripts"
                srcDoc={highlightSrc + DOMPurify.sanitize(htmFragment ?? "")}
                className="w-full h-full"
            />
        </>
    );
}


export const SelectElementDialog: React.FC<TransformationNodeProps> = ({node, onSchemaUpdate}) => {
    const pageHTML = useAppSelector(state => state.transformationSchema.pageHTML);

    const [open, setOpen] = React.useState(false);
    return (
        <Dialog open={open} onOpenChange={setOpen}>
            <DialogTrigger asChild>
                <div className="cursor-pointer">
                    <MdSelectAll
                        className="w-8 h-8 text-gray-400 hover:text-red-700 p-1.5 -ml-1.5"
                    />
                    <span className="sr-only">Select element using page viewer</span>
                </div>
            </DialogTrigger>
            <DialogContent className="max-w-[1200px] w-[60vw] max-h-[1000px] h-[70vh] flex flex-col">
                <DialogHeader className="shrink">
                    <DialogTitle>Select element</DialogTitle>
                </DialogHeader>
                <IframeViewer onSelectedElement={(selector) => {
                    if (hasSelector(node)) {
                        onSchemaUpdate({...node, selector});
                    }
                    setOpen(false);
                }}/>
            </DialogContent>
        </Dialog>
    );
};


export const SelectorInput: React.FC<TransformationNodeProps> = ({node, onSchemaUpdate}) => {
    const {path} = usePath();
    const {schemaValidationErrors, transformationErrors} = useTransformationContext([...path, "selector"]);
    const hasError = schemaValidationErrors.length > 0 || transformationErrors.length > 0;
    const hasMultipleNodes = transformationErrors.find(({code}) => code === "multiple_nodes_found");
    const hasChildren = transformationErrors.find(({code}) => code === "node_contains_children");

    if (!hasSelector(node)) {
        return null;
    }

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        if (!hasSelector(node)) return;
        onSchemaUpdate({...node, selector: event.target.value});
    };

    return (
        <div className="flex-1">
            <div className="relative flex items-center gap-2">
                <MdNearMe className="absolute left-2 w-4 h-4 text-gray-400"/>
                <Input
                    type="text"
                    onChange={handleChange}
                    placeholder="CSS Selector..."
                    required
                    value={node.selector}
                    className={cx("pl-7 h-9", {"border-red-500": hasError})}
                />
                {/* TODO: WIP */}
                {/*<SelectElementDialog node={node} onSchemaUpdate={onSchemaUpdate}/>*/}
            </div>
            {hasMultipleNodes && (
                <div className="text-xs text-muted-foreground">Try adding :nth-of-type(1) or :first()</div>
            )}
            {hasChildren && (
                <div className="text-xs text-muted-foreground">Try adding :not(:has(*))</div>
            )}
        </div>
    )
}