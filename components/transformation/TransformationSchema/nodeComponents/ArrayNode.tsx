import React from "react";
import {ExtractNodeType} from "@/lib/transformService/nodeTypes/types";
import {arrayType} from "@/lib/transformService/nodeTypes/arrayType";
import {hasSelector} from "@/lib/transformService/nodeTypes/selectorSchema";
import {TypeInput} from "@/components/transformation/TransformationSchema/nodeComponents/TypeInput";
import {SelectorInput} from "@/components/transformation/TransformationSchema/nodeComponents/SelectorInput";
import {
    TransformationNode,
    TransformationNodeProps
} from "@/components/transformation/TransformationSchema/TransformationNode";
import {PathProvider, usePath} from "@/components/transformation/TransformationSchema/PathContext";

export const ArrayNode: React.FC<TransformationNodeProps> = ({node, onSchemaUpdate}) => {
    const {path} = usePath();

    if (!arrayType.isOfType(node)) {
        return null;
    }

    const handleItemsChange = (items: ExtractNodeType<typeof arrayType>["items"]) => {
        onSchemaUpdate({...node, selector: hasSelector(node) ? node.selector : "", type: "array", items});
    }

    return (<div>
        <div className="flex gap-2 items-center py-2">
            <div className="shrink-1">
                <TypeInput node={node} onSchemaUpdate={onSchemaUpdate}/>
            </div>
            <SelectorInput onSchemaUpdate={onSchemaUpdate} node={node}/>
        </div>
        {arrayType.isOfType(node) && (
            <div className="px-2 pb-2 bg-gray-100 rounded-md border border-gray-200">
                <div className={"text-xs text-gray-500 py-1 px-2.5 -ml-2 -mr-2 bg-gray-200"}>Array items</div>
                <PathProvider path={[...path, "items"]}>
                    <TransformationNode
                        onSchemaUpdate={handleItemsChange}
                        node={node.items ?? {type: "string", selector: ""}}
                    />
                </PathProvider>
            </div>
        )}
    </div>)
}