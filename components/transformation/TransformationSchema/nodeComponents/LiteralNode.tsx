import React from "react";
import {hasSelector} from "@/lib/transformService/nodeTypes/selectorSchema";
import {TypeInput} from "@/components/transformation/TransformationSchema/nodeComponents/TypeInput";
import {SelectorInput} from "@/components/transformation/TransformationSchema/nodeComponents/SelectorInput";
import {TransformationNodeProps} from "@/components/transformation/TransformationSchema/TransformationNode";


export const LiteralNode: React.FC<TransformationNodeProps> = ({node, onSchemaUpdate}) => {
    return (
        <div className="flex gap-2 items-center py-2">
            <div className="shrink-1">
                <TypeInput node={node} onSchemaUpdate={onSchemaUpdate}/>
            </div>
            {hasSelector(node) && (
                <SelectorInput onSchemaUpdate={onSchemaUpdate} node={node}/>
            )}
        </div>
    )
}