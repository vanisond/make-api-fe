import React from "react";
import {TransformationNodeProps} from "@/components/transformation/TransformationSchema/TransformationNode";
import {TypeInput} from "@/components/transformation/TransformationSchema/nodeComponents/TypeInput";


export const NullNode: React.FC<TransformationNodeProps> = ({node, onSchemaUpdate}) => {
    return (
        <div className="flex gap-2 py-2">
            <div className="shrink-1">
                <TypeInput node={node} onSchemaUpdate={onSchemaUpdate}/>
            </div>
        </div>
    )
}