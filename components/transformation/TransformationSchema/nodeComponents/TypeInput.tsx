import React, {useMemo} from "react";
import {NodeType, nodeTypeSchemas} from "@/lib/transformService/nodeTypes";
import {
    MdAbc,
    MdCheckBoxOutlineBlank,
    MdDataArray,
    MdDataObject,
    MdNumbers,
    MdRadioButtonChecked,
    MdSpaceBar
} from "react-icons/md";
import type {TransformationNodeProps} from "@/components/transformation/TransformationSchema/TransformationNode";
import {Select, SelectContent, SelectItem, SelectTrigger, SelectValue} from "@/components/ui/select";

export const typeOptions: { label: string, value: NodeType, Icon: React.ElementType }[] = [
    {label: "string", value: "string", Icon: MdAbc},
    {label: "number", value: "number", Icon: MdNumbers},
    {label: "object", value: "object", Icon: MdDataObject},
    {label: "array", value: "array", Icon: MdDataArray},
    {label: "boolean", value: "boolean", Icon: MdRadioButtonChecked},
    {label: "null", value: "null", Icon: MdSpaceBar},
];

export const TypeInput: React.FC<TransformationNodeProps> = ({node, onSchemaUpdate}) => {
    const handleTypeChange = (input: string) => {
        const type = input as NodeType;
        const newValue = nodeTypeSchemas[type].defaultValue(node);
        onSchemaUpdate(newValue);
    }

    const SelectedTypeIcon = useMemo(() => {
        const selectedType = typeOptions.find(({value}) => value === node.type);
        return selectedType?.Icon ?? MdCheckBoxOutlineBlank;
    }, [node.type]);

    return (
        <Select value={node.type} onValueChange={handleTypeChange}>
            <SelectTrigger className="h-9 w-[140px]">
                <SelectValue placeholder="Type...  "/>
            </SelectTrigger>
            <SelectContent>
                {typeOptions.map(({label, value, Icon}) => (
                    <SelectItem key={value} value={value}>
                        <span className="flex gap-2 items-center">
                            <Icon className="text-gray-500"/>
                            {label}
                        </span>
                    </SelectItem>
                ))}
            </SelectContent>
        </Select>
    )
}
