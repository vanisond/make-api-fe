import React, {createContext, useContext, useMemo} from "react";
import {ZodIssue} from "zod";
import {TransformContextErrorType} from "@/lib/transformService/transform";
import {useAppSelector} from "@/redux/hooks";

interface TransformationContextType {
    schemaValidationErrors: ZodIssue[],
    transformationErrors: TransformContextErrorType[],
}

const TransformationContext = createContext<TransformationContextType>({
    schemaValidationErrors: [],
    transformationErrors: []
});

export const useTransformationContext = (path: (string | number)[]) => {
    const context = useContext(TransformationContext);

    const transformationErrors = context.transformationErrors.filter(error => error.path.join(".") === path.join("."));
    const schemaValidationErrors = context.schemaValidationErrors.filter(issue => issue.path.join(".") === path.join("."));

    return {
        path: path.join("."),
        originalSchemaValidationErrors: context.schemaValidationErrors,
        originalTransformationErrors: context.transformationErrors,
        schemaValidationErrors,
        transformationErrors,
    }
}

type TransformationProviderProps = {
    children: React.ReactNode;
}

export const TransformationProvider: React.FC<TransformationProviderProps> = ({children}) => {
    const schemaValidationErrors = useAppSelector(state => state.transformationSchema.schemaValidationErrors);
    const transformationErrors = useAppSelector(state => state.transformationSchema.transformationErrors);
    return (
        <TransformationContext.Provider value={useMemo(() => ({
            schemaValidationErrors,
            transformationErrors
        }), [schemaValidationErrors, transformationErrors])}>
            {children}
        </TransformationContext.Provider>
    );
};
