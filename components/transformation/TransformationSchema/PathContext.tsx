import React, {createContext, useContext} from "react";

interface PathContextType {
    path: (string | number)[];
}

const PathContext = createContext<PathContextType>({ path: [] });

export const usePath = () => useContext(PathContext);

type PathProviderProps = {
    path: (string | number)[];
    children: React.ReactNode;
}

export const PathProvider: React.FC<PathProviderProps> = ({ path, children }) => {
    const value = React.useMemo(() => ({path}), [path]);
    return (
        <PathContext.Provider value={value}>
            {children}
        </PathContext.Provider>
    );
};
