import React from "react";
import {TransformationSchemaNodeType} from "@/lib/transformService/nodeTypes";
import nodeComponents from "@/components/transformation/TransformationSchema/nodeComponents";
import {usePath} from "@/components/transformation/TransformationSchema/PathContext";

export type TransformationNodeProps = {
    node: TransformationSchemaNodeType
    onSchemaUpdate: (node: TransformationSchemaNodeType) => void
}

export const TransformationNode: React.FC<TransformationNodeProps> = ({node, onSchemaUpdate}) => {
    const {path} = usePath();
    const NodeTypeComponent = nodeComponents[node.type];
    return (
        <NodeTypeComponent key={path.join(".")} node={node} onSchemaUpdate={onSchemaUpdate}/>
    );
}
