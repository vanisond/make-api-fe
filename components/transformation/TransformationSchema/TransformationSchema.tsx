"use client";

import React, {useCallback} from "react";
import {TransformationNode} from "@/components/transformation/TransformationSchema/TransformationNode";
import {useAppDispatch, useAppSelector} from "@/redux/hooks";
import {setSchema} from "@/redux/features/transformationSchema/transformationSchemaSlice";
import {TransformationSchemaNodeType} from "@/lib/transformService/nodeTypes";
import {PathProvider} from "@/components/transformation/TransformationSchema/PathContext";
import {TransformationProvider} from "@/components/transformation/TransformationSchema/TransformationContext";

export const TransformationSchema = () => {
    const dispatch = useAppDispatch();
    const schema = useAppSelector(state => state.transformationSchema.schema);

    const handleSchemaUpdate = useCallback((schema: TransformationSchemaNodeType) => {
        dispatch(setSchema(schema));
    }, [dispatch]);

    return (
        <div className="h-full flex flex-col justify-between overflow-auto">
            <div className="px-2 pb-2 m-2 bg-gray-100 rounded-md border border-gray-300">
                <div className={"text-xs text-gray-500 py-1 px-2.5 -ml-2 -mr-2 bg-gray-200"}>root</div>
                <PathProvider path={[]}>
                    <TransformationProvider>
                        <TransformationNode key="schema" node={schema} onSchemaUpdate={handleSchemaUpdate}/>
                    </TransformationProvider>
                </PathProvider>
            </div>
        </div>
    )
}