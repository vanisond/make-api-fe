import React, {useEffect} from "react";
import {useAppDispatch, useAppSelector} from "@/redux/hooks";
import {setPageHTML} from "@/redux/features/transformationSchema/transformationSchemaSlice";
import {Textarea} from "@/components/ui/textarea";

export const PageHTMLViewer = () => {
    const dispatch = useAppDispatch();
    const pageHTML = useAppSelector(state => state.transformationSchema.pageHTML);

    const handleChangePageHTML = (event: React.ChangeEvent<HTMLTextAreaElement>) => {
        dispatch(setPageHTML(event.target.value));
    }

    return (
        <div className="text-xs h-full p-2">
            <Textarea name="pageHtml" id="pageHtml" className="w-full h-full" onChange={handleChangePageHTML} value={pageHTML ?? ""} />
        </div>
    )
}