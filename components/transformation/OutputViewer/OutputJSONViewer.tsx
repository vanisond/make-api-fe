"use client"

import React, {useEffect} from "react";
import {useAppDispatch, useAppSelector} from "@/redux/hooks";
import {transformHTMLtoJSON} from "@/lib/transformService/transform";
import {nodeSchema} from "@/lib/transformService/nodeTypes";
import {MdWarning} from "react-icons/md";
import {ZodError} from "zod";
import {
    setOutputJSON,
    setSchemaValidationErrors,
    setTransformationErrors
} from "@/redux/features/transformationSchema/transformationSchemaSlice";
import SyntaxHighlighter from 'react-syntax-highlighter';
import {shortenLongStrings} from "@/lib/utils";


export const OutputJSONViewer = () => {
    const dispatch = useAppDispatch();
    const pageHTML = useAppSelector(state => state.transformationSchema.pageHTML);
    const schema = useAppSelector(state => state.transformationSchema.schema);
    const outputJSON = useAppSelector(state => shortenLongStrings(state.transformationSchema.outputJSON, 5000));
    const schemaValidationErrors = useAppSelector(state => state.transformationSchema.schemaValidationErrors);
    const transformationErrors = useAppSelector(state => state.transformationSchema.transformationErrors);

    useEffect(() => {
        if (!pageHTML || !schema) return;

        const transformationResult = transformHTMLtoJSON(pageHTML, schema);
        if (!transformationResult.success) {
            dispatch(setSchemaValidationErrors([{
                code: "custom",
                path: [],
                fatal: true,
                message: transformationResult.error ?? "Unknown error occurred during transformation"
            }]));
            return;
        }

        // Transform success
        dispatch(setOutputJSON(transformationResult.data));
        dispatch(setTransformationErrors(transformationResult.context?.errors ?? []));

        try {
            nodeSchema.parse(schema);
            dispatch(setSchemaValidationErrors([])); // clear validation errors
        } catch (error) {
            if (error instanceof ZodError) {
                dispatch(setSchemaValidationErrors(error.errors));
                return;
            } else {
                dispatch(setSchemaValidationErrors([{
                    code: "custom",
                    path: [],
                    fatal: true,
                    message: transformationResult.error ?? "Unknown error occurred during validation"
                }]));
            }
        }

    }, [pageHTML, schema]);

    return (
        <>
            {transformationErrors?.length > 0 && (
                <pre
                    className="p-2 text-xs text-yellow-800 bg-yellow-200 whitespace-break-spaces overflowWrap-anywhere">
                    {transformationErrors.map(({message, path}, index) => (
                        <div key={index} className={"flex items-top gap-2"}>
                            <MdWarning className="w-4 h-4"/>
                            <span>{message}</span>
                        </div>
                    ))}
                </pre>
            )}
            {schemaValidationErrors.length > 0 ? (
                <pre className="p-2 text-xs text-red-800 bg-red-200 whitespace-break-spaces overflowWrap-anywhere">
                    {schemaValidationErrors.map(({message, path}, index) => (
                        <div key={index} className="flex items-top gap-2">
                            <MdWarning className="w-4 h-4"/>
                            <span>{`${message} at path .${path.join(".")}`}</span>
                        </div>
                    ))}
                </pre>
            ) : (
                <SyntaxHighlighter
                    className="text-xs whitespace-break-spaces overflow-wrap-anywhere"
                    wrapLines
                    wrapLongLines
                    customStyle={{overflowX: "hidden"}}
                    language="json"
                >
                    {JSON.stringify(outputJSON, null, 2)}
                </SyntaxHighlighter>
            )}
        </>
    );

}