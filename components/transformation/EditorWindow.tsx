"use client";

import React, {useEffect} from "react";
import {Panel, PanelGroup, PanelResizeHandle} from "react-resizable-panels";
import {TransformationSchema} from "@/components/transformation/TransformationSchema/TransformationSchema";
import {OutputJSONViewer} from "@/components/transformation/OutputViewer/OutputJSONViewer";
import {PageHTMLViewer} from "@/components/transformation/OutputViewer/PageHTMLViewer";
import {setPageHTML, setSchema} from "@/redux/features/transformationSchema/transformationSchemaSlice";
import {useAppDispatch} from "@/redux/hooks";
import {Tabs, TabsContent, TabsList, TabsTrigger} from "@/components/ui/tabs";
import {NodeSchemaType} from "@/lib/transformService/nodeTypes";

type EditorWindowProps = {
    transformationSchema: NodeSchemaType
    content: string
}


export const EditorWindow: React.FC<EditorWindowProps> = ({transformationSchema, content}) => {
    const dispatch = useAppDispatch();
    useEffect(() => {
        dispatch(setSchema(transformationSchema));
        dispatch(setPageHTML(content));
    }, [dispatch, transformationSchema, content]);

    return (
        <PanelGroup className="h-full rounded-sm" direction="horizontal">
            <Panel defaultSizePercentage={50} minSizePercentage={20} className="bg-gray-200">
                    <TransformationSchema />
            </Panel>
            <PanelResizeHandle className="w-1 bg-gray-300"/>
            <Panel defaultSizePercentage={50} minSizePercentage={30} className="bg-gray-200">
                <div className="h-full flex flex-col max-w-full justify-between overflow-hidden">
                    <Tabs defaultValue="outputJson" className="h-full flex flex-col">
                        <div className="pt-2 pl-2">
                            <TabsList>
                                <TabsTrigger value="outputJson">Output JSON</TabsTrigger>
                                <TabsTrigger value="htmlSource">Source HTML</TabsTrigger>
                            </TabsList>
                        </div>
                        <TabsContent value="outputJson">
                            <OutputJSONViewer/>
                        </TabsContent>
                        <TabsContent value="htmlSource" className="flex-1 pt-0 mt-0">
                            <PageHTMLViewer/>
                        </TabsContent>
                    </Tabs>
                </div>
            </Panel>
        </PanelGroup>
    )
}