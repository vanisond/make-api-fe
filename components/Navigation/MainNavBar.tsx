"use client";

import React from "react";
import Link from "next/link";
import {signOut, useSession} from "next-auth/react";
import {
    DropdownMenu,
    DropdownMenuContent,
    DropdownMenuItem,
    DropdownMenuSeparator,
    DropdownMenuTrigger
} from "@/components/ui/dropdown-menu";
import {MdLogout} from "react-icons/md";
import {User, UserRole} from "@prisma/client";
import logo from "@/public/logo.svg";
import Image from "next/image";

export const MainNavBar = () => {
    const session = useSession();
    return (
        <nav className="z-50 w-full bg-white border-b border-gray-200 dark:bg-gray-800 dark:border-gray-700">
            <div className="px-3 py-3 lg:px-5 lg:pl-3">
                <div className="flex items-center justify-between">
                    <div className="flex items-center justify-start">
                        <Link href="/" className="flex gap-2 items-center ml-2 md:mr-24">
                            <Image src={logo} alt="MakeAPI" className="w-6 h-6"/>
                            <span className="self-center text-xl font-semibold sm:text-2xl whitespace-nowrap dark:text-white">MakeAPI</span>
                        </Link>
                    </div>
                    <div className="flex items-center">

                        {session.status === "authenticated" && (
                            <DropdownMenu>
                                <DropdownMenuTrigger className="flex gap-2 items-center">
                                    <span className="inline-flex items-center justify-center h-8 w-8 rounded-full bg-gray-800">
                                      <span className="text-xs font-medium text-white leading-none">
                                          {session.data.user?.email?.substring(0, 2).toUpperCase()}
                                      </span>
                                    </span>
                                    <span>
                                        {session.data.user?.name?.substring(0, 62) || "User"}
                                    </span>
                                </DropdownMenuTrigger>
                                <DropdownMenuContent>
                                    <Link href={`/profile`}>
                                        <DropdownMenuItem>
                                            Profile
                                        </DropdownMenuItem>
                                    </Link>
                                    <Link href={`/projects`}>
                                        <DropdownMenuItem>
                                            My projects
                                        </DropdownMenuItem>
                                    </Link>
                                    {
                                        ((session.data.user as User)?.role === UserRole.ADMIN)  && (
                                            <Link href={`/admin`}>
                                                <DropdownMenuItem>
                                                    Admin
                                                </DropdownMenuItem>
                                            </Link>
                                        )
                                    }
                                    <DropdownMenuSeparator/>
                                    <DropdownMenuItem
                                        className="flex gap-2"
                                        onClick={(e) => {
                                            e.preventDefault();
                                            void signOut();
                                        }}
                                    >
                                        <MdLogout/>
                                        Sign out
                                    </DropdownMenuItem>
                                </DropdownMenuContent>
                            </DropdownMenu>
                        )}


                        <div className="flex items-center gap-2">
                            {session.status === "unauthenticated" && (
                                <Link
                                    href={`/login`}
                                >
                                    Sign in
                                </Link>
                            )}

                        </div>
                    </div>
                </div>
            </div>
        </nav>
    )
}