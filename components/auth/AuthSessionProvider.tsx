"use client";

import React from "react";
import {SessionProvider} from "next-auth/react"
import {config} from "@/lib/auth";
import {getServerSession} from "next-auth";

const AuthSessionProvider: React.FC<React.PropsWithChildren> = ({children}) => {
    return (
        <SessionProvider>
            {children}
        </SessionProvider>
    );
}

export default AuthSessionProvider;