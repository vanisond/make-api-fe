import Link from "next/link";
import {MdArrowBack} from "react-icons/md";
import {Separator} from "@/components/ui/separator";
import React from "react";
import {EditorWindow} from "@/components/transformation/EditorWindow";
import {NodeSchemaType} from "@/lib/transformService/nodeTypes";

export default async function Sandbox() {
    const rootTransformationSchema: NodeSchemaType= {
        type: 'object',
        selector: 'body',
        properties: [
            {
                property: 'main_title',
                type: 'string',
                selector: 'h1'
            },
            {
                property: 'ordered_list',
                type: 'object',
                selector: 'div:has(ul):first',
                properties: [
                    {
                        property: 'title',
                        type: 'string',
                        selector: 'h4'
                    },
                    {
                        property: 'items',
                        type: 'array',
                        selector: 'ul > li',
                        items: {
                            type: 'string',
                            selector: ''
                        }
                    }
                ]
            },
            {
                property: 'table',
                type: 'array',
                selector: 'tbody > tr',
                items: {
                    type: 'object',
                    selector: '',
                    properties: [
                        {
                            property: 'col1',
                            type: 'number',
                            selector: ':nth-of-type(1)'
                        },
                        {
                            property: 'col2',
                            type: 'number',
                            selector: ':nth-of-type(2)'
                        },
                        {
                            property: 'col3',
                            type: 'number',
                            selector: ':nth-of-type(3)'
                        }
                    ]
                }
            },
            {
                property: 'form',
                type: 'object',
                selector: 'form',
                properties: [
                    {
                        property: 'has_name_input',
                        type: 'boolean',
                        selector: '[name="name"]'
                    },
                    {
                        property: 'has_password_input',
                        type: 'boolean',
                        selector: '[name="password"]'
                    }
                ]
            }
        ]
    }
    const htmlContent = `<html>
<head>
    <title>Example page</title>
</head>
<body>

<h1>Main Heading (h1)</h1>
<h2>Sub Heading (h2)</h2>

<div class="panel">
    <h3>Panel Title (h3)</h3>
    <p>This is a panel with text content.</p>
</div>

<div>
    <h4>Div with List (h4)</h4>
    <ul>
        <li>Unordered List Item 1</li>
        <li>Unordered List Item 2</li>
        <li>Unordered List Item 3</li>
    </ul>
    <ol>
        <li>Ordered List Item 1</li>
        <li>Ordered List Item 2</li>
        <li>Ordered List Item 3</li>
    </ol>
</div>

<table border="1">
    <caption>
        Table Example
    </caption>
    <thead>
    <tr>
        <th>Header 1</th>
        <th>Header 2</th>
        <th>Header 3</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>1.1</td>
        <td>1.2</td>
        <td>1.3</td>
    </tr>
    <tr>
        <td>2.1</td>
        <td>2.2</td>
        <td>2.3</td>
    </tr>
    </tbody>
</table>

<form action="#">
    <label for="name">Name:</label>
    <input type="text" id="name" name="name" />
    <input type="submit" value="Submit" />
</form>

<footer>
    <p>This is a footer.</p>
</footer>
</body>
</html>`;

    return (
        <div className="flex flex-col h-full">
            <div className="flex items-center">
                <Link className="p-2" href="/"><MdArrowBack/></Link>
                <h1 className="flex-1 text-xl font-medium">Sandbox</h1>
                <div className="flex-none">
                    {/* todo */}
                </div>
            </div>

            <Separator className="mt-2 mb-4"/>

            <EditorWindow
                transformationSchema={rootTransformationSchema}
                content={htmlContent}
            />

        </div>
    );
}