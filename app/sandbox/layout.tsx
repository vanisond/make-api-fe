import React from "react";


export default function ProjectsLayout({children}: { children: React.ReactNode }) {
    return (
        <div className="px-5 py-6 h-full">
            {children}
        </div>
    )
}