import './globals.css'
import type {Metadata} from 'next'
import {Roboto_Mono} from 'next/font/google'
import React, {Suspense} from "react";
import {ReduxProvider} from "@/redux/ReduxProvider";
import {MainNavBar} from "@/components/Navigation/MainNavBar";
import AuthSessionProvider from "@/components/auth/AuthSessionProvider";
import {auth} from "@/lib/auth";
import {Toaster} from "react-hot-toast";
import {MdFavorite} from "react-icons/md";

const font = Roboto_Mono({ subsets: ['latin'] })

export const metadata: Metadata = {
  title: 'MakeAPI',
  description: 'Transform HTML data to JSON',
}

export default async function RootLayout({children}: Readonly<{ children: React.ReactNode }>) {

  const session = await auth();

  return (
    <html lang="en">
    <body className={font.className}>
    <AuthSessionProvider>
    <ReduxProvider>
      <div className="flex flex-col min-h-screen">
        <Toaster position="top-right" />
        <MainNavBar/>
        <div className="flex flex-auto bg-gray-50">
          <div className="flex-auto">
            <Suspense fallback={<div>Loading...</div>}>
              {children}
            </Suspense>
          </div>
        </div>
        <footer
            className="flex flex-none justify-center items-center h-20 bg-white border-t border-gray-200 dark:bg-gray-800 dark:border-gray-700">
          <div className="text-xs text-gray-500 flex items-center gap-2">
            <span>© 2023 MakeAPI</span>
            <span>·</span>
            <span>Made with <MdFavorite className="w-4 h-4 inline" /> in Prague</span>
          </div>
        </footer>
      </div>
    </ReduxProvider>
    </AuthSessionProvider>
    </body>
    </html>
  )
}
