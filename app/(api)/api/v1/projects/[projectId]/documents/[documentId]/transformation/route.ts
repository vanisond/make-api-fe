import {NextRequest} from "next/server";
import {getProjectDocumentById} from "@/lib/repository/documents";
import {verifyProjectAccessToken} from "@/lib/repository/projectAccessTokens";
import {getRootTransformationSchema} from "@/lib/repository/transformationSchemas";
import {transformHTMLtoJSON} from "@/lib/transformService/transform";
import {nodeSchema} from "@/lib/transformService/nodeTypes";
import {fetchHtml} from "@/lib/urlContentService/htmlFetcher";
import hashObject from "object-hash";
import {applyRateLimitPolicy} from "@/lib/rateLimitService/applyApiLimits";
import {createFailureReporter} from "@/lib/transformFailureService/failureManager";

export async function OPTIONS(request: NextRequest, context: { params: { projectId: string, documentId: string } }) {
    const accessToken = request.nextUrl.searchParams.get("token") ?? undefined;
    const validatedToken = await verifyProjectAccessToken(context.params.projectId, accessToken);

    if (!validatedToken) {
        return Response.json(
            {success: false, message: 'authentication failed'},
            {status: 401}
        )
    }

    const { rateLimiterResponse, rateLimitHeaders } = await applyRateLimitPolicy(validatedToken);
    if (rateLimiterResponse.isLimited) {
        return Response.json(
            {success: false, message: 'rate limit exceeded'},
            {
                status: 429,
                headers: {
                    ...(rateLimiterResponse?.secondsBeforeNext && {'Retry-After': `${Math.round(rateLimiterResponse.secondsBeforeNext / 1000)}`}),
                    ...rateLimitHeaders,
                }
            }
        )
    }

    // Get Origin from request and check if it is allowed
    const requestOrigin = request.headers.get('Origin');
    const responseAllowOrigin = validatedToken.allowedOrigins.find((allowedOrigin) => {
        if (allowedOrigin === '*') {
            return true;
        }
        return allowedOrigin === requestOrigin;
    });

    return new Response(null, {
        status: 204,
        headers: {
            ...(responseAllowOrigin && {
                "Access-Control-Allow-Origin": responseAllowOrigin,
                'Access-Control-Allow-Methods': 'GET, OPTIONS',
                'Access-Control-Allow-Headers': 'Content-Type',
                'Access-Control-Max-Age': '86400',
            }),
            ...rateLimitHeaders
        }
    })
}

export async function GET(request: NextRequest, context: { params: { projectId: string, documentId: string } }) {
    const accessToken = request.nextUrl.searchParams.get("token") ?? undefined;
    const validatedToken = await verifyProjectAccessToken(context.params.projectId, accessToken);

    if (!validatedToken) {
        return Response.json(
            {success: false, message: 'authentication failed'},
            {status: 401}
        )
    }

    const { rateLimiterResponse, rateLimitHeaders } = await applyRateLimitPolicy(validatedToken);
    if (rateLimiterResponse.isLimited) {
        return Response.json(
            {success: false, message: 'rate limit exceeded'},
            {
                status: 429,
                headers: {
                    ...(rateLimiterResponse?.secondsBeforeNext && {'Retry-After': `${Math.round(rateLimiterResponse.secondsBeforeNext / 1000)}`}),
                    ...rateLimitHeaders,
                }
            }
        )
    }

    const document = await getProjectDocumentById(context.params.documentId);
    const rootTransformationSchema = await getRootTransformationSchema(context.params.documentId);

    if (!document || !rootTransformationSchema) {
        return Response.json(
            {success: false, message: 'Document not found'},
            {status: 404, headers: rateLimitHeaders}
        )
    }

    // Failure handling
    const {
        reportTransformFailure,
        resetTransformFailure
    } = createFailureReporter(
        document.id,
        {
            projectId: document.projectId,
        }
    );

    // Deny access if document is disabled
    if (document.transformDisabled) {
        await reportTransformFailure('transform-disabled');
        const documentPageUrl = `${process.env.NEXT_PUBLIC_BASE_URL}/projects/${document.projectId}/documents/${document.id}`;
        return Response.json(
            {success: false, message: `Transformation has been disabled for this document due to previous failures. If you are the owner of this transformation, please check the document in you projects page. Visit: ${documentPageUrl}`},
            {status: 503, headers: rateLimitHeaders}
        )
    }

    const parsedSchema = nodeSchema.safeParse(rootTransformationSchema.schema);
    if (!parsedSchema.success) {
        await reportTransformFailure('schema-invalid');
        return Response.json(
            {success: false, message: 'Invalid schema: ' + JSON.stringify(parsedSchema.error.flatten())},
            {status: 500, headers: rateLimitHeaders}
        )
    }

    const htmlContent = await fetchHtml(document.url, document.downloadStrategy);
    if (!htmlContent.success) {
        await reportTransformFailure('html-fetch-failed');
        return Response.json(
            {success: false, message: `Failed to fetch HTML for the document: ${document.url}`},
            {status: 500, headers: rateLimitHeaders}
        )
    }

    const transformationResult = transformHTMLtoJSON(htmlContent.data, parsedSchema.data);
    if (!transformationResult.success) {
        await reportTransformFailure('transform-failed');
        return Response.json(
            {success: false, message: `Failed to transform HTML to JSON for the document: ${document.url}`},
            {status: 500, headers: rateLimitHeaders}
        )
    }

    const transformErrors = transformationResult.context?.errors ?? [];
    if (document.strictTransform && transformErrors.length > 0) {
        await reportTransformFailure('transform-strict-failed');
        return Response.json(
            {
                success: false,
                message: `Failed to transform HTML to JSON when strict mode is enabled for the document: ${document.url}`,
                errors: transformErrors
            },
            {status: 500, headers: rateLimitHeaders}
        )
    }

    const transformedResult = transformationResult.data;
    const transformedResultHash = hashObject(transformedResult, {
        algorithm: 'sha1',
        respectFunctionProperties: false,
    });

    // Request is successful, reset failure record
    await resetTransformFailure();

    // Get Origin from request and check if it is allowed
    const requestOrigin = request.headers.get('Origin');
    const responseAllowOrigin = validatedToken.allowedOrigins.find((allowedOrigin) => {
        if (allowedOrigin === '*') {
            return true;
        }
        return allowedOrigin === requestOrigin;
    });

    if (request.headers.get('If-None-Match') === `"${transformedResultHash}"`) {
        return new Response(null, {
            status: 304,
            headers: {
                'Content-Type': 'application/json',
                'Cache-Control': `max-age=${document.cacheMaxAge}, private`,
                'X-Exit-Point': "304", // used for debugging
                'ETag': `"${transformedResultHash}"`,
                ...(responseAllowOrigin && {
                    'Access-Control-Allow-Origin': responseAllowOrigin,
                }),
                ...rateLimitHeaders
            }
        })
    }

    return Response.json({
        documentName: document.name,
        sourceUrl: document.url,
        data: transformedResult,
        ...(transformErrors.length > 0 ? transformErrors : undefined),
    }, {
        status: 200,
        headers: {
            'Content-Type': 'application/json',
            'Cache-Control': `max-age=${document.cacheMaxAge}, private`,
            'ETag': `"${transformedResultHash}"`,
            'X-Exit-Point': "200", // used for debugging
            ...(responseAllowOrigin && {
                'Access-Control-Allow-Origin': responseAllowOrigin,
            }),
            ...rateLimitHeaders
        }
    });
}
