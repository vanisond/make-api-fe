import {NextRequest} from "next/server";
import {getProjectDocuments} from "@/lib/repository/documents";
import {verifyProjectAccessToken} from "@/lib/repository/projectAccessTokens";
import {getProjectById} from "@/lib/repository/projects";
import {applyRateLimitPolicy} from "@/lib/rateLimitService/applyApiLimits";

export async function OPTIONS(request: NextRequest, context: { params: { projectId: string } }) {
    const accessToken = request.nextUrl.searchParams.get("token") ?? undefined;
    const validatedToken = await verifyProjectAccessToken(context.params.projectId, accessToken);

    if (!validatedToken) {
        return Response.json(
            {success: false, message: 'authentication failed'},
            {status: 401}
        )
    }

    const {rateLimiterResponse, rateLimitHeaders} = await applyRateLimitPolicy(validatedToken);
    if (rateLimiterResponse.isLimited) {
        return Response.json(
            {success: false, message: 'rate limit exceeded'},
            {
                status: 429,
                headers: {
                    ...(rateLimiterResponse?.secondsBeforeNext && {'Retry-After': `${Math.round(rateLimiterResponse.secondsBeforeNext / 1000)}`}),
                    ...rateLimitHeaders,
                }
            }
        )
    }

    // Get Origin from request and check if it is allowed
    const requestOrigin = request.headers.get('Origin');
    const responseAllowOrigin = validatedToken.allowedOrigins.find((allowedOrigin) => {
        if (allowedOrigin === '*') {
            return true;
        }
        return allowedOrigin === requestOrigin;
    });

    return new Response(null, {
        status: 204,
        headers: {
            ...(responseAllowOrigin && {
                "Access-Control-Allow-Origin": responseAllowOrigin,
                'Access-Control-Allow-Methods': 'GET, OPTIONS',
                'Access-Control-Allow-Headers': 'Content-Type',
                'Access-Control-Max-Age': '86400',
            }),
            ...rateLimitHeaders
        }
    })
}

export async function GET(request: NextRequest, context: { params: { projectId: string } }) {
    const accessToken = request.nextUrl.searchParams.get("token") ?? undefined;
    const validatedToken = await verifyProjectAccessToken(context.params.projectId, accessToken);

    if (!validatedToken) {
        return Response.json(
            {success: false, message: 'authentication failed'},
            {status: 401}
        )
    }

    const {rateLimiterResponse, rateLimitHeaders} = await applyRateLimitPolicy(validatedToken);
    if (rateLimiterResponse.isLimited) {
        return Response.json(
            {success: false, message: 'rate limit exceeded'},
            {
                status: 429,
                headers: {
                    ...(rateLimiterResponse?.secondsBeforeNext && {'Retry-After': `${Math.round(rateLimiterResponse.secondsBeforeNext / 1000)}`}),
                    ...rateLimitHeaders,
                }
            }
        )
    }

    const project = await getProjectById(context.params.projectId);
    if (!project) {
        return Response.json(
            {success: false, message: 'Project not found'},
            {status: 404, headers: rateLimitHeaders}
        )
    }

    const projectDocuments = await getProjectDocuments(project.id);

    // Get Origin from request and check if it is allowed
    const requestOrigin = request.headers.get('Origin');
    const responseAllowOrigin = validatedToken.allowedOrigins.find((allowedOrigin) => {
        if (allowedOrigin === '*') {
            return true;
        }
        return allowedOrigin === requestOrigin;
    });

    return Response.json({
        data: {
            project: {
                name: project.name,
            },
            documents: projectDocuments.map((document) => ({
                name: document.name,
                transformUrlPath: `/api/v1/projects/${project.id}/documents/${document.id}/transformation`,
            })),
        },
    }, {
        status: 200,
        headers: {
            'Content-Type': 'application/json',
            'X-Exit-Point': "200", // used for debugging
            ...(responseAllowOrigin && {
                'Access-Control-Allow-Origin': responseAllowOrigin,
            }),
            ...rateLimitHeaders
        }
    });
}
