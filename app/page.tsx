import Link from "next/link";
import Image from "next/image";
import homepageImage from "@/public/5d1b55f5-de93-4e92-a36a-9d83f60d9cc8.png";

export default function Home() {
    return (
        <div className="flex w-full h-full items-center justify-center bg-gradient-to-br from-sky-50 to-fuchsia-100">
            <div className=" h-full w-full flex items-center justify-center">
                <div className="flex flex-col text-center space-y-8">

                        <Image
                            className="h-96 w-fit mx-auto opacity-70"
                            src={homepageImage}
                            alt=""
                        />

                    <h1 className="text-5xl font-bold bg-clip-text text-transparent bg-gradient-to-r from-blue-500 to-red-400">Transform HTML to JSON</h1>
                    <p className="text-xl">Easily convert static HTML pages into accessible APIs with our tool.</p>
                    <div className="mx-auto inline-flex gap-2">
                        <Link href="/login" className="inline-block bg-white text-gray-800 px-6 py-3 rounded-full font-semibold hover:bg-gray-100 transition duration-300">
                            Get Started
                        </Link>
                        <Link href="/sandbox" className="inline-block text-gray-800 px-6 py-3 rounded-full font-semibold hover:bg-gray-100 transition duration-300">
                            Try out
                        </Link>
                    </div>
                </div>
            </div>
        </div>
    );
}