import Link from 'next/link'

export default function NotFound() {
    return (
        <div className="flex flex-col items-center justify-center h-full">
            <h1 className="text-3xl font-medium">Page not found</h1>
            <Link href="/" className="mt-4 hover:underline">Go back to home</Link>

        </div>
    )
}
