import {Table, TableBody, TableCell, TableHead, TableHeader, TableRow} from "@/components/ui/table";
import React from "react";


const envVariables = [
    {
        name: "NEXT_PUBLIC_BASE_URL",
        description: "Base URL of the web application"
    },
    {
        name: "NEXTAUTH_URL",
        description: "Only for production https://next-auth.js.org/configuration/options#nextauth_url"
    },
    {
        name: "PAGE_FETCHER_SERVICE_BASE_URL",
        description: "Page Fetcher Service URL"
    },
    {
        name: "API_RATE_LIMIT_INACTIVITY_THRESHOLD_MS",
        description: "Time after which the old entries are deleted from the rate limiter request store"
    },
    {
        name: "API_RATE_LIMIT_CLEANUP_INTERVAL_MS",
        description: "Minimum time between two cleanup runs (checks for old entries in the rate limiter request store)"
    },
    {
        name: "API_RATE_LIMIT_DEFAULT_ACCESS_TOKEN_MAX_REQUESTS",
        description: "Rate limit for the access token entity (max. requests per time-window)"
    },
    {
        name: "API_RATE_LIMIT_DEFAULT_ACCESS_TOKEN_WINDOW_MS",
        description: "Rate limit for the access token entity (time-window in milliseconds)"
    },
    {
        name: "API_RATE_LIMIT_DEFAULT_USER_MAX_REQUESTS",
        description: "Rate limit for the user entity (max. requests per time-window)"
    },
    {
        name: "API_RATE_LIMIT_DEFAULT_USER_WINDOW_MS",
        description: "Rate limit for the user entity (time-window in milliseconds)"
    },
];

export const EnvVariablesTable = () => {
    return (
        <Table>
            <TableHeader>
                <TableRow>
                    <TableHead>Variable</TableHead>
                    <TableHead>Value</TableHead>
                    <TableHead>Description</TableHead>
                </TableRow>
            </TableHeader>
            <TableBody>
                {envVariables.map((envVariable) => (
                    <TableRow key={envVariable.name}>
                        <TableCell>
                            {envVariable.name}
                        </TableCell>
                        <TableCell>
                            {process.env[envVariable.name]}
                        </TableCell>
                        <TableCell>
                            {envVariable.description}
                        </TableCell>
                    </TableRow>
                ))}
            </TableBody>
        </Table>
    )
}