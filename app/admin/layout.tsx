import React from 'react';
import Link from "next/link";
import {MdArrowBack, MdDashboard, MdPerson} from "react-icons/md";
import {Separator} from "@/components/ui/separator";


export default function AdminLayout({children}: {children: React.ReactNode}) {
    return (
        <div className="px-5 py-6 h-full">
            <div className="flex items-center gap-1">
                <Link className="p-2" href={`/`}><MdArrowBack/></Link>
                <h1 className="flex-1 text-xl font-medium">Administration</h1>
                <div className="flex-none">
                    {/* todo: action buttons */}
                </div>
            </div>

            <Separator className="mt-2 mb-4"/>
            <div className="flex">

            <aside className="w-64 h-full transition-transform -translate-x-full sm:translate-x-0" aria-label="Sidebar">
                <div className="h-full px-3 py-4 overflow-y-auto bg-gray-50 dark:bg-gray-800">
                    <ul className="space-y-2">
                        <li>
                            <Link href="/admin" className="flex items-center p-2 text-gray-900 rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-gray-700 group">
                                <MdDashboard />
                                <span className="ms-3">Variables</span>
                            </Link>
                        </li>
                        <li>
                            <Link href="/admin/users" className="flex items-center p-2 text-gray-900 rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-gray-700 group">
                                <MdPerson />
                                <span className="ms-3">Users</span>
                            </Link>
                        </li>
                    </ul>
                </div>
            </aside>

            <div className="p-4 flex-1 overflow-x-auto">
                {children}
            </div>

            </div>

        </div>

    )
}