import {Separator} from "@/components/ui/separator";
import React from "react";
import {EnvVariablesTable} from "@/app/admin/env-variables-table";

export default async function Admin() {
    return (
        <div>
            <h2 className="text-lg font-medium">Environment variables</h2>
            <Separator className="mt-2 mb-4"/>
            <EnvVariablesTable/>
        </div>
    );
}