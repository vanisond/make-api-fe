import {Separator} from "@/components/ui/separator";
import React from "react";
import {getAllUsers} from "@/lib/repository/users";
import {UsersTable} from "@/app/admin/users/users-table";

export default async function Users() {
    const users = await getAllUsers();

    return (
        <div>
            <h2 className="text-lg font-medium">Users</h2>
            <Separator className="mt-2 mb-4"/>
            <UsersTable users={users}/>
        </div>
    );
}