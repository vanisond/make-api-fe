import {getUserProjects} from "@/lib/repository/projects";
import {getDisabledProjectDocumentsCount, getProjectDocumentsCount} from "@/lib/repository/documents";
import {Separator} from "@/components/ui/separator";
import React from "react";
import {ProjectsTable} from "@/app/projects/projects-table";


export default async function ProjectsSlot({params}: Readonly<{ params: { userId: string } }>) {

    const projects = await getUserProjects(params.userId);
    const projectIds = projects.map(project => project.id);
    const documentCounts = await getProjectDocumentsCount(projectIds);
    const disabledDocumentsCounts = await getDisabledProjectDocumentsCount(projectIds);
    const projectsWithCounts = projects.map(project => ({
        ...project,
        documentsCount: documentCounts[project.id] ?? 0,
        disabledDocumentsCount: disabledDocumentsCounts[project.id] ?? 0,
    }))

    return (
        <div>
            <div className="flex items-center">
                <h2 className="flex-1 text-lg font-medium">User projects</h2>
            </div>
            <Separator className="mt-2 mb-4"/>
            <ProjectsTable projectsWithCounts={projectsWithCounts}/>
        </div>
    )
}