import {getUserById} from "@/lib/repository/users";
import {Separator} from "@/components/ui/separator";
import {RemoveUserButton} from "@/app/(forms)/user/remove-user-button";
import React from "react";
import {UserDialog} from "@/app/(forms)/user/user-form";
import {ChangePasswordDialog} from "@/app/(forms)/user/change-password-form";
import {RateLimitDialog} from "@/app/(forms)/rate-limit/rate-limit-form";
import {RateLimitType} from "@prisma/client";
import {notFound} from "next/navigation";


export default async function User({params}: Readonly<{ params: { userId: string } }>) {
    const user = await getUserById(params.userId);
    if (!user) {
        notFound();
    }

    return (
        <div>

            <div className="flex flex-col space-y-2">

                <div className="flex items-center gap-1">
                    <h2 className="text-lg font-medium">
                        Profile info
                    </h2>
                </div>
                <Separator className="mt-2 mb-4"/>
                <div>
                    <span className="font-medium">Name:</span> {user?.name || "n/a"}
                </div>
                <div>
                    <span className="font-medium">E-mail:</span> {user?.email}
                </div>
                <div>
                    <span className="font-medium">Role:</span> {user?.role}
                </div>
                <div className="flex gap-2">
                    <UserDialog user={user!}/>
                    <ChangePasswordDialog user={user!}/>
                </div>

                <h2 className="text-lg font-medium pt-6">Custom rate-limits</h2>
                <Separator className="mt-2 mb-4"/>
                <div>
                    <RateLimitDialog
                        rateLimitId={user?.apiRequestsPerUserRateLimitId ?? undefined}
                        rateLimitType={RateLimitType.USER}
                        userId={user!.id}
                    />
                </div>

                <h2 className="text-lg font-medium pt-6">Delete user</h2>
                <Separator className="mt-2 mb-4"/>
                <div>
                    <RemoveUserButton user={user!}/>
                </div>
            </div>
        </div>
    );
}