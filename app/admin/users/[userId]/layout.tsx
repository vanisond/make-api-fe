import React from "react";
import {Tabs, TabsContent, TabsList, TabsTrigger} from "@/components/ui/tabs";
import {OutputJSONViewer} from "@/components/transformation/OutputViewer/OutputJSONViewer";
import {PageHTMLViewer} from "@/components/transformation/OutputViewer/PageHTMLViewer";
import {Separator} from "@/components/ui/separator";
import Link from "next/link";
import {MdArrowBack} from "react-icons/md";

export default function UserLayout(props: {
    children: React.ReactNode
    projects: React.ReactNode
}) {
    return (
        <>
            <div className="flex items-center gap-1">
                <Link className="p-2" href={`/admin/users`}><MdArrowBack/></Link>
                <h2 className="text-xl font-medium">
                    User details
                </h2>
            </div>
            <Separator className="mt-2 mb-4"/>
            {props.children}
            <div className="my-6"/>
            {props.projects}
        </>
    )
}