"use client";

import React from "react";
import {Table, TableBody, TableCell, TableHead, TableHeader, TableRow} from "@/components/ui/table";
import Link from "next/link";
import {User} from "@prisma/client";

type UsersTableProps = {
    users: User[];

}

export const UsersTable: React.FC<UsersTableProps> = ({users}) => {
    return (
        <Table>
            <TableHeader>
                <TableRow>
                    <TableHead>ID</TableHead>
                    <TableHead>User name</TableHead>
                    <TableHead>E-mail</TableHead>
                    <TableHead>Role</TableHead>
                    {/*<TableHead className="text-right">Actions</TableHead>*/}
                </TableRow>
            </TableHeader>
            <TableBody>
                {users.map((user) => (
                    <TableRow key={user.id}>
                        <TableCell>
                            <Link href={`/admin/users/${user.id}`}>
                                {user.id}
                            </Link>
                        </TableCell>
                        <TableCell>
                            {user.name || "n/a"}
                        </TableCell>
                        <TableCell>
                            {user.email}
                        </TableCell>
                        <TableCell>
                            {user.role}
                        </TableCell>
                    </TableRow>
                ))}
            </TableBody>
        </Table>
    )
}