import {Separator} from "@/components/ui/separator";
import React from "react";
import {auth} from "@/lib/auth";
import {getUserById} from "@/lib/repository/users";
import {RemoveUserButton} from "@/app/(forms)/user/remove-user-button";

export default async function DeleteAccount() {

    const session = await auth();
    if (!session?.user?.id) {
        return null;
    }

    const user = await getUserById(session?.user?.id);

    return (
        <div>
            <h2 className="text-lg font-medium">Delete account</h2>
            <Separator className="mt-2 mb-4"/>
            <p className="mb-2">
                If you want to delete your account, please click the button below. This action is irreversible.
            </p>
            <div className="flex gap-2">
                <RemoveUserButton user={user!}/>
            </div>
        </div>
    );
}