import {Separator} from "@/components/ui/separator";
import React from "react";
import {auth} from "@/lib/auth";
import {getUserById} from "@/lib/repository/users";
import {UserDialog} from "@/app/(forms)/user/user-form";
import {ChangePasswordDialog} from "@/app/(forms)/user/change-password-form";

export default async function Profile() {

    const session = await auth();
    if (!session?.user?.id) {
        return null;
    }

    const user = await getUserById(session?.user?.id);

    return (
        <div>
            <h2 className="text-lg font-medium">Profile info</h2>
            <Separator className="mt-2 mb-4"/>

            <div className="flex flex-col space-y-2">
                <div>
                    <span className="font-medium">Name:</span> {user?.name}
                </div>
                <div>
                    <span className="font-medium">E-mail:</span> {user?.email}
                </div>
                <div className="flex gap-2">
                    <UserDialog user={user!}/>
                    <ChangePasswordDialog user={user!}/>
                </div>
            </div>

        </div>
    );
}