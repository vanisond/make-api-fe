"use client";

import React, {useTransition} from "react";
import {Table, TableBody, TableCell, TableHead, TableHeader, TableRow} from "@/components/ui/table";
import Link from "next/link";
import {Project} from "@prisma/client";
import {
    AlertDialog,
    AlertDialogAction,
    AlertDialogCancel,
    AlertDialogContent,
    AlertDialogDescription,
    AlertDialogFooter,
    AlertDialogHeader,
    AlertDialogTitle,
    AlertDialogTrigger
} from "@/components/ui/alert-dialog";
import {Button} from "@/components/ui/button";
import {MdDelete} from "react-icons/md";
import toast from "react-hot-toast";
import {deleteProjectServerAction} from "@/app/(forms)/project/actions";


const RemoveProjectButton: React.FC<{ project: Project }> = ({project}) => {
    const [isPending, startTransition] = useTransition()
    return (
        <AlertDialog>
            <AlertDialogTrigger asChild>
                <Button
                    className="flex gap-1"
                    variant="destructive"
                    size="sm"
                    disabled={isPending}
                >
                    <MdDelete/>
                    Delete
                </Button>
            </AlertDialogTrigger>
            <AlertDialogContent>
                <AlertDialogHeader>
                    <AlertDialogTitle>Are you absolutely sure?</AlertDialogTitle>
                    <AlertDialogDescription>
                        This action cannot be undone. This will permanently delete the
                        project with all its documents and transformation schemas.
                    </AlertDialogDescription>
                </AlertDialogHeader>
                <AlertDialogFooter>
                    <AlertDialogCancel>Cancel</AlertDialogCancel>
                    <AlertDialogAction
                        onClick={() => startTransition(async () => {
                            const result = await deleteProjectServerAction(project.id);
                            if (result.success) {
                                toast.success("Project deleted");
                            } else {
                                toast.error(result.error!);
                            }
                        })}
                        disabled={isPending}
                    >
                        Delete
                    </AlertDialogAction>
                </AlertDialogFooter>
            </AlertDialogContent>
        </AlertDialog>
    )
}


type ProjectsTableProps = {
    projectsWithCounts: (Project & { documentsCount: number, disabledDocumentsCount: number })[];
}

export const ProjectsTable: React.FC<ProjectsTableProps> = ({projectsWithCounts}) => {
    return (
        <Table>
            <TableHeader>
                <TableRow>
                    <TableHead>Project name</TableHead>
                    <TableHead>Documents</TableHead>
                    <TableHead className="text-right">Actions</TableHead>
                </TableRow>
            </TableHeader>
            <TableBody>
                {projectsWithCounts.map(project => (
                    <TableRow key={project.id}>
                        <TableCell>
                            <Link href={`/projects/${project.id}`}>
                                <b>{project.name}</b>
                            </Link>
                        </TableCell>
                        <TableCell>
                            {project.documentsCount}
                            {project.disabledDocumentsCount > 0 && (
                                <span className="text-destructive" title="Disabled documents">
                                    {" "}({project.disabledDocumentsCount})
                                </span>
                            )}
                        </TableCell>
                        <TableCell className="text-right">
                            <div className="inline-flex gap-2">
                                <RemoveProjectButton project={project}/>
                            </div>
                        </TableCell>
                    </TableRow>
                ))}
            </TableBody>
        </Table>
    )
}