import {Separator} from "@/components/ui/separator";
import Link from "next/link";
import React from "react";
import {getProjectDocuments} from "@/lib/repository/documents";
import {getProjectById} from "@/lib/repository/projects";
import {CreateDocumentDialog} from "@/app/(forms)/document/document-form";
import {MdArrowBack} from "react-icons/md";
import {ProjectSettingsDialog} from "@/app/projects/[projectId]/project-settings";
import {getProjectAccessTokens} from "@/lib/repository/projectAccessTokens";
import {DocumentsTable} from "@/app/projects/[projectId]/documents-table";
import Image from "next/image";
import noDocumentsImage from "@/public/no-documents.png";
import {auth} from "@/lib/auth";
import {UserRole} from "@prisma/client";
import {notFound} from "next/navigation";

export default async function Project({params}: Readonly<{ params: { projectId: string } }>) {
    const project = await getProjectById(params.projectId);
    if (!project) {
        notFound();
    }

    const session = await auth();
    if (project.ownerId !== session?.user?.id && session?.user?.role !== UserRole.ADMIN) {
        // Can't change the status code here (for e.g. 403), headers were already sent at this point
        // @see https://github.com/vercel/next.js/discussions/53225
        notFound();
    }

    const projectDocuments = await getProjectDocuments(params.projectId);
    const projectAccessTokens = await getProjectAccessTokens(params.projectId);

    return (
        <div>
            <div className="flex items-center gap-1">
                <Link className="p-2" href={`/projects`}><MdArrowBack/></Link>
                <h1 className="flex-1 text-xl font-medium">{project?.name}</h1>
                <div className="flex-none">
                    <CreateDocumentDialog projectId={params.projectId}/>
                </div>
                <div className="flex-none">
                    <ProjectSettingsDialog project={project} projectAccessTokens={projectAccessTokens}/>
                </div>
            </div>

            <Separator className="mt-2 mb-4"/>

            {projectDocuments.length === 0 ? (
                <div className="flex flex-col gap-2 text-gray-600 text-center">
                    <Image
                        className="mx-auto max-h-[50vh] w-auto opacity-90"
                        src={noDocumentsImage}
                        alt="No documents"
                    />
                    <span>There are no documents in this project yet.</span>
                    <div>
                        <CreateDocumentDialog projectId={params.projectId}/>
                    </div>
                </div>
            ) : (
                <DocumentsTable projectId={params.projectId} projectDocuments={projectDocuments}/>
            )}
        </div>
    );
}