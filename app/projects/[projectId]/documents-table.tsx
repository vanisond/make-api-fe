"use client";

import React, {useTransition} from "react";
import {Table, TableBody, TableCell, TableHead, TableHeader, TableRow} from "@/components/ui/table";
import Link from "next/link";
import {ProjectDocument} from "@prisma/client";
import {
    AlertDialog,
    AlertDialogAction,
    AlertDialogCancel,
    AlertDialogContent,
    AlertDialogDescription,
    AlertDialogFooter,
    AlertDialogHeader,
    AlertDialogTitle,
    AlertDialogTrigger
} from "@/components/ui/alert-dialog";
import {Button} from "@/components/ui/button";
import {MdApi, MdCopyAll, MdDelete, MdOpenInNew, MdWarning} from "react-icons/md";
import toast from "react-hot-toast";
import {
    Dialog,
    DialogContent,
    DialogDescription,
    DialogHeader,
    DialogTitle,
    DialogTrigger
} from "@/components/ui/dialog";
import {Input} from "@/components/ui/input";
import {Label} from "@/components/ui/label";
import {Textarea} from "@/components/ui/textarea";
import {HoverCard, HoverCardContent, HoverCardTrigger} from "@/components/ui/hover-card";
import {cx} from "class-variance-authority";
import {deleteDocumentServerAction} from "@/app/(forms)/document/actions";


const ApiButton: React.FC<{ projectId: string, projectDocument: ProjectDocument }> = ({projectId, projectDocument}) => {
    const [open, setOpen] = React.useState(false);
    const [token, setToken] = React.useState("ENTER_TOKEN");
    const transformationEndpoint = process.env.NEXT_PUBLIC_BASE_URL + `/api/v1/projects/${projectId}/documents/${projectDocument.id}/transformation?token=${token}`;
    return (
        <Dialog open={open} onOpenChange={setOpen}>
            <DialogTrigger asChild>
                <Button
                    className="flex gap-1"
                    variant="outline"
                    size="sm"
                    disabled={false}
                >
                    <MdApi/>
                    API
                </Button>
            </DialogTrigger>
            <DialogContent>
                <DialogHeader>
                    <DialogTitle>API endpoint</DialogTitle>
                    <DialogDescription>
                        This is the API endpoint for this document.
                    </DialogDescription>
                </DialogHeader>

                <div className="grid w-full max-w-xs items-center gap-1.5">
                    <Label htmlFor="accessToken">Access token</Label>
                    <Input
                        id="accessToken"
                        value={token}
                        onClick={(event) => event.currentTarget.select()}
                        onChange={(event) => setToken(event.target.value)}
                    />
                </div>

                <div className="grid w-full items-center gap-1.5">
                    <Label htmlFor="transformationEndpoint">Transformation endpoint</Label>
                    <Textarea
                        id="transformationEndpoint"
                        className="max-h-28"
                        value={transformationEndpoint}
                        onClick={(event) => event.currentTarget.select()}
                        readOnly
                    />
                </div>

                <div className="flex gap-2">
                    <Button
                        className="flex gap-2"
                        type="button"
                        variant="outline"
                        onClick={() => navigator.clipboard.writeText(transformationEndpoint)}
                    >
                        <MdCopyAll/>
                        Copy link
                    </Button>

                    <Button
                        asChild
                        className="flex gap-2"
                        type="button"
                        variant="outline"
                    >
                        <Link href={transformationEndpoint} target="_blank">
                            <MdOpenInNew/>
                            Open
                        </Link>
                    </Button>
                </div>
            </DialogContent>
        </Dialog>
    )
}

const RemoveDocumentButton: React.FC<{
    projectId: string,
    projectDocument: ProjectDocument
}> = ({
          projectId,
          projectDocument
      }) => {
    const [isPending, startTransition] = useTransition()
    return (
        <AlertDialog>
            <AlertDialogTrigger asChild>
                <Button
                    className="flex gap-1"
                    variant="outline"
                    size="sm"
                    disabled={isPending}
                >
                    <MdDelete/>
                    <span className="sr-only">
                        Delete
                    </span>
                </Button>
            </AlertDialogTrigger>
            <AlertDialogContent>
                <AlertDialogHeader>
                    <AlertDialogTitle>Are you absolutely sure?</AlertDialogTitle>
                    <AlertDialogDescription>
                        This action cannot be undone. This will permanently delete the
                        document and its corresponding transformation schemas.
                    </AlertDialogDescription>
                </AlertDialogHeader>
                <AlertDialogFooter>
                    <AlertDialogCancel>Cancel</AlertDialogCancel>
                    <AlertDialogAction
                        onClick={() => startTransition(async () => {
                            const result = await deleteDocumentServerAction(projectDocument.id);
                            if (result.success) {
                                toast.success("Project document deleted");
                            } else {
                                toast.error(result.error!);
                            }
                        })}
                        disabled={isPending}
                    >
                        Delete
                    </AlertDialogAction>
                </AlertDialogFooter>
            </AlertDialogContent>
        </AlertDialog>
    )
}

const TransformDisabledWarningIcon = () => {
    return (

        <HoverCard>
            <HoverCardTrigger>
                <span className="text-destructive">
                    <MdWarning className="h-4 w-4 text-destructive"/>
                </span>
            </HoverCardTrigger>
            <HoverCardContent>
                Transformation has been disabled for this document due to repeated failures in transformation.
            </HoverCardContent>
        </HoverCard>
    )
}


type DocumentTableProps = {
    projectId: string;
    projectDocuments: ProjectDocument[];

}

export const DocumentsTable: React.FC<DocumentTableProps> = ({projectId, projectDocuments}) => {
    return (
        <Table>
            <TableHeader>
                <TableRow>
                    <TableHead>Document</TableHead>
                    <TableHead>url</TableHead>
                    <TableHead className="text-right">Actions</TableHead>
                </TableRow>
            </TableHeader>
            <TableBody>
                {projectDocuments.map(projectDocument => (
                    <TableRow key={projectDocument.id}>
                        <TableCell>
                            <Link
                                href={`/projects/${projectId}/documents/${projectDocument.id}`}
                                className="flex gap-2 items-center"
                            >
                                {projectDocument.transformDisabled && (<TransformDisabledWarningIcon/>)}
                                <b className={cx({"text-destructive": projectDocument.transformDisabled})}>{projectDocument.name}</b>
                            </Link>
                        </TableCell>
                        <TableCell>
                            {projectDocument.url}
                        </TableCell>
                        <TableCell className="text-right">
                            <div className="inline-flex gap-2">
                                <ApiButton projectId={projectId} projectDocument={projectDocument}/>
                                <RemoveDocumentButton projectId={projectId} projectDocument={projectDocument}/>
                            </div>
                        </TableCell>
                    </TableRow>
                ))}
            </TableBody>
        </Table>
    )
}