import {getProjectDocumentById} from "@/lib/repository/documents";
import {Separator} from "@/components/ui/separator";
import React from "react";
import {getProjectById} from "@/lib/repository/projects";
import {getRootTransformationSchema} from "@/lib/repository/transformationSchemas";
import {MdArrowBack} from "react-icons/md";
import {fetchHtml} from "@/lib/urlContentService/htmlFetcher";
import {TransformationEditor} from "@/app/(forms)/transform/transformation-editor";
import Link from "next/link";

export default async function ProjectDocument({params}: Readonly<{
    params: { projectId: string; documentId: string }
}>) {
    const project = await getProjectById(params.projectId);
    const projectDocument = await getProjectDocumentById(params.documentId);

    if (!projectDocument || !project) {
        return <div>Project or document not found</div>;
    }

    const rootTransformationSchema = await getRootTransformationSchema(projectDocument.id);

    // TODO: load as stream, so it won't block the UI
    const htmlContent = await fetchHtml(projectDocument.url, projectDocument.downloadStrategy);

    return (
        <div className="flex flex-col h-full">
            <div className="flex items-center">
                <Link className="p-2" href={`/projects/${params.projectId}`}><MdArrowBack/></Link>
                <h1 className="flex-1 text-xl font-medium">{project?.name} / {projectDocument?.name}</h1>
                <div className="flex-none">
                    {/* todo */}
                </div>
            </div>

            <Separator className="mt-2 mb-4"/>

            <TransformationEditor
                transformationSchema={rootTransformationSchema}
                projectDocument={projectDocument}
                contentLoadSuccess={htmlContent.success}
                contentType={htmlContent.headers?.["content-type"]}
                content={htmlContent.data}
            />

        </div>
    );
}
