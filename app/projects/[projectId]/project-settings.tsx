"use client";

import React from "react";
import {
    Dialog,
    DialogContent,
    DialogDescription,
    DialogHeader,
    DialogTitle,
    DialogTrigger
} from "@/components/ui/dialog";
import {Button} from "@/components/ui/button";
import {AccessTokenDialog} from "@/app/(forms)/access-token/access-token-form";
import {Project, ProjectAccessToken, RateLimitType} from "@prisma/client";
import {format, formatDistanceToNow} from "date-fns";
import {Table, TableBody, TableCell, TableHead, TableHeader, TableRow} from "@/components/ui/table";
import {ProjectForm} from "@/app/(forms)/project/project-form";
import {Separator} from "@/components/ui/separator";
import {RateLimitDialog} from "@/app/(forms)/rate-limit/rate-limit-form";


const AccessTokenRow: React.FC<{ accessToken: ProjectAccessToken }> = ({accessToken}) => {
    return (
        <TableRow>
            <TableCell>
                {accessToken.description}
            </TableCell>
            <TableCell>
                {accessToken?.expiresAt ? format(accessToken?.expiresAt, "PPP") : "never"}
            </TableCell>
            <TableCell>
                {accessToken?.lastUsedAt ? formatDistanceToNow(accessToken?.lastUsedAt, {
                    includeSeconds: true,
                    addSuffix: true
                }) : "-"}
            </TableCell>
            <TableCell className="flex gap-2 items-center">
                <AccessTokenDialog projectId={accessToken.projectId} accessToken={accessToken}/>
                <RateLimitDialog
                    rateLimitId={accessToken.apiRequestsPerTokenRateLimitId ?? undefined}
                    rateLimitType={RateLimitType.ACCESS_TOKEN}
                    accessTokenId={accessToken.id}
                />
            </TableCell>
        </TableRow>
    )

}

const AccessTokensList: React.FC<{ projectAccessTokens: ProjectAccessToken[] }> = ({projectAccessTokens}) => {

    if (!projectAccessTokens.length) {
        return <div className="text-gray-600">No access tokens available.</div>
    }

    return (
        <Table>
            <TableHeader>
                <TableRow>
                    <TableHead>Description</TableHead>
                    <TableHead>Expiration</TableHead>
                    <TableHead>Last used</TableHead>
                    <TableHead>Actions</TableHead>
                </TableRow>
            </TableHeader>
            <TableBody>
                {projectAccessTokens?.map(accessToken => (
                    <AccessTokenRow key={accessToken.id} accessToken={accessToken}/>
                ))}
            </TableBody>
        </Table>
    )
}

export const ProjectSettingsDialog: React.FC<{
    project: Project,
    projectAccessTokens: ProjectAccessToken[]
}> = ({project, projectAccessTokens}) => {
    const [open, setOpen] = React.useState(false);

    return (
        <Dialog open={open} onOpenChange={setOpen}>
            <DialogTrigger asChild><Button variant="secondary">Project settings</Button></DialogTrigger>
            <DialogContent className="min-w-[min(860px,90vw)]">
                <DialogHeader>
                    <DialogTitle>Project settings</DialogTitle>
                    <DialogDescription>
                        {/* TODO */}
                    </DialogDescription>
                </DialogHeader>

                <ProjectForm project={project}/>

                <Separator className="my-4"/>

                <h2>Access tokens</h2>
                <AccessTokensList projectAccessTokens={projectAccessTokens}/>
                <div>
                    <AccessTokenDialog projectId={project.id}/>
                </div>
            </DialogContent>
        </Dialog>
    )
}