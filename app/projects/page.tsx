import React from 'react';
import {auth} from "@/lib/auth";
import {getUserProjects} from "@/lib/repository/projects";
import {CreateProjectDialog} from "@/app/(forms)/project/project-form";
import {Separator} from "@/components/ui/separator";
import {ProjectsTable} from "@/app/projects/projects-table";
import Image from "next/image";
import noProjectsImage from "@/public/no-projects.png";
import {getDisabledProjectDocumentsCount, getProjectDocumentsCount} from "@/lib/repository/documents";

export default async function Projects()  {
    const session = await auth();
    const projects = await getUserProjects(session?.user?.id);
    const projectIds = projects.map(project => project.id);
    const documentCounts = await getProjectDocumentsCount(projectIds);
    const disabledDocumentsCounts = await getDisabledProjectDocumentsCount(projectIds);
    const projectsWithCounts = projects.map(project => ({
        ...project,
        documentsCount: documentCounts[project.id] ?? 0,
        disabledDocumentsCount: disabledDocumentsCounts[project.id] ?? 0,
    }))

    return (
        <div>
            <div className="flex items-center">
                <h1 className="flex-1 text-xl font-medium">Projects</h1>
                <div className="flex-none">
                    <CreateProjectDialog/>
                </div>
            </div>

            <Separator className="mt-2 mb-4"/>

            {projects.length === 0 ? (
                <div className="flex flex-col gap-2 text-gray-600 text-center">
                    <Image
                        className="mx-auto max-h-[50vh] w-auto opacity-90"
                        src={noProjectsImage}
                        alt="No projects"
                    />
                    <span>There are no projects yet.</span>
                    <div>
                        <CreateProjectDialog/>
                    </div>
                </div>
            ) : (
                <ProjectsTable projectsWithCounts={projectsWithCounts} />
            )}
        </div>
    )
}
