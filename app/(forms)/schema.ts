import {z} from "zod";
import {DocumentDownloadStrategy, UserRole} from "@prisma/client";

export const signInSchema = z.object({
    email: z.string().email(),
    password: z.string().min(3),
});

export type SignInSchemaType = z.infer<typeof signInSchema>

export const signUpSchema = z.object({
    email: z.string().email(),
    password: z.string().min(3),
    checkPassword: z.string(),
}).superRefine((data, ctx) => {
    if (data.password !== data.checkPassword) {
        ctx.addIssue({
            code: z.ZodIssueCode.custom,
            message: "Passwords do not match",
            path: ["checkPassword"],
        });
    }
});

export type SignUpSchemaType = z.infer<typeof signUpSchema>

export const projectSchema = z.object({
    name: z.string().min(4)
})

export type ProjectSchemaType = z.infer<typeof projectSchema>

const MAX_CACHE_CONTROL_MAX_AGE = 31536000; // 1 year in seconds

export const projectDocumentSchema = z.object({
    projectId: z.string(),
    name: z.string().min(4),
    url: z.string().url(),
    cacheMaxAge: z.number().int().min(0).max(MAX_CACHE_CONTROL_MAX_AGE).optional(),
    downloadStrategy: z.enum([DocumentDownloadStrategy.RAW, DocumentDownloadStrategy.HEADLESS]),
    strictTransform: z.boolean().default(true),
})

export type ProjectDocumentSchemaType = z.infer<typeof projectDocumentSchema>

export const projectAccessTokensSchema = z.object({
    projectId: z.string(),
    description: z.string().min(4),
    token: z.string().optional(),
    // list of origins, one per line
    allowedOrigins: z.string()
        .transform((value) => {
            const lines = value.split("\n").filter(line => line.trim() !== '');
            return lines;
        })
        .superRefine((lines, ctx) => {
            for (const [index, line] of lines.entries()) {
                const trimmedLine = line.trim();
                // wildcard
                if (trimmedLine === '*') {
                    continue;
                }
                try {
                    const url = new URL(trimmedLine);
                    // simplified hostname validation, only checks for invalid characters and label length
                    const hostnameRegex = /^[a-zA-Z0-9-]{1,63}(\.[a-zA-Z0-9-]{1,63})*(\.[a-zA-Z]{2,})?$/;
                    if (!hostnameRegex.test(url.hostname)) {
                        ctx.addIssue({
                            code: z.ZodIssueCode.custom,
                            message: `Invalid origin at line ${index + 1}`,
                        });
                        continue;
                    }
                    const origin = url.origin;
                    if (origin !== trimmedLine) {
                        ctx.addIssue({
                            code: z.ZodIssueCode.custom,
                            message: `Invalid origin at line ${index + 1}\n (did you mean "${origin}"?)`,
                        });
                    }
                } catch (e) {
                    ctx.addIssue({
                        code: z.ZodIssueCode.custom,
                        message: `Invalid origin: ${trimmedLine} at line ${index + 1}`,
                    });
                }
            }
        })
        .refine((lines) => {
            const uniqueLines = [...new Set(lines)];
            return lines.length === uniqueLines.length;
        }, {
            message: "Duplicate origins are not allowed",
        })
        .transform((lines) => {
            return lines.join("\n");
        })
        .optional(),
    expiresAt: z.date().min(new Date(), {message: "Expiration date must be in the future"}).nullish(),
});

export type ProjectAccessTokensSchemaType = z.infer<typeof projectAccessTokensSchema>

export const rateLimitSchema = z.object({
    maxRequests: z.number().int(),
    windowMs: z.number().int(),
})

export type RateLimitSchemaType = z.infer<typeof rateLimitSchema>

export const userSchema = z.object({
    email: z.string().email(),
    name: z.string().optional(),
    role: z.enum([UserRole.USER, UserRole.ADMIN]).default(UserRole.USER),
});

export type UserSchemaType = z.infer<typeof userSchema>

export const changePasswordSchema = z.object({
    oldPassword: z.string().optional(),
    newPassword: z.string().min(3),
    checkPassword: z.string(),
}).superRefine((data, ctx) => {
    if (data.newPassword !== data.checkPassword) {
        ctx.addIssue({
            code: z.ZodIssueCode.custom,
            message: "Passwords do not match",
            path: ["checkPassword"],
        });
    }
})

export type ChangePasswordSchemaType = z.infer<typeof changePasswordSchema>
