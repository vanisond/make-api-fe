"use client";

import {SubmitHandler, useForm} from "react-hook-form";
import {projectSchema, ProjectSchemaType} from "@/app/(forms)/schema";
import {zodResolver} from "@hookform/resolvers/zod";
import {createProjectServerAction, updateProjectServerAction} from "@/app/(forms)/project/actions";
import React, {useTransition} from "react";
import toast from "react-hot-toast";
import {Button} from "@/components/ui/button";
import {Input} from "@/components/ui/input"
import {Form, FormControl, FormDescription, FormField, FormItem, FormLabel, FormMessage,} from "@/components/ui/form"
import {
    Dialog,
    DialogContent,
    DialogDescription,
    DialogHeader,
    DialogTitle,
    DialogTrigger,
} from "@/components/ui/dialog"
import {Project} from "@prisma/client";


type ProjectFormProps = {
    onAfterSubmit?: () => void;
    project?: Project; // if provided, the form will be in edit mode
}

/**
 * Form for creating and updating projects. When creating a project, the `project` prop should be omitted.
 */
export const ProjectForm: React.FC<ProjectFormProps> = ({onAfterSubmit, project}) => {
    const isEditMode = !!project;
    const [isPending, startTransition] = useTransition();
    const form = useForm<ProjectSchemaType>({
        // https://www.npmjs.com/package/@hookform/resolvers#zod
        resolver: zodResolver(projectSchema),
        defaultValues: project
    });

    const processForm: SubmitHandler<ProjectSchemaType> = (data) => {
        startTransition(async () => {
            if (isEditMode) {
                const result = await updateProjectServerAction(project.id, data);
                if (result.success) {
                    toast.success("Project updated!");
                    onAfterSubmit?.();
                } else {
                    toast.error("Error updating project");
                }
            } else {
                const result = await createProjectServerAction(data);
                if (result.success) {
                    toast.success("Project created!");
                    form.reset();
                    onAfterSubmit?.();
                } else {
                    toast.error("Error creating project");
                }
            }
        })
    }

    return (
        <div>
            <Form {...form}>
                <form onSubmit={form.handleSubmit(processForm)} className="space-y-6">
                    <FormField
                        control={form.control}
                        name="name"
                        render={({field}) => (
                            <FormItem>
                                <FormLabel>Project name</FormLabel>
                                <FormControl>
                                    <Input placeholder="Acme" {...field} />
                                </FormControl>
                                <FormDescription>
                                    {/* TODO */}
                                </FormDescription>
                                <FormMessage/>
                            </FormItem>
                        )}
                    />
                    <Button
                        type="submit"
                        disabled={isPending}
                        variant={isEditMode ? "outline" : "default"}
                    >
                        {isEditMode ? "Save changes" : "Create project"}
                    </Button>
                </form>
            </Form>
        </div>
    )
}

export const CreateProjectDialog = () => {
    const [open, setOpen] = React.useState(false);
    return (
        <Dialog open={open} onOpenChange={setOpen}>
            <DialogTrigger asChild><Button>Create project</Button></DialogTrigger>
            <DialogContent>
                <DialogHeader>
                    <DialogTitle>Create new project</DialogTitle>
                    <DialogDescription>
                        {/* TODO */}
                    </DialogDescription>
                </DialogHeader>
                <ProjectForm onAfterSubmit={() => setOpen(false)}/>
            </DialogContent>
        </Dialog>
    )
}