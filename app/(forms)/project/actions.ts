"use server";

import {projectSchema, ProjectSchemaType} from "@/app/(forms)/schema";
import {createProject, deleteProject, getProjectById, updateProject} from "@/lib/repository/projects";
import {auth} from "@/lib/auth";
import {revalidatePath} from "next/cache";
import {Prisma, UserRole} from "@prisma/client";
import {z} from "zod";

export const createProjectServerAction = async (formData: ProjectSchemaType) => {
    try {
        const projectData = projectSchema.parse(formData);
        const session = await auth();

        if (!session?.user?.id) {
            return {
                success: false,
                error: "You need to be logged in to create a project",
            };
        }

        const project = await createProject(session?.user?.id, projectData);
        revalidatePath('/');

        return {
            success: true,
            project,
        };

    } catch (error) {

        // https://zod.dev/ERROR_HANDLING
        if (error instanceof z.ZodError) {
            return {
                success: false,
                error: "Invalid data",
            };
        }

        // https://www.prisma.io/docs/concepts/components/prisma-client/handling-exceptions-and-errors
        if (error instanceof Prisma.PrismaClientKnownRequestError) {
            // The .code property can be accessed in a type-safe manner
            if (error.code === 'P2002') {
                return {
                    success: false,
                    error: "A project with that name already exists"
                };
            }
        }

        throw error
    }
}

export const updateProjectServerAction = async (projectId: string, formData: ProjectSchemaType) => {
    try {
        const projectData = projectSchema.parse(formData);
        const session = await auth();

        if (!session?.user?.id) {
            return {
                success: false,
                error: "You need to be logged in to update a project",
            };
        }

        // Check if user is owner of the project
        const project = await getProjectById(projectId);
        if (!project || project.ownerId !== session.user.id) {
            return {
                success: false,
                error: "Project not found",
            };
        }

        await updateProject(projectId, projectData);
        revalidatePath('/');

        return {
            success: true,
            project,
        };

    } catch (error) {

        // https://zod.dev/ERROR_HANDLING
        if (error instanceof z.ZodError) {
            return {
                success: false,
                error: "Invalid data",
            };
        }

        throw error
    }

}


export const deleteProjectServerAction = async (projectId: string) => {
    try {
        const session = await auth();

        if (!session?.user?.id) {
            return {
                success: false,
                error: "You need to be logged in to delete a project",
            };
        }

        // Check if user is owner of the project
        const project = await getProjectById(projectId);
        if (!project) {
            return {
                success: false,
                error: "Project not found",
            };
        }

        if (project.ownerId !== session.user.id && session.user.role !== UserRole.ADMIN) {
            return {
                success: false,
                error: "You are not allowed to delete this project",
            };
        }


        await deleteProject(projectId);
        revalidatePath('/');

        return {
            success: true,
            project,
        };

    } catch (error) {
        // TODO: Handle error
        throw error
    }
}