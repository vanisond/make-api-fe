"use client";

import React, {useTransition} from "react";
import {SubmitHandler, useForm} from "react-hook-form";
import {changePasswordSchema, ChangePasswordSchemaType} from "@/app/(forms)/schema";
import {zodResolver} from "@hookform/resolvers/zod";
import {User, UserRole} from "@prisma/client";
import toast from "react-hot-toast";
import {Form, FormControl, FormDescription, FormField, FormItem, FormLabel, FormMessage} from "@/components/ui/form";
import {Input} from "@/components/ui/input";
import {Button} from "@/components/ui/button";
import {changePasswordServerAction} from "@/app/(forms)/user/actions";
import {
    Dialog,
    DialogContent,
    DialogDescription,
    DialogHeader,
    DialogTitle,
    DialogTrigger
} from "@/components/ui/dialog";
import {useSession} from "next-auth/react";

type ChangePasswordProps = {
    onAfterSubmit?: () => void;
    user: User;
}

export const ChangePassword: React.FC<ChangePasswordProps> = (
    {
        onAfterSubmit,
        user
    }) => {
    const session = useSession();
    const [isPending, startTransition] = useTransition();
    const form = useForm<ChangePasswordSchemaType>({
        resolver: zodResolver(changePasswordSchema),
    });

    const processForm: SubmitHandler<ChangePasswordSchemaType> = (data) => {
        startTransition(async () => {
            const result = await changePasswordServerAction(user?.id, data);
            if (result?.success) {
                toast.success("Password changed!");
                onAfterSubmit?.();
            } else {
                toast.error(result.error ?? "Error updating password");
                // wait a little bit before allowing to submit again
                await new Promise(resolve => setTimeout(resolve, 600));
            }
        });
    }

    return (
        <div>
            <Form {...form}>
                <form id="change-password-form" onSubmit={form.handleSubmit(processForm)} className="space-y-6">
                    {((session.data?.user as User)?.role !== UserRole.ADMIN) && (
                        <FormField
                            control={form.control}
                            name="oldPassword"
                            render={({field}) => (
                                <FormItem>
                                    <FormLabel>Current password</FormLabel>
                                    <FormControl>
                                        <Input type="password" {...field} />
                                    </FormControl>
                                    <FormDescription>
                                        {/* TODO */}
                                    </FormDescription>
                                    <FormMessage/>
                                </FormItem>
                            )}
                        />
                    )}
                    <FormField
                        control={form.control}
                        name="newPassword"
                        render={({field}) => (
                            <FormItem>
                                <FormLabel>New password</FormLabel>
                                <FormControl>
                                    <Input type="password" {...field} />
                                </FormControl>
                                <FormDescription>
                                    {/* TODO */}
                                </FormDescription>
                                <FormMessage/>
                            </FormItem>
                        )}
                    />
                    <FormField
                        control={form.control}
                        name="checkPassword"
                        render={({field}) => (
                            <FormItem>
                                <FormLabel>Confirm new password</FormLabel>
                                <FormControl>
                                    <Input type="password" {...field} />
                                </FormControl>
                                <FormDescription>
                                    {/* TODO */}
                                </FormDescription>
                                <FormMessage/>
                            </FormItem>
                        )}
                    />
                    <div className="flex gap-2">
                        <Button type="submit" form="change-password-form" disabled={isPending}>Change password</Button>
                    </div>
                </form>
            </Form>
        </div>
    );
}

type ChangePasswordDialogProps = {
    user: User;
}

export const ChangePasswordDialog: React.FC<ChangePasswordDialogProps> = ({user}) => {
    const [open, setOpen] = React.useState(false);
    return (
        <Dialog open={open} onOpenChange={setOpen}>
            <DialogTrigger asChild>
                <Button variant="outline">Change password</Button>
            </DialogTrigger>
            <DialogContent>
                <DialogHeader>
                    <DialogTitle>Change password</DialogTitle>
                    <DialogDescription>
                        {/* TODO */}
                    </DialogDescription>
                </DialogHeader>
                <ChangePassword
                    onAfterSubmit={() => setOpen(false)}
                    user={user}
                />
            </DialogContent>
        </Dialog>
    )
}