"use client";

import React, {useTransition} from "react";
import {SubmitHandler, useForm} from "react-hook-form";
import {userSchema, UserSchemaType} from "@/app/(forms)/schema";
import {zodResolver} from "@hookform/resolvers/zod";
import {User, UserRole} from "@prisma/client";
import toast from "react-hot-toast";
import {Form, FormControl, FormDescription, FormField, FormItem, FormLabel, FormMessage} from "@/components/ui/form";
import {Input} from "@/components/ui/input";
import {Button} from "@/components/ui/button";
import {updateUserServerAction} from "@/app/(forms)/user/actions";
import {
    Dialog,
    DialogContent,
    DialogDescription,
    DialogHeader,
    DialogTitle,
    DialogTrigger
} from "@/components/ui/dialog";
import {RadioGroup, RadioGroupItem} from "@/components/ui/radio-group";
import {useSession} from "next-auth/react";

type UserFormProps = {
    onAfterSubmit?: () => void;
    user: User;
}

export const UserForm: React.FC<UserFormProps> = (
    {
        onAfterSubmit,
        user
    }) => {
    const session = useSession();
    const [isPending, startTransition] = useTransition();
    const form = useForm<UserSchemaType>({
        resolver: zodResolver(userSchema),
        defaultValues: user
    });

    const processForm: SubmitHandler<UserSchemaType> = (data) => {
        startTransition(async () => {
            const result = await updateUserServerAction(user?.id, data);
            if (result?.success) {
                toast.success("User updated!");
                onAfterSubmit?.();
            } else {
                toast.error("Error updating user");
            }
        });
    }

    return (
        <div>
            <Form {...form}>
                <form id="user-form" onSubmit={form.handleSubmit(processForm)} className="space-y-6">
                    <FormField
                        control={form.control}
                        name="name"
                        render={({field}) => (
                            <FormItem>
                                <FormLabel>User name</FormLabel>
                                <FormControl>
                                    <Input {...field}/>
                                </FormControl>
                                <FormDescription>
                                    {/* TODO */}
                                </FormDescription>
                                <FormMessage/>
                            </FormItem>
                        )}
                    />
                    <FormField
                        control={form.control}
                        name="email"
                        render={({field}) => (
                            <FormItem>
                                <FormLabel>E-mail</FormLabel>
                                <FormControl>
                                    <Input {...field}/>
                                </FormControl>
                                <FormDescription>
                                    {/* TODO */}
                                </FormDescription>
                                <FormMessage/>
                            </FormItem>
                        )}
                    />

                    {((session.data?.user as User)?.role === UserRole.ADMIN) && (
                        <FormField
                            control={form.control}
                            name="role"
                            render={({field}) => (
                                <FormItem className="space-y-3">
                                    <FormLabel>User role</FormLabel>
                                    <FormControl>
                                        <RadioGroup
                                            onValueChange={field.onChange}
                                            defaultValue={field.value}
                                            className="flex flex-row gap-5 space-y-1"
                                        >
                                            <FormItem className="flex items-center space-x-3 space-y-0">
                                                <FormControl>
                                                    <RadioGroupItem value={UserRole.USER}/>
                                                </FormControl>
                                                <FormLabel className="font-normal">
                                                    USER
                                                </FormLabel>
                                            </FormItem>
                                            <FormItem className="flex items-center space-x-3 space-y-0">
                                                <FormControl>
                                                    <RadioGroupItem value={UserRole.ADMIN}/>
                                                </FormControl>
                                                <FormLabel className="font-normal">
                                                    ADMIN
                                                </FormLabel>
                                            </FormItem>
                                        </RadioGroup>
                                    </FormControl>
                                    <FormDescription>
                                        {/* TODO */}
                                    </FormDescription>
                                    <FormMessage/>
                                </FormItem>
                            )}
                        />
                    )}


                    <div className="flex gap-2">
                        <Button type="submit" form="user-form" disabled={isPending}>Save changes</Button>
                    </div>
                </form>
            </Form>
        </div>
    );
}

type UserDialogProps = {
    user: User;
}

export const UserDialog: React.FC<UserDialogProps> = ({user}) => {
    const [open, setOpen] = React.useState(false);
    return (
        <Dialog open={open} onOpenChange={setOpen}>
            <DialogTrigger asChild>
                <Button variant="outline">Update</Button>
            </DialogTrigger>
            <DialogContent>
                <DialogHeader>
                    <DialogTitle>Update user</DialogTitle>
                    <DialogDescription>
                        {/* TODO */}
                    </DialogDescription>
                </DialogHeader>
                <UserForm
                    onAfterSubmit={() => setOpen(false)}
                    user={user}
                />
            </DialogContent>
        </Dialog>
    )
}
