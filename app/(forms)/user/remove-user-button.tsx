"use client";

import React, {useTransition} from "react";
import {User} from "@prisma/client";
import {
    AlertDialog, AlertDialogAction, AlertDialogCancel,
    AlertDialogContent, AlertDialogDescription, AlertDialogFooter,
    AlertDialogHeader,
    AlertDialogTitle,
    AlertDialogTrigger
} from "@/components/ui/alert-dialog";
import {Button} from "@/components/ui/button";
import {MdDelete} from "react-icons/md";
import {Input} from "@/components/ui/input";
import toast from "react-hot-toast";
import {deleteUserServerAction} from "@/app/(forms)/user/actions";
import {signOut} from "next-auth/react";

export const RemoveUserButton: React.FC<{ user: User }> = ({user}) => {
    const [isPending, startTransition] = useTransition();
    const [confirmText, setConfirmText] = React.useState("");
    return (
        <AlertDialog>
            <AlertDialogTrigger asChild>
                <Button
                    className="flex gap-1"
                    variant="destructive"
                    disabled={isPending}
                >
                    <MdDelete/>
                    Delete user
                </Button>
            </AlertDialogTrigger>
            <AlertDialogContent>
                <AlertDialogHeader>
                    <AlertDialogTitle>Are you absolutely sure?</AlertDialogTitle>
                    <AlertDialogDescription>
                        This action cannot be undone. This will permanently delete the user <strong>{user.name}</strong>.
                        This will also delete all projects and documents owned by this user.
                        <Input
                            onChange={(e) => setConfirmText(e.target.value)}
                            value={confirmText}
                            placeholder="Type DELETE to confirm."
                            className="mt-4"
                        />
                    </AlertDialogDescription>
                </AlertDialogHeader>
                <AlertDialogFooter>
                    <AlertDialogCancel>Cancel</AlertDialogCancel>
                    <AlertDialogAction
                        onClick={() => startTransition(async () => {
                            const result = await deleteUserServerAction(user.id);
                            if (result.success) {
                                toast.success("User deleted");
                                void signOut();
                            } else {
                                toast.error(result.error!);
                            }
                        })}
                        disabled={isPending || confirmText !== "DELETE"}
                    >
                        Delete
                    </AlertDialogAction>
                </AlertDialogFooter>
            </AlertDialogContent>
        </AlertDialog>
    )
}