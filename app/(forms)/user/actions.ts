"use server";

import {deleteUser, getUserById, updateUser} from "@/lib/repository/users";
import {auth} from "@/lib/auth";
import {Prisma, UserRole} from "@prisma/client";
import {revalidatePath} from "next/cache";
import {changePasswordSchema, ChangePasswordSchemaType} from "@/app/(forms)/schema";
import {compare, hash} from "bcrypt";

export const updateUserServerAction = async (id: string, data: Prisma.UserUpdateArgs['data']) => {
    try {
        const session = await auth();
        if (!session?.user?.id) {
            return {
                success: false,
                error: "You need to be logged in to proceed",
            };
        }

        const isAdmin = session.user.role === UserRole.ADMIN;

        if (session.user.id !== id && !isAdmin) {
            return {
                success: false,
                error: "You cannot delete another user",
            };
        }

        const dataToUpdate = {
            ...data,
            role: isAdmin ? data.role : undefined, // Only admins can change the role
            password: undefined
        };

        await updateUser(id, dataToUpdate);
        revalidatePath('/');

        return {
            success: true,
        };
    } catch (error) {
        throw error
    }
}

export const changePasswordServerAction = async (userId: string, data: ChangePasswordSchemaType) => {
    try {
        const parsedData = changePasswordSchema.safeParse(data);
        if (!parsedData.success) {
            return {
                success: false,
                error: "Invalid data",
            };
        }

        const session = await auth();
        if (!session?.user?.id) {
            return {
                success: false,
                error: "You need to be logged in to proceed",
            };
        }

        if (session.user.id !== userId && session.user.role !== UserRole.ADMIN) {
            return {
                success: false,
                error: "You cannot change the password of another user",
            };
        }

        const user = await getUserById(userId);
        if (!user) {
            return {
                success: false,
                error: "User not found",
            };
        }

        // Only change if old password is correct or if user is admin
        if (!await compare(data.oldPassword ?? "", user.password) && session.user.role !== UserRole.ADMIN) {
            return {
                success: false,
                error: "Current password is incorrect",
            };
        }

        const dataToUpdate = {
            password: await hash(data.newPassword, 10),
        };

        await updateUser(userId, dataToUpdate);
        revalidatePath('/');

        return {
            success: true,
        };
    } catch (error) {
        throw error;
    }
}

export const deleteUserServerAction = async (id: string) => {
    try {
        const session = await auth();
        if (!session?.user?.id) {
            return {
                success: false,
                error: "You need to be logged in to proceed",
            };
        }

        if (session.user.id !== id && session.user.role !== UserRole.ADMIN) {
            return {
                success: false,
                error: "You cannot delete another user",
            };
        }

        await deleteUser(id);
        revalidatePath('/');

        return {
            success: true,
        };
    } catch (error) {
        throw error
    }
}