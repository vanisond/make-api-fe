"use server";

import {projectAccessTokensSchema, ProjectAccessTokensSchemaType} from "@/app/(forms)/schema";
import {auth} from "@/lib/auth";
import {revalidatePath} from "next/cache";
import {z} from "zod";
import {
    createProjectAccessToken,
    deleteProjectAccessToken,
    getProjectAccessTokenById,
    updateProjectAccessToken
} from "@/lib/repository/projectAccessTokens";
import {getProjectById} from "@/lib/repository/projects";

export const createProjectAccessTokenAction = async (formData: ProjectAccessTokensSchemaType) => {
    try {
        const projectData = projectAccessTokensSchema.parse(formData);
        const session = await auth();

        if (!session?.user?.id) {
            return {
                success: false,
                error: "You need to be logged in to create a token",
            };
        }

        const project = await getProjectById(projectData.projectId);
        if (project?.ownerId !== session.user.id) {
            return {
                success: false,
                error: "You are not the owner of this token",
            };
        }

        await createProjectAccessToken(projectData);
        revalidatePath('/');

        return {
            success: true,
        };

    } catch (error) {

        // https://zod.dev/ERROR_HANDLING
        if (error instanceof z.ZodError) {
            return {
                success: false,
                error: "Invalid data",
            };
        }

        throw error
    }
}

export const updateProjectAccessTokenAction = async (id: string, formData: ProjectAccessTokensSchemaType) => {
    try {
        const projectData = projectAccessTokensSchema.parse(formData);
        const session = await auth();

        if (!session?.user?.id) {
            return {
                success: false,
                error: "You need to be logged in to update a token",
            };
        }

        // Check if user is owner of the project
        const token = await getProjectAccessTokenById(id);
        if (token?.project?.ownerId !== session.user.id) {
            return {
                success: false,
                error: "You are not the owner of this token",
            };
        }

        await updateProjectAccessToken(id, {
            ...projectData,
            expiresAt: projectData.expiresAt ?? undefined,
            updatedAt: new Date(),
        });
        revalidatePath('/');

        return {
            success: true,
        };

    } catch (error) {

        // https://zod.dev/ERROR_HANDLING
        if (error instanceof z.ZodError) {
            return {
                success: false,
                error: "Invalid data",
            };
        }

        throw error
    }


}

export const deleteProjectAccessTokenAction = async (id: string) => {
    try {
        const session = await auth();

        if (!session?.user?.id) {
            return {
                success: false,
                error: "You need to be logged in to delete a token",
            };
        }

        // Check if user is owner of the project
        const token = await getProjectAccessTokenById(id);
        if (token?.project?.ownerId !== session.user.id) {
            return {
                success: false,
                error: "You are not the owner of this token",
            };
        }

        await deleteProjectAccessToken(id);
        revalidatePath('/');

        return {
            success: true,
        };

    } catch (error) {
        throw error
    }
}


