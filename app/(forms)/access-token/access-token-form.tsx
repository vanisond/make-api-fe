"use client";

import React, {useTransition} from "react";
import {SubmitHandler, useForm} from "react-hook-form";
import {projectAccessTokensSchema, ProjectAccessTokensSchemaType} from "@/app/(forms)/schema";
import {zodResolver} from "@hookform/resolvers/zod";
import toast from "react-hot-toast";
import {Form, FormControl, FormDescription, FormField, FormItem, FormLabel, FormMessage} from "@/components/ui/form";
import {Input} from "@/components/ui/input";
import {Button} from "@/components/ui/button";
import {
    Dialog,
    DialogContent,
    DialogDescription,
    DialogHeader,
    DialogTitle,
    DialogTrigger
} from "@/components/ui/dialog";
import {
    createProjectAccessTokenAction,
    deleteProjectAccessTokenAction,
    updateProjectAccessTokenAction
} from "@/app/(forms)/access-token/actions";
import {Calendar} from "@/components/ui/calendar";
import {Popover, PopoverContent, PopoverTrigger} from "@/components/ui/popover";
import {cn} from "@/lib/utils";
import {MdClose, MdCopyAll, MdDelete, MdEditCalendar, MdRefresh, MdWarning} from "react-icons/md";
import {format, formatDistance} from "date-fns";
import {Textarea} from "@/components/ui/textarea";
import {ProjectAccessToken} from "@prisma/client";
import {
    AlertDialog,
    AlertDialogAction,
    AlertDialogCancel,
    AlertDialogContent,
    AlertDialogDescription,
    AlertDialogFooter,
    AlertDialogHeader,
    AlertDialogTitle,
    AlertDialogTrigger
} from "@/components/ui/alert-dialog";
import {Accordion, AccordionContent, AccordionItem, AccordionTrigger} from "@/components/ui/accordion";

const generateRandomToken = () => {
    return Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
}

const RemoveAccessTokenButton: React.FC<{ accessToken: ProjectAccessToken }> = ({accessToken}) => {
    const [isPending, startTransition] = useTransition()
    return (
        <AlertDialog>
            <AlertDialogTrigger asChild>
                <Button
                    className="flex gap-1"
                    variant="destructive"
                    disabled={isPending}
                >
                    <MdDelete/>
                    Revoke token
                </Button>
            </AlertDialogTrigger>
            <AlertDialogContent>
                <AlertDialogHeader>
                    <AlertDialogTitle>Are you absolutely sure?</AlertDialogTitle>
                    <AlertDialogDescription>
                        This action cannot be undone. This will permanently delete the
                        access token, so it can no longer be used to access the API.
                    </AlertDialogDescription>
                </AlertDialogHeader>
                <AlertDialogFooter>
                    <AlertDialogCancel>Cancel</AlertDialogCancel>
                    <AlertDialogAction
                        onClick={() => startTransition(async () => {
                            const result = await deleteProjectAccessTokenAction(accessToken.id);
                            if (result.success) {
                                toast.success("Access token deleted");
                            } else {
                                toast.error(result.error!);
                            }
                        })}
                        disabled={isPending}
                    >
                        Delete
                    </AlertDialogAction>
                </AlertDialogFooter>
            </AlertDialogContent>
        </AlertDialog>
    )
}

type AccessTokenFormProps = {
    onAfterSubmit?: () => void;
    projectId: string;
    accessToken?: ProjectAccessToken
}

export const AccessTokenForm: React.FC<AccessTokenFormProps> = ({onAfterSubmit, projectId, accessToken}) => {
    const isEditMode = !!accessToken;
    const [isPending, startTransition] = useTransition();
    const form = useForm<ProjectAccessTokensSchemaType>({
        resolver: zodResolver(projectAccessTokensSchema),
        defaultValues: {
            ...accessToken,
            projectId,
            allowedOrigins: accessToken?.allowedOrigins.join("\n"),
            token: isEditMode ? undefined : generateRandomToken(),
        },
    });

    const processForm: SubmitHandler<ProjectAccessTokensSchemaType> = (data) => {
        startTransition(async () => {
            if (isEditMode) {
                const result = await updateProjectAccessTokenAction(accessToken?.id, data);
                if (result.success) {
                    form.reset();
                    toast.success("Access token was updated!");
                    onAfterSubmit?.();
                } else {
                    toast.error(result?.error ?? "Error updating access token");
                }
            } else {
                const result = await createProjectAccessTokenAction(data);
                if (result.success) {
                    form.reset();
                    toast.success("New access token was created!");
                    onAfterSubmit?.();
                } else {
                    toast.error(result?.error ?? "Error creating access token");
                }
            }
        })
    };

    return (
        <div>
            <Form {...form}>
                <form id="access-token-form" onSubmit={form.handleSubmit(processForm)} className="space-y-6">
                    <input type="hidden" {...form.register("projectId")} value={projectId}/>
                    <FormField
                        control={form.control}
                        name="token"
                        render={({field}) => (
                            <FormItem>
                                <FormLabel>Access Token</FormLabel>
                                <FormControl>
                                    <Input
                                        {...field}
                                        className="font-black	"
                                        placeholder="************"
                                        disabled
                                        onClick={(event) => {
                                            debugger
                                            event.preventDefault()
                                            event.currentTarget.select();
                                        }}
                                    />
                                </FormControl>
                                {!isEditMode && (
                                    <div className="flex gap-2">
                                        <Button
                                            className="flex gap-2"
                                            type="button"
                                            variant="outline"
                                            onClick={() => navigator.clipboard.writeText(field.value ?? "")}
                                        >
                                            <MdCopyAll/>
                                            Copy
                                        </Button>
                                        <Button
                                            className="flex gap-2"
                                            type="button"
                                            variant="outline"
                                            onClick={() => field.onChange(generateRandomToken())}
                                        >
                                            <MdRefresh/>
                                            Regenerate
                                        </Button>
                                    </div>
                                )}
                                <FormDescription className="flex items-center">
                                    <MdWarning className="w-12 h-12 mr-3" />
                                    {isEditMode ? (
                                        <span>
                                            Editing value of this token is not possible. If you want to
                                            change it, you need to create a new token.
                                        </span>
                                    ) : (
                                        <span>
                                            Please copy this token and store it somewhere safe.
                                            You won&apos;t be able to see it again.
                                        </span>
                                    )}
                                </FormDescription>
                                <FormMessage/>
                            </FormItem>
                        )}
                    />
                    <FormField
                        control={form.control}
                        name="description"
                        render={({field}) => (
                            <FormItem>
                                <FormLabel>Description</FormLabel>
                                <FormControl>
                                    <Textarea placeholder="eg.: My access token" {...field} />
                                </FormControl>
                                <FormDescription>
                                    {/* TODO */}
                                </FormDescription>
                                <FormMessage/>
                            </FormItem>
                        )}
                    />
                    <FormField
                        control={form.control}
                        name="expiresAt"
                        render={({field}) => (
                            <FormItem>
                                <FormLabel>Expiration date</FormLabel>
                                <FormControl>
                                    <div className="flex gap-2">
                                        <Popover>
                                            <PopoverTrigger asChild>
                                                <Button
                                                    variant={"outline"}
                                                    className={cn(
                                                        "w-[280px] justify-start text-left font-normal",
                                                        !field.value && "text-muted-foreground"
                                                    )}
                                                    disabled={isEditMode}
                                                >
                                                    <MdEditCalendar className="mr-2 h-4 w-4"/>
                                                    {field.value ? format(field.value, "PPP") :
                                                        <span>Pick a date</span>}
                                                </Button>
                                            </PopoverTrigger>
                                            <PopoverContent className="w-auto p-0">
                                                <Calendar
                                                    mode="single"
                                                    selected={field.value ?? undefined}
                                                    weekStartsOn={1}
                                                    fromDate={new Date()}
                                                    defaultMonth={field.value ?? undefined}
                                                    onSelect={(value) => field.onChange(value)}
                                                    initialFocus
                                                />
                                            </PopoverContent>
                                        </Popover>
                                        <Button
                                            type="button"
                                            variant="outline"
                                            className="flex gap-2"
                                            disabled={!field.value}
                                            onClick={() => field.onChange(undefined)}
                                        >
                                            <MdClose/> Clear
                                        </Button>
                                    </div>
                                </FormControl>
                                <FormDescription className="flex flex-col">
                                    {field.value && (
                                        <span className="text-xs text-gray-400 mb-2">
                                            Expires {formatDistance(field.value, new Date(), {addSuffix: true})}
                                        </span>
                                    )}
                                    <span>
                                        {!isEditMode && "Leave empty for no expiration date."}
                                    </span>
                                </FormDescription>
                                <FormMessage/>
                            </FormItem>
                        )}
                    />

                    <Accordion type="single" collapsible>
                        <AccordionItem value="item-1">
                            <AccordionTrigger>Advanced options</AccordionTrigger>
                            <AccordionContent>
                                <FormField
                                    control={form.control}
                                    name="allowedOrigins"
                                    render={({field}) => (
                                        <FormItem>
                                            <FormLabel className="flex gap-2 items-center">
                                                Allowed origins
                                            </FormLabel>
                                            <FormControl>
                                                <Textarea
                                                    {...field}
                                                />
                                            </FormControl>
                                            <FormDescription>
                                                CORS origins that are allowed to use this token. One origin per line.
                                            </FormDescription>
                                            <FormMessage/>
                                        </FormItem>
                                    )}
                                />
                            </AccordionContent>
                        </AccordionItem>
                    </Accordion>

                    <div className="flex gap-2">
                        <Button type="submit" form="access-token-form" disabled={isPending}>Save token</Button>
                        {isEditMode && (
                            <RemoveAccessTokenButton accessToken={accessToken}/>
                        )}
                    </div>
                </form>
            </Form>
        </div>
    )
}

export const AccessTokenDialog: React.FC<{
    projectId: string,
    accessToken?: ProjectAccessToken
}> = ({projectId, accessToken}) => {
    const isEditMode = !!accessToken;
    const [open, setOpen] = React.useState(false);
    return (
        <Dialog open={open} onOpenChange={setOpen}>
            <DialogTrigger asChild>
                {isEditMode ? (
                    <Button variant="outline" size="sm">Edit</Button>
                ) : (
                    <Button>Add token</Button>
                )}
            </DialogTrigger>
            <DialogContent>
                <DialogHeader>
                    <DialogTitle>{isEditMode ? "Edit" : "Create"} access token</DialogTitle>
                    <DialogDescription>
                        {isEditMode ? "Edit" : "Create"} a new access token for this project.
                    </DialogDescription>
                </DialogHeader>
                <AccessTokenForm
                    onAfterSubmit={() => setOpen(false)}
                    projectId={projectId}
                    accessToken={accessToken}
                />
            </DialogContent>
        </Dialog>
    )
}

