"use client";

import React, {useEffect, useMemo, useTransition} from "react";
import {SubmitHandler, useForm, useFormContext} from "react-hook-form";
import {projectDocumentSchema, ProjectDocumentSchemaType} from "@/app/(forms)/schema";
import {zodResolver} from "@hookform/resolvers/zod";
import toast from "react-hot-toast";
import {Form, FormControl, FormDescription, FormField, FormItem, FormLabel, FormMessage} from "@/components/ui/form";
import {Input} from "@/components/ui/input";
import {Button} from "@/components/ui/button";
import {
    Dialog,
    DialogContent,
    DialogDescription,
    DialogHeader,
    DialogTitle,
    DialogTrigger
} from "@/components/ui/dialog";
import {
    checkUrlContentServerAction,
    createDocumentServerAction,
    loadRobotsTxtServerAction,
    updateDocumentServerAction
} from "@/app/(forms)/document/actions";
import {DocumentDownloadStrategy, ProjectDocument} from "@prisma/client";
import debounce from 'lodash/debounce';
import {Alert, AlertDescription, AlertTitle} from "@/components/ui/alert";
import {MdWarning} from "react-icons/md";
import {getRobotsTxtUrl} from "@/lib/utils";
import {Loader2} from "lucide-react";
import {RadioGroup, RadioGroupItem} from "@/components/ui/radio-group";
import {Accordion, AccordionContent, AccordionItem, AccordionTrigger} from "@/components/ui/accordion";
import {Checkbox} from "@/components/ui/checkbox";


const RobotsTxtChecker = () => {
    const form = useFormContext();
    const url = form.watch("url");
    const robotsTxtUrl = getRobotsTxtUrl(url);
    const [isPending, startTransition] = useTransition();
    const [robots, setRobots] = React.useState<any>(null);
    const [isAllowed, setIsAllowed] = React.useState<boolean>(true);
    const [isTyping, setIsTyping] = React.useState<boolean>(false);
    const [hasError, setHasError] = React.useState<boolean>(false);

    const debouncedLoadRobots = useMemo(() => debounce((url: string) => {
        startTransition(
            async () => {
                setRobots(null);
                const data = await loadRobotsTxtServerAction(url);
                console.log({data, timestamp: Date.now()});
                setRobots(data.robotsTxt);
                setIsAllowed(data.isAllowed ?? true);
                setHasError(!!url && !data.success);
                setIsTyping(false);
            }
        )
    }, 650), []);

    useEffect(() => {
        setIsTyping(true);
        debouncedLoadRobots(url)
    }, [debouncedLoadRobots, url]);

    if (!url || (isTyping && url && !isPending)) {
        return null;
    }

    if (isPending || robots === null) {
        return (
            <Alert key="robots-alert" className="transition-all transform">
                {/* TODO: use different spinner */}
                <Loader2 className="mr-2 h-4 w-4 animate-spin"/>
                <AlertTitle>
                    Checking robots.txt file
                </AlertTitle>
                <AlertDescription>
                    <p>
                        Checking the robots.txt file on the site...
                    </p>
                </AlertDescription>
            </Alert>
        )
    }

    if (isAllowed && !hasError) {
        return (
            <Alert key="robots-alert" variant="success" className="transition-all transform ">
                <AlertTitle>
                    Robots.txt file is not blocking the site
                </AlertTitle>
                <AlertDescription>
                    <p>
                        The robots.txt file on the site is not blocking the site. This means that the site owner has
                        not specified any restrictions on how the site can be accessed.
                    </p>
                    <Dialog>
                        <DialogTrigger asChild>
                            <Button type="button" variant="link">View the robots.txt file</Button>
                        </DialogTrigger>
                        <DialogContent>
                            <DialogHeader>
                                <DialogTitle>
                                    Content of robots.txt file
                                </DialogTitle>
                                <DialogDescription>
                                    ({robotsTxtUrl})
                                </DialogDescription>
                                <pre className="text-sm whitespace-pre-wrap max-h-[50vh] overflow-y-auto">
                                    {robots}
                                </pre>
                            </DialogHeader>
                        </DialogContent>
                    </Dialog>
                </AlertDescription>
            </Alert>
        )
    }

    if (hasError) {
        return (
            <Alert key="robots-alert" variant="warning" className="transition-all transform">
                <MdWarning className="h-4 w-4"/>
                <AlertTitle>
                    Error loading robots.txt file
                </AlertTitle>
                <AlertDescription>
                    <p>
                        {/*// info about missing robots.txt file*/}
                        The robots.txt file could not be loaded. This is not a problem, but it is recommended to check
                        the site&apos;s terms of use to make sure that it is allowed to scrape the site.
                    </p>
                </AlertDescription>
            </Alert>
        );
    }

    return (
        <Alert key="robots-alert" variant="destructive" className="transition-all transform">
            <MdWarning className="h-4 w-4"/>
            <AlertTitle>
                {hasError ? "Error loading robots.txt file" : "Robots.txt file is blocking the site"}
            </AlertTitle>
            <AlertDescription>
                <p>
                    The robots.txt file on the site is blocking the site. This means that the site owner has
                    specified restrictions on how the site can be accessed. This is not a problem, but it is
                    recommended to check the site&apos;s terms of use to make sure that it is allowed to scrape the
                    site.
                </p>
                <Dialog>
                    <DialogTrigger asChild>
                        <Button type="button" variant="link">View the robots.txt file</Button>
                    </DialogTrigger>
                    <DialogContent>
                        <DialogHeader>
                            <DialogTitle>
                                Content of robots.txt file
                            </DialogTitle>
                            <DialogDescription>
                                ({robotsTxtUrl})
                            </DialogDescription>
                            <pre className="text-sm whitespace-pre-wrap max-h-[50vh] overflow-y-auto">
                                    {robots}
                                </pre>
                        </DialogHeader>
                    </DialogContent>
                </Dialog>
            </AlertDescription>
        </Alert>
    );
}


const DocumentNameSuggester = () => {
    const form = useFormContext();
    const url = form.watch("url");
    const name = form.watch("name");
    const [suggestedName, setSuggestedName] = React.useState<string | undefined>();
    const showSuggestedName = !name && suggestedName;

    const debouncedLoadPageTitle = useMemo(() => debounce((url: string) => {
        checkUrlContentServerAction(url).then((data) => {
            console.log({data, timestamp: Date.now()});
            setSuggestedName(data.title?.substring(0, 500));
            if (!form.getFieldState("name").isDirty && !form.getValues("name")) {
                form.setValue("name", data.title?.substring(0, 500));
            }
        });
    }, 650), [form]);

    useEffect(() => {
        debouncedLoadPageTitle(url)
    }, [url]);

    if (!showSuggestedName) {
        return null;
    }

    return (
        <div className="text-xs text-gray-500 -mt-4 flex gap-1">
            <span className="py-0.5">
                Suggested:
            </span>
            <a
                href="#"
                className="italic py-0.5 leading-snug rounded-md hover:bg-gray-50"
                onClick={(event) => {
                    event.preventDefault();
                    form.setValue("name", suggestedName);
                }}
            >
                &ldquo;{suggestedName}&rdquo;
            </a>
        </div>
    );
}


type DocumentFormProps = {
    onAfterSubmit?: () => void;
    projectId: string;
    projectDocument?: ProjectDocument; // if provided, the form will be in edit mode
}

export const DocumentForm: React.FC<DocumentFormProps> = (
    {
        onAfterSubmit,
        projectId,
        projectDocument
    }
) => {
    const isEditMode = !!projectDocument;
    const [isPending, startTransition] = useTransition();
    const form = useForm<ProjectDocumentSchemaType>({
        resolver: zodResolver(projectDocumentSchema),
        defaultValues: projectDocument ?? {
            strictTransform: true,
            downloadStrategy: DocumentDownloadStrategy.RAW,
        }
    });

    const processForm: SubmitHandler<ProjectDocumentSchemaType> = (data) => {
        startTransition(async () => {
            if (isEditMode) {
                const result = await updateDocumentServerAction(projectDocument?.id, data);
                if (result.success) {
                    toast.success("Document updated!");
                    onAfterSubmit?.();
                } else {
                    toast.error(result.error ?? "Error updating document");
                }
            } else {
                const result = await createDocumentServerAction(data);
                if (result.success) {
                    toast.success("Document created!");
                    form.reset();
                    onAfterSubmit?.();
                } else {
                    toast.error(result.error ?? "Error creating document");
                }
            }
        })
    }

    return (
        <div>
            <Form {...form}>
                <form onSubmit={form.handleSubmit(processForm)} className="space-y-6">
                    <input type="hidden" {...form.register("projectId")} value={projectId} />
                    <FormField
                        control={form.control}
                        name="url"
                        render={({field}) => (
                            <FormItem>
                                <FormLabel>Document URL</FormLabel>
                                <FormControl>
                                    <Input
                                        type="url"
                                        placeholder="https://example.com"
                                        spellCheck={false}
                                        {...field}
                                    />
                                </FormControl>
                                <FormDescription>
                                    {/* TODO */}
                                </FormDescription>
                                <FormMessage/>
                            </FormItem>
                        )}
                    />
                    <div className="transition-hei transition transformght duration-75 ease-out">
                        <RobotsTxtChecker/>
                    </div>
                    <FormField
                        control={form.control}
                        name="name"
                        render={({field}) => (
                            <FormItem>
                                <FormLabel>Document name</FormLabel>
                                <FormControl>
                                    <Input placeholder="Example page" {...field} />
                                </FormControl>
                                <FormDescription>
                                    {/* TODO */}
                                </FormDescription>
                                <FormMessage/>
                            </FormItem>
                        )}
                    />
                    <DocumentNameSuggester/>


                    <Accordion type="single" collapsible>
                        <AccordionItem value="item-1">
                            <AccordionTrigger>Advanced options</AccordionTrigger>
                            <AccordionContent>
                                <FormField
                                    control={form.control}
                                    name="downloadStrategy"
                                    render={({field}) => (
                                        <FormItem className="space-y-3">
                                            <FormLabel>Download strategy</FormLabel>
                                            <FormControl>
                                                <RadioGroup
                                                    onValueChange={field.onChange}
                                                    defaultValue={field.value}
                                                    className="flex flex-row gap-5 space-y-1"
                                                >
                                                    <FormItem className="flex items-center space-x-3 space-y-0">
                                                        <FormControl>
                                                            <RadioGroupItem value={DocumentDownloadStrategy.RAW}/>
                                                        </FormControl>
                                                        <FormLabel className="font-normal">
                                                            Simple fetch
                                                        </FormLabel>
                                                    </FormItem>
                                                    <FormItem className="flex items-center space-x-3 space-y-0">
                                                        <FormControl>
                                                            <RadioGroupItem value={DocumentDownloadStrategy.HEADLESS}/>
                                                        </FormControl>
                                                        <FormLabel className="font-normal">
                                                            Headless
                                                        </FormLabel>
                                                    </FormItem>
                                                </RadioGroup>
                                            </FormControl>
                                            <FormDescription>
                                                {/* Show description based on the selected value of the downloadStrategy */}
                                                {field.value === DocumentDownloadStrategy.RAW && (
                                                    <>
                                                        The document will be downloaded using a simple HTTP request.
                                                        This is the
                                                        fastest option, but it will not work for sites that require
                                                        JavaScript to
                                                        render the content.
                                                    </>
                                                )}
                                                {field.value === DocumentDownloadStrategy.HEADLESS && (
                                                    <>
                                                        The document will be downloaded using a headless browser. This
                                                        is a slower
                                                        option, but it will work for sites that require JavaScript to
                                                        render the
                                                        content.
                                                    </>
                                                )}
                                            </FormDescription>
                                            <FormMessage/>
                                        </FormItem>
                                    )}
                                />
                                <FormField
                                    control={form.control}
                                    name="strictTransform"
                                    render={({ field }) => (
                                        <FormItem className="flex flex-row items-start mt-2 space-x-3 space-y-0 rounded-md border p-4">
                                            <FormControl>
                                                <Checkbox
                                                    checked={field.value}
                                                    onCheckedChange={field.onChange}
                                                />
                                            </FormControl>
                                            <div className="space-y-1 leading-none">
                                                <FormLabel>
                                                    Strict transform
                                                </FormLabel>
                                                <FormDescription>
                                                    If enabled, the API transformation endpoint will be disabled if some errors are encountered while transforming the document.
                                                </FormDescription>
                                            </div>
                                        </FormItem>
                                    )}
                                />
                                <FormField
                                    control={form.control}
                                    name="cacheMaxAge"
                                    render={({field}) => (
                                        <FormItem className="space-y-3 mt-2">
                                            <FormLabel>Cache control max-age</FormLabel>
                                            <FormControl>
                                                <Input
                                                    type="number"
                                                    min={0}
                                                    step={1}
                                                    {...field}
                                                    value={field.value ?? 0}
                                                    onChange={(event) => field.onChange(+event.target.value)}
                                                />
                                            </FormControl>
                                            <FormDescription>
                                                Specify the max-age value for the Cache-Control header. This will tell the browser how long it can cache the document before it needs to be re-downloaded.
                                            </FormDescription>
                                            <FormMessage/>
                                        </FormItem>
                                    )}
                                />
                            </AccordionContent>
                        </AccordionItem>
                    </Accordion>


                    <Button
                        type="submit"
                        disabled={isPending}
                    >
                        {isEditMode ? "Update document" : "Add document"}
                    </Button>
                </form>
            </Form>
        </div>
    );
}

export const CreateDocumentDialog : React.FC<{projectId: string}> = ({projectId}) => {
    const [open, setOpen] = React.useState(false);
    return (
        <Dialog open={open} onOpenChange={setOpen}>
            <DialogTrigger asChild><Button>Add document</Button></DialogTrigger>
            <DialogContent>
                <DialogHeader>
                    <DialogTitle>Add document</DialogTitle>
                    <DialogDescription>
                        {/* TODO */}
                    </DialogDescription>
                </DialogHeader>
                <DocumentForm onAfterSubmit={() => setOpen(false)} projectId={projectId}/>
            </DialogContent>
        </Dialog>
    )
}

export const UpdateDocumentDialog: React.FC<{ projectId: string, projectDocument?: ProjectDocument }> = (
    {
        projectId,
        projectDocument
    }
) => {
    const isEditMode = !!projectDocument;
    const [open, setOpen] = React.useState(false);
    return (
        <Dialog open={open} onOpenChange={setOpen}>
            <DialogTrigger asChild>
                {isEditMode ? (
                    <Button variant="secondary">Document settings</Button>
                ) : (
                    <Button>Add document</Button>
                )}
            </DialogTrigger>
            <DialogContent>
                <DialogHeader>
                    <DialogTitle>Document settings</DialogTitle>
                    <DialogDescription>
                        {/* TODO */}
                    </DialogDescription>
                </DialogHeader>
                <DocumentForm
                    onAfterSubmit={() => setOpen(false)}
                    projectId={projectId}
                    projectDocument={projectDocument}
                />
            </DialogContent>
        </Dialog>
    )
}