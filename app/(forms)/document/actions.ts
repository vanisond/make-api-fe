"use server";

import {projectDocumentSchema, ProjectDocumentSchemaType} from "@/app/(forms)/schema";
import {auth} from "@/lib/auth";
import {revalidatePath} from "next/cache";
import {Prisma} from "@prisma/client";
import {z} from "zod";
import {
    createProjectDocument,
    deleteProjectDocument,
    getProjectDocumentById,
    updateProjectDocument
} from "@/lib/repository/documents";
import robotsParser from 'robots-parser';
import * as cheerio from "cheerio";
import axios from "axios";
import {getRobotsTxtUrl} from "@/lib/utils";
import dns from 'node:dns';
import {getProjectById} from "@/lib/repository/projects";

export const createDocumentServerAction = async (formData: ProjectDocumentSchemaType) => {
    try {
        const projectDocumentData = projectDocumentSchema.parse(formData);
        const session = await auth();

        if (!session?.user?.id) {
            return {
                success: false,
                error: "You need to be logged in to create a project",
            };
        }

        const document = await createProjectDocument(projectDocumentData);
        revalidatePath('/');

        return {
            success: true,
            document,
        };

    } catch (error) {

        // https://zod.dev/ERROR_HANDLING
        if (error instanceof z.ZodError) {
            return {
                success: false,
                error: "Invalid data",
            };
        }

        // https://www.prisma.io/docs/concepts/components/prisma-client/handling-exceptions-and-errors
        if (error instanceof Prisma.PrismaClientKnownRequestError) {
            // The .code property can be accessed in a type-safe manner
            if (error.code === 'P2002') {
                return {
                    success: false,
                    error: "A document with that name already exists"
                };
            }
        }

        throw error
    }
}

export const updateDocumentServerAction = async (projectId: string, formData: ProjectDocumentSchemaType) => {
    try {
        const projectData = projectDocumentSchema.parse(formData);
        const session = await auth();

        if (!session?.user?.id) {
            return {
                success: false,
                error: "You need to be logged in to create a document",
            };
        }

        const document = await updateProjectDocument(projectId, projectData);
        revalidatePath('/');

        return {
            success: true,
            document,
        };

    } catch (error) {

        // https://zod.dev/ERROR_HANDLING
        if (error instanceof z.ZodError) {
            return {
                success: false,
                error: "Invalid data",
            };
        }

        // https://www.prisma.io/docs/concepts/components/prisma-client/handling-exceptions-and-errors
        if (error instanceof Prisma.PrismaClientKnownRequestError) {
            // The .code property can be accessed in a type-safe manner
            if (error.code === 'P2002') {
                return {
                    success: false,
                    error: "A document with that name already exists"
                };
            }
        }

        throw error
    }
}

export const deleteDocumentServerAction = async (documentId: string) => {
    try {
        const session = await auth();

        if (!session?.user?.id) {
            return {
                success: false,
                error: "You need to be logged in to delete a project document",
            };
        }

        // Check if user is owner of the project
        const projectDocument = await getProjectDocumentById(documentId);
        const project = projectDocument?.projectId && await getProjectById(projectDocument.projectId);
        if (!project || project.ownerId !== session.user.id) {
            return {
                success: false,
                error: "Project not found",
            };
        }

        await deleteProjectDocument(documentId);
        revalidatePath('/');

        return {
            success: true,
        };

    } catch (error) {
        throw error
    }
}

export const loadRobotsTxtServerAction = async (url: string) => {
    const urlSchema = projectDocumentSchema.pick({ url: true });
    const parsedData = urlSchema.safeParse({ url });

    if (!parsedData.success) {
        return {
            success: false,
            error: "Invalid URL",
        };
    }

    // Use DNS to check if domain exists
    try {
        await dns.promises.lookup(new URL(parsedData.data.url).hostname);
    } catch (error) {
        return {
            success: false,
            error: "Invalid URL",
        };
    }

    const robotsTxtUrl = getRobotsTxtUrl(parsedData.data.url);
    if (!robotsTxtUrl) {
        return {
            success: false,
            error: "Invalid URL",
        };
    }

    try {
        const res = await axios.get(`${process.env.PAGE_FETCHER_SERVICE_BASE_URL}/raw?url=${robotsTxtUrl}`);
        const data = res.data;
        const robots = robotsParser(robotsTxtUrl, data)

        return {
            success: true,
            robotsTxt: data,
            isAllowed: robots.isAllowed(parsedData.data.url, '*')
        }
    } catch (error) {
        return {
            success: false,
            error: "Failed to load robots.txt",
        }
    }
}

export const checkUrlContentServerAction = async (url: string) => {
    const urlSchema = projectDocumentSchema.pick({ url: true });
    const parsedData = urlSchema.safeParse({ url });

    if (!parsedData.success) {
        return {
            success: false,
            error: "Invalid URL",
        };
    }

    try {
        const res = await axios.get(`${process.env.PAGE_FETCHER_SERVICE_BASE_URL}/raw?url=${parsedData.data.url}`);
        const data = await res.data;
        const $ = cheerio.load(data);
        const parsedHtml = $.html();
        const title = $('title').text();

        return {
            success: true,
            data: parsedHtml, // TODO: remove this
            title,
            isValidHtml: parsedHtml.length > 0
        }
    } catch (error) {
        return {
            success: false,
            error: "Failed to load page content",
        }
    }
}
