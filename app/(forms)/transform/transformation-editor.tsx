"use client";

import React, {useTransition} from "react";
import toast from "react-hot-toast";
import {EditorWindow} from "@/components/transformation/EditorWindow";
import {ProjectDocument, TransformationSchema} from "@prisma/client";
import {Button} from "@/components/ui/button";
import {useAppSelector} from "@/redux/hooks";
import {updateTransformationSchemaServerAction} from "@/app/(forms)/transform/actions";
import {UpdateDocumentDialog} from "@/app/(forms)/document/document-form";
import {MdWarning} from "react-icons/md";
import {HoverCard, HoverCardContent, HoverCardTrigger} from "@/components/ui/hover-card";
import {nodeSchema} from "@/lib/transformService/nodeTypes";

const SaveSchemaButton: React.FC<{ transformationSchema: TransformationSchema }> = ({transformationSchema}) => {
    const [isPending, startTransition] = useTransition()
    const schema = useAppSelector(state => state.transformationSchema.schema);

    const handleSaveSchema = async () => {
        startTransition(async () => {
            const result = await updateTransformationSchemaServerAction(transformationSchema.id, {
                ...transformationSchema,
                schema
            });

            if (result.success) {
                toast.success("Schema saved!");
            }

            if (result.success && result.reEnabled) {
                toast.success("Transformation was successfully re-enabled for this document.");
            }

            if (!result.success) {
                toast.error(result.error!);
            }
        })
    }

    return (
        <Button
            onClick={handleSaveSchema}
            disabled={isPending}
        >
            Save changes
        </Button>
    )
}

type TransformationEditorProps = {
    content: string;
    contentLoadSuccess: boolean;
    contentType?: string;
    projectDocument: ProjectDocument;
    transformationSchema: TransformationSchema;
}

export const TransformationEditor: React.FC<TransformationEditorProps> = (
    {
        content,
        contentLoadSuccess,
        contentType,
        projectDocument,
        transformationSchema
    }
) => {
    const parsedSchema = nodeSchema.safeParse(transformationSchema.schema);
    const isText = contentType?.includes("text/");

    return (
        <div className="flex flex-col gap-2 h-full">
            <div className="flex items-center gap-2">
                <div className="flex-1">Transformation schema for{" "}
                    {contentLoadSuccess ? (
                        <span className="bg-gray-100 italic py-0.5 px-2 rounded-lg">
                            {projectDocument.url}
                            {!isText && (
                                <HoverCard>
                                    <HoverCardTrigger>
                                        <MdWarning className="h-4 w-4 ml-2 inline text-warning"/>
                                    </HoverCardTrigger>
                                    <HoverCardContent>
                                        {contentType ? (
                                            `The content type of the document on this URL is ${contentType}. The transformation schema might not work as expected.`
                                        ) : (
                                            "The content type of the document on this URL is unknown. The transformation schema might not work as expected."
                                        )}
                                    </HoverCardContent>
                                </HoverCard>
                            )}
                            {projectDocument.transformDisabled && (
                                <HoverCard>
                                    <HoverCardTrigger>
                                        <MdWarning className="h-4 w-4 ml-2 inline text-destructive"/>
                                    </HoverCardTrigger>
                                    <HoverCardContent>
                                        Transformation has been disabled for this document due to repeated failures in transformation. Please check the transformation schema, URL and download strategy.
                                    </HoverCardContent>
                                </HoverCard>
                            )}
                        </span>
                    ) : (
                        <HoverCard>
                            <HoverCardTrigger>
                                <span className="inline-flex items-center text-destructive gap-2 bg-gray-100 italic py-0.5 px-2 rounded-lg">
                                    <span>{projectDocument.url}</span>
                                    <MdWarning className="h-4 w-4 text-destructive"/>
                                </span>
                            </HoverCardTrigger>
                            <HoverCardContent>
                                The content of the document on this URL could not be loaded. Please check the URL and download strategy.
                            </HoverCardContent>
                        </HoverCard>
                    )}
                </div>
                <div>
                    <SaveSchemaButton transformationSchema={transformationSchema}/>
                </div>
                <div>
                    <UpdateDocumentDialog
                        projectId={projectDocument.projectId}
                        projectDocument={projectDocument}
                    />
                </div>
            </div>
            {parsedSchema.success && (
                <EditorWindow
                    transformationSchema={parsedSchema.data}
                    content={content}
                />
            )}
        </div>

    );
}