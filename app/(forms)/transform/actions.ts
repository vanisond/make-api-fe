"use server";

import {auth} from "@/lib/auth";
import {revalidatePath} from "next/cache";
import {z} from "zod";
import {updateTransformationSchema} from "@/lib/repository/transformationSchemas";
import type {Prisma, ProjectDocument} from "@prisma/client";
import {transformHTMLtoJSON} from "@/lib/transformService/transform";
import {disableProjectDocumentTransform, getProjectDocumentById} from "@/lib/repository/documents";
import {fetchHtml} from "@/lib/urlContentService/htmlFetcher";
import {nodeSchema, TransformationSchemaNodeType} from "@/lib/transformService/nodeTypes";

/**
 * Returns true if the document was re-enabled
 */
const reEnableDocumentTransform = async (document: ProjectDocument, schema: TransformationSchemaNodeType) => {
    const htmlContent = await fetchHtml(document.url, document.downloadStrategy);
    if (!htmlContent.success) {
        return false;
    }
    const transformationResult = transformHTMLtoJSON(htmlContent.data, schema);
    if (!transformationResult.success) {
        return false;
    }

    const transformErrors = transformationResult.context?.errors ?? [];
    if (document.strictTransform && transformErrors.length > 0) {
        return false;
    }

    await disableProjectDocumentTransform(document.id, false);
    return true;
}

export const updateTransformationSchemaServerAction = async (schemaId: string, schemaData: Prisma.TransformationSchemaUpdateArgs['data']) => {
    try {
        const session = await auth();

        if (!session?.user?.id) {
            return {
                success: false,
                error: "You need to be logged in to create a project",
            };
        }

        const parsedSchema = nodeSchema.safeParse(schemaData.schema);
        if (!parsedSchema.success) {
            return {
                success: false,
                error: "Invalid schema",
            };
        }

        const transformationSchema = await updateTransformationSchema(schemaId, schemaData);

        // Re-enable document transform if it was disabled
        const document = await getProjectDocumentById(transformationSchema.projectDocumentId);
        let reEnabled = false;
        if (document?.transformDisabled) {
            reEnabled = await reEnableDocumentTransform(document, parsedSchema.data);
        }


        revalidatePath('/');

        return {
            success: true,
            project: transformationSchema,
            reEnabled,
        };

    } catch (error) {

        // https://zod.dev/ERROR_HANDLING
        if (error instanceof z.ZodError) {
            return {
                success: false,
                error: "Invalid data",
            };
        }

        throw error
    }
}