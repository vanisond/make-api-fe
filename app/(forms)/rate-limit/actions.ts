"use server";

import {Prisma} from '@prisma/client';
import {getRateLimit, updateAccessTokenRateLimit, updateRateLimit, updateUserRateLimit} from "@/lib/repository/rateLimits";
import {rateLimitSchema, RateLimitSchemaType} from "@/app/(forms)/schema";
import {auth} from "@/lib/auth";
import {getProjectAccessTokenById} from "@/lib/repository/projectAccessTokens";
import {z} from "zod";
import {revalidatePath} from "next/cache";

export const getRateLimitServerAction = async (id?: string) => {
    try {
        const rateLimit = await getRateLimit(id);

        return {
            success: true,
            rateLimit,
        };
    } catch (error) {
        return {
            success: false,
            error: "Rate limit not found",
        };
    }
}

export const updateRateLimitServerAction = async (id: string, data: Prisma.RateLimitUpdateArgs['data']) => {
    return updateRateLimit(id, data);
}

export const updateUserRateLimitServerAction = async (userId: string, data: RateLimitSchemaType) => {
    try {
        const rateLimit = rateLimitSchema.parse(data);

        const session = await auth();
        if (!session?.user?.id) {
            return {
                success: false,
                error: "You need to be logged in to proceed",
            };
        }

        if (session.user.id !== userId) {
            return {
                success: false,
                error: "You cannot update the rate limit of another user",
            };
        }

        const rateLimitResult = await updateUserRateLimit(userId, rateLimit);
        revalidatePath('/');
        return {
            success: true,
            data: rateLimitResult,
        }
    } catch (error) {

        // https://zod.dev/ERROR_HANDLING
        if (error instanceof z.ZodError) {
            return {
                success: false,
                error: "Invalid data",
            };
        }

        throw error
    }
}

export const updateAccessTokenRateLimitServerAction = async (accessTokenId: string, data: RateLimitSchemaType) => {
    try {
        const rateLimit = rateLimitSchema.parse(data);

        const session = await auth();
        if (!session?.user?.id) {
            return {
                success: false,
                error: "You need to be logged in to proceed",
            };
        }

        const accessToken = await getProjectAccessTokenById(accessTokenId);
        if (!accessToken) {
            return {
                success: false,
                error: "Access token not found",
            };
        }

        if (accessToken.project.ownerId !== session.user.id) {
            return {
                success: false,
                error: "You cannot update the rate limit of another user",
            };
        }

        const rateLimitResult = await updateAccessTokenRateLimit(accessTokenId, rateLimit);
        revalidatePath('/');
        return {
            success: true,
            data: rateLimitResult,
        }
    } catch (error) {

        // https://zod.dev/ERROR_HANDLING
        if (error instanceof z.ZodError) {
            return {
                success: false,
                error: "Invalid data",
            };
        }

        throw error
    }
}
