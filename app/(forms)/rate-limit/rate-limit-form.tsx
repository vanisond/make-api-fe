"use client";

import React, {useEffect, useTransition} from "react";
import {SubmitHandler, useForm} from "react-hook-form";
import {rateLimitSchema, RateLimitSchemaType} from "@/app/(forms)/schema";
import {zodResolver} from "@hookform/resolvers/zod";
import {Button} from "@/components/ui/button";
import {
    Dialog,
    DialogContent,
    DialogDescription,
    DialogHeader,
    DialogTitle,
    DialogTrigger
} from "@/components/ui/dialog";
import {RateLimitType} from "@prisma/client";
import toast from "react-hot-toast";
import {
    getRateLimitServerAction,
    updateAccessTokenRateLimitServerAction,
    updateUserRateLimitServerAction
} from "@/app/(forms)/rate-limit/actions";
import {Form, FormControl, FormDescription, FormField, FormItem, FormLabel, FormMessage} from "@/components/ui/form";
import {Input} from "@/components/ui/input";

type UserProps = {
    rateLimitType: typeof RateLimitType.USER
    userId: string;
    accessTokenId?: never;
};

type AccessTokenProps = {
    rateLimitType: typeof RateLimitType.ACCESS_TOKEN
    userId?: never;
    accessTokenId: string;
};

type RateLimitFormProps = {
    onAfterSubmit?: () => void;
    rateLimitId?: string;
} & (UserProps | AccessTokenProps);

export const RateLimitForm: React.FC<RateLimitFormProps> = (
    {
        onAfterSubmit,
        rateLimitType,
        rateLimitId,
        userId,
        accessTokenId
    }) => {
    const [isPending, startTransition] = useTransition();
    const form = useForm<RateLimitSchemaType>({
        resolver: zodResolver(rateLimitSchema),
    });

    useEffect(() => {
        if (!rateLimitId) return;
        startTransition(async () => {
            const result = await getRateLimitServerAction(rateLimitId);
            if (result?.success) {
                form.reset(result?.rateLimit ?? {});
            }
        })
    }, [rateLimitId]);

    const processForm: SubmitHandler<RateLimitSchemaType> = (data) => {
        startTransition(async () => {
            if (rateLimitType === RateLimitType.USER) {
                const result = await updateUserRateLimitServerAction(userId, data);
                if (result?.success) {
                    toast.success("Rate limit updated!");
                    onAfterSubmit?.();
                } else {
                    toast.error("Error updating rate limit");
                }
            }

            if (rateLimitType === RateLimitType.ACCESS_TOKEN) {
                const result = await updateAccessTokenRateLimitServerAction(accessTokenId, data);
                if (result?.success) {
                    toast.success("Rate limit updated!");
                    onAfterSubmit?.();
                } else {
                    toast.error("Error updating rate limit");
                }
            }
        });
    }

    return (
        <div>
            <Form {...form}>
                <form id="access-token-form" onSubmit={form.handleSubmit(processForm)} className="space-y-6">
                    <FormField
                        control={form.control}
                        name="maxRequests"
                        render={({field}) => (
                            <FormItem>
                                <FormLabel>Maximum requests per time window</FormLabel>
                                <FormControl>
                                    <Input
                                        type="number"
                                        min={0}
                                        step={1}
                                        {...field}
                                        onChange={(event) => field.onChange(+event.target.value)}
                                    />
                                </FormControl>
                                <FormDescription>
                                    {/* TODO */}
                                </FormDescription>
                                <FormMessage/>
                            </FormItem>
                        )}
                    />
                    <FormField
                        control={form.control}
                        name="windowMs"
                        render={({field}) => (
                            <FormItem>
                                <FormLabel>Time window (milliseconds)</FormLabel>
                                <FormControl>
                                    <Input
                                        type="number"
                                        min={0}
                                        step={1}
                                        {...field}
                                        onChange={(event) => field.onChange(+event.target.value)}
                                    />
                                </FormControl>
                                <FormDescription>
                                    {/* TODO */}
                                </FormDescription>
                                <FormMessage/>
                            </FormItem>
                        )}
                    />

                    <div className="flex gap-2">
                        <Button type="submit" form="access-token-form" disabled={isPending}>Save rate limit</Button>
                    </div>
                </form>
            </Form>
        </div>
    );
}

type RateLimitDialogProps = {
    rateLimitId?: string
} & (UserProps | AccessTokenProps)

export const RateLimitDialog: React.FC<RateLimitDialogProps> = ({rateLimitId, ...props}) => {
    const [open, setOpen] = React.useState(false);
    return (
        <Dialog open={open} onOpenChange={setOpen}>
            <DialogTrigger asChild>
                <Button variant="outline">Limits</Button>
            </DialogTrigger>
            <DialogContent>
                <DialogHeader>
                    <DialogTitle>Rate limits</DialogTitle>
                    <DialogDescription>
                        {/* TODO */}
                    </DialogDescription>
                </DialogHeader>
                <RateLimitForm
                    onAfterSubmit={() => setOpen(false)}
                    rateLimitId={rateLimitId}
                    {...props}
                />
            </DialogContent>
        </Dialog>
    )
}

