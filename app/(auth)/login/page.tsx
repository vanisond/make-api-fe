"use client";

import Image from "next/image";
import Link from "next/link";
import React, {useState} from "react";
import {useRouter} from "next/navigation";
import {signIn} from "next-auth/react";
import toast from "react-hot-toast";
import logo from "@/public/logo.svg";
import {SubmitHandler, useForm} from "react-hook-form";
import {signInSchema, SignInSchemaType} from "@/app/(forms)/schema";
import {zodResolver} from "@hookform/resolvers/zod";
import {Form, FormControl, FormDescription, FormField, FormItem, FormLabel, FormMessage} from "@/components/ui/form";
import {Input} from "@/components/ui/input";
import {Button} from "@/components/ui/button";

export default function Login() {
    const [loading, setLoading] = useState(false);
    const router = useRouter();

    const form = useForm<SignInSchemaType>({
        resolver: zodResolver(signInSchema),
    });

    const processForm: SubmitHandler<SignInSchemaType> = (data) => {
        setLoading(true);
        signIn("credentials", {
            redirect: false,
            email: data.email,
            password: data.password,
            // @ts-ignore
        }).then(({error}) => {
            if (error) {
                setLoading(false);
                toast.error(error);
            } else {
                router.refresh();
                router.push("/");
            }
        });
    }

    return (
        <div className="flex h-full items-center justify-center ">
            <div className="z-10 w-full max-w-md overflow-hidden rounded-2xl border border-gray-100 shadow-xl">
                <div
                    className="flex flex-col items-center justify-center space-y-3 border-b border-gray-200 bg-white px-4 py-6 pt-8 text-center sm:px-16">
                    <Link href="/">
                        <Image
                            src={logo}
                            priority
                            alt="Logo"
                            className="h-10 w-10"
                        />
                    </Link>
                    <h3 className="text-xl font-semibold">Sign In</h3>
                    <p className="text-sm text-gray-500">
                        Sign in to your account
                    </p>
                </div>
                <Form {...form}>
                    <form
                        id="sign-in-form"
                        onSubmit={form.handleSubmit(processForm)}
                        className="flex flex-col space-y-4 bg-gray-50 px-4 py-8 sm:px-16"
                    >
                        <FormField
                            control={form.control}
                            name="email"
                            render={({field}) => (
                                <FormItem>
                                    <FormLabel>E-mail</FormLabel>
                                    <FormControl>
                                        <Input placeholder="john.doe@example.com" {...field}/>
                                    </FormControl>
                                    <FormDescription>
                                        {/* TODO */}
                                    </FormDescription>
                                    <FormMessage/>
                                </FormItem>
                            )}
                        />
                        <FormField
                            control={form.control}
                            name="password"
                            render={({field}) => (
                                <FormItem>
                                    <FormLabel>Password</FormLabel>
                                    <FormControl>
                                        <Input type="password" {...field}/>
                                    </FormControl>
                                    <FormDescription>
                                        {/* TODO */}
                                    </FormDescription>
                                    <FormMessage/>
                                </FormItem>
                            )}
                        />
                        <Button
                            type="submit"
                            disabled={loading}
                        >
                            {loading ? (
                                "..."
                            ) : (
                                <p>{"Sign In"}</p>
                            )}
                        </Button>
                        {(
                            <p className="text-center text-sm text-gray-600">
                                Don&apos;t have an account?{" "}
                                <Link href="/register" className="font-semibold text-gray-800">
                                    Sign up
                                </Link>{" "}
                                for free.
                            </p>
                        )}
                    </form>
                </Form>
            </div>
        </div>
    );
}