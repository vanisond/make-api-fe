import NextAuth from "next-auth"
import CredentialsProvider from "next-auth/providers/credentials";
import EmailProvider from "next-auth/providers/email";
import {v4 as uuidv4} from 'uuid';
import {PrismaAdapter} from "@auth/prisma-adapter";
import prisma from "@/lib/prisma";
import {config} from "@/lib/auth";

const handler = NextAuth(config);

export {handler as GET, handler as POST}

