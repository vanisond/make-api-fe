import { PrismaClient } from '@prisma/client'
import {hash} from "bcrypt";

const prisma = new PrismaClient();

async function main() {
    const admin = await prisma.user.upsert({
        where: { email: 'admin@make.api' },
        update: {},
        create: {
            email: 'admin@make.api',
            name: 'Admin',
            password: await hash("admin-password", 10),
            role: 'ADMIN',
        },
    });
    console.log({admin});
}

main()
    .then(async () => {
        await prisma.$disconnect()
    })
    .catch(async (e) => {
        console.error(e)
        await prisma.$disconnect()
        process.exit(1)
    });