import {NextRequest, NextResponse} from 'next/server'
import {getToken} from "next-auth/jwt";
import {User, UserRole} from "@prisma/client";

export const config = {
    matcher: '/:path*',
}

export default async function middleware(req: NextRequest) {
    // Get the pathname of the request (e.g. /, /protected)
    const path = req.nextUrl.pathname;

    // If it's the root path, just render it
    if (path === "/") {
        return NextResponse.next();
    }

    const session = await getToken({
        req,
        secret: process.env.NEXTAUTH_SECRET,
    });

    // Protect the /projects and /admin paths from unauthenticated access
    if (!session && (path.startsWith("/projects") || path.startsWith("/admin") || path.startsWith("/profile"))) {
        return NextResponse.redirect(new URL("/login", req.url));
    }

    // Protect the /admin path from non-admin users
    if (session && path.startsWith("/admin") && (session as User)?.role !== UserRole.ADMIN) {
        return new NextResponse(null, {status: 403});
    }

    // Redirect logged-in users away from the /login and /register paths
    if (session && (path.startsWith("/login") || path.startsWith("/register"))) {
        return NextResponse.redirect(new URL("/projects", req.url));
    }

    return NextResponse.next();
}